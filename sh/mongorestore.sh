#!/bin/bash
# Description: Btf5 mongorestore
# Usage:  sh mongorestore.sh  
# Usage:  sh mongorestore.sh btf5 ~/backup/btf5/backup.tar.gz

database=btf5
# targetFile=./btf5-bttc-datas.201905.backup.gz
# targetFile=./btf5-bttc-datas.201811.backup.gz
targetFile=./btf5-bttc-datas.201809.backup.gz

if [ "$1" != "" ]
then
  database="$1"
fi

if [ "$2" != "" ]
then
  targetFile="$2"
fi

echo "mongorestore start..."
echo "copy file"
docker cp $targetFile mongodb:backup.gz
# docker cp $targetFile mongodb:backup.tar.gz

# docker cp btf5-bttc-datas.201809.backup.gz mongodb:btf5-bttc-datas.201809.backup.gz
# docker exec -t mongodb mongodump -d btf5 --gzip --archive=btf5-bttc-datas.201809.backup.gz
# docker cp mongodb:dbdirectory.gz dbdirectory.gz
# docker exec -t mongodb mongorestore -d btf5 --gzip --archive=btf5-bttc-datas.201809.backup.gz
# docker cp /tmp/backup/backup.tar.gz mongodb:backup.tar.gz
# echo "unzip file"
# docker exec -t mongodb tar zxvf backup.tar.gz
echo "mongorestore"
docker exec -t mongodb mongorestore -d $database --gzip --archive=backup.gz
# docker exec -t mongodb mongorestore -d $database /data/db/temp/$database
echo "remove temp file and folder"
docker exec -t mongodb rm /backup.gz
# docker exec -t mongodb rm -rf /data/db/temp
echo "mongorestore done.";
