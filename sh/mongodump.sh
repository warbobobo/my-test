#!/bin/bash
# Description: Btf5 mongodump.sh
# Usage:  sh mongodump.sh  
# Usage:  sh mongodump.sh btf5 ~/backup/btf5/backup.tar.gz
# Usage:  sudo sh mongodump.sh btf5 /var/maxwin/backup.tar.gz

IFS=" " 
database=btf5
# targetFile=./btf5-bttc-datas.201809-1.backup.gz
# targetFile=./btf5-bttc-datas.201905-2.backup.gz
targetFile=./btf5-bttc-datas.201909-middle.backup.gz
# targetFile=~/backup/btf5/backup.tar.gz
collections=($"app-metas blogs bttc-groups bttc-healths bttc-packs bttc-path-metas bttc-paths bttcs consumers detour-messages encryptions etags events freeways graphs location-ic-types location-types locations mssages phrases settings traveltime-calc-types traveltimes users")

if [ "$1" != "" ]
then
  database="$1"
fi

if [ "$2" != "" ]
then
  targetFile="$2"
fi

echo $database
echo $targetFile

echo "collectionName: ${collectionName}"
echo "mongodump start..."
# for collection in $collections; do
# echo $collection
# docker exec -t mongodb mongodump -d $database --collection $collection --gzip --archive=backup.gz
# # docker exec -t mongodb mongodump -d $database --collection $collection -o /data/db/temp
# # docker exec -t mongodb mongodump -d btf5 --gzip --archive=btf5-bttc-datas.201809.backup.gz
# done
# echo "compression all files in the folder"
# docker exec -t mongodb tar zcvf backup.tar.gz /data/db/temp/$database
docker exec -t mongodb mongodump -d $database --collection bttc-data-middles --gzip --archive=backup.gz
# docker exec -t mongodb mongodump -d $database --collection bttc-datas --gzip --archive=backup.gz
echo "move file"
docker cp mongodb:backup.gz $targetFile
echo "remove temp file and folder"
docker exec -t mongodb rm /backup.gz
# docker exec -t mongodb rm -rf /data/db/temp
echo "mongodump done.";
