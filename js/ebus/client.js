const axios = require('axios');
const FormData = require('form-data');
const request = require('request');

const get = async (route_id) => {
  let mydata = {};
  await axios({
    // url: `https://citybus.taichung.gov.tw/cms/api/route/${route_id}/map`,
    method: 'get',
  })
  .then((response) => {
      // console.log(response);
      // console.log(response.data);
      mydata = response.data;
  })
    // .catch(function (error) {
    // // console.log("error",error);
    // console.log("error",error.response.status,error.response.statusText);
    // });
  return mydata;
}

const upload = async(route_Id,title,img,onShelfAt) => {
  const formData = new FormData();
  // formData.append(`files.source`, request('https://citybus.taichung.gov.tw/cms/api/route/300/map/46/image'), 'file.png');
  formData.append(`files`, request(img), 'file.png');
  await axios({
    url: 'http://10.40.201.178:1380/ebus/strapi/upload/',
    method: 'post',
    data: formData,
    headers : formData.getHeaders(),
  })
  .then(async (response) => {
    console.log(response.data[0]._id);
    await axios({
      url: 'http://10.40.201.178:1380/ebus/strapi/route-maps',
      method: 'post',
      data: {
        route_Id,
        title,
        onShelfAt,
        source: response.data[0]._id,
        locale : 'zh'
      },
    })
    .then((response) => {
      console.log(response.data);
    })
  })
  .catch(function (error) {
   console.log("error",error.response.status,error.response.statusText);
  });
}

const post = async (data) => {
  await axios({
    // url: 'http://10.40.201.178:1380/ebus/strapi/route-maps',
    // url: 'https://citybus.taichung.gov.tw/ebus/strapi/route-maps',
    url: 'http://10.40.201.178:1340/route-groups',
    method: 'post',
    data: data,
  })
  .then((response) => {
    console.log(response.data);
  })
  .catch(function (error) {
  // console.log("error",error);
   console.log("error",error.response.status,error.response.statusText);
  });
}

const remove = async () => {
  await axios
  .delete(`${url}${id}`)
  .then(response => {
    console.log(response);
  })
  .catch(error => {
    // Handle error.
    console.log("error",error.response.status,error.response.statusText);
  });
}
  
module.exports = {
  get,
  upload,
  post,
  remove
};