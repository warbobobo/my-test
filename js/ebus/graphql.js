const gql = require('graphql-tag');
const { fetchData } = require('../graphql/helper');

// const url = 'https://citybus.taichung.gov.tw/ebus/strapi/graphql';
// const url = 'http://192.168.30.30:4000/graphql';
const query = gql`
query RoutesSchedule($lang: String!="zh"){
  routes(lang:$lang) {
    edges {
      node {
        id
        name
        departure
        description
        destination
        seq
      }
    }
  }
}
`;

const index = async () => {
  const { data } = await fetchData(url, query, { lang: 'zh' });
  // const { data } = await fetchData(url, query, { });
  // const { data } = await fetchData(query, { lang: 'zh' });
  console.log('data:',data);
  return data;
  // console.log('data:',data.routes.edges[0].node.id);
  // var list = [];
  // for(i in data.routes.edges) {
  //   list.push(data.routes.edges[i].node.id);
  // }
  // console.log(list.sort());
};
// index();

module.exports = {
  index
}