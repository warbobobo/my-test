const graphql = require('./graphql');
const client = require('./client');
const mydata  = require('./mydata')

let sum =0;
const takeImg = (json) => {
  return json.content;
}

const findImg = (url) => {
  const index = url.indexOf('image');
  console.log(index);
}

const testMap = async () => {
  // const route = await graphql.index();
  const route = mydata.routeMap;
  // console.log('route', route.routes.edges.length);
  // for(let i=0 ;i<3;i++) {
  for(let i=0 ;i<route.routes.edges.length;i++){
    // console.log('node', route.routes.edges[i].node);
    // console.log('id', route.routes.edges[i].node.id);
    const title = route.routes.edges[i].node.description;
    const route_Id = route.routes.edges[i].node.name;
    const route_map = await client.get(route.routes.edges[i].node.id);
    if(route_map.length!=0){
      // console.log('route_map', route_map);
        // for(let j=0 ;j<route_map.length;j++) 
        // sum++;
        const j = route_map.length - 1;
        {
        // if(route_map[j].content.indexOf('/image') > 0){
        if(route_map[j].type == 'image'){
          sum++;
          console.log('i', i);
          // const onShelfAt = route_map[j].on_shelf_at;
          // await client.upload(route_Id,title,route_map[j].content,onShelfAt);
          return;
        }
        else {
          // await client.post
          const onShelfAt = route_map[j].on_shelf_at;
          await client.post(route_Id,title,onShelfAt);
        }
      }
    }
  }
  console.log(sum);
  // const route_map = await client.get();
}

const testGroup = async () => {
  // const data = {
  //   // route_Id,
  //   // title,
  //   // onShelfAt,
  //   description: '紅線',
  //   name: '紅線',
  //   routes: [
  //     // {
  //     //   routeId: 801,
  //     //   name: '紅1'
  //     // },
  //     // {
  //     //   routeId: 179,
  //     //   name: '紅2'
  //     // },
  //     {
  //       routeId: 810,
  //       name: '紅10'
  //     }
  //   ],
  //   locale : 'zh',
  // };
  // for(let i=0;i<1;i++){
    for(let i=1;i<mydata.routeGroup.length;i++){
    let routes = [];
    for(let j=0;j<mydata.routeGroup[i].routes.length;j++)
    {
      let routeId = mydata.routeGroup[i].routes[j].Id;
      let name = mydata.routeGroup[i].routes[j].NameZh;
      routes.push({routeId,name})
    }
    let group = {
      description: mydata.routeGroup[i].description,
      name: mydata.routeGroup[i].name,
      routes: routes,
      locale : 'zh',
    };
    await client.post(group);
  }
}
// findImg('https:\/\/citybus.taichung.gov.tw\/cms\/api\/route\/300\/map\/1\/image');
// client.post('https://citybus.taichung.gov.tw/cms/api/route/300/map/46/image');
// graphql.index();
testGroup();
// console.log(mydata.routeGroup);