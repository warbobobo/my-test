var routeMap = {
  "data": {
    "routes": {
      "edges": [
        {
          "node": {
            "id": "265",
            "name": "265",
            "departure": "東勢",
            "description": "東勢  -  新五村(經新社)",
            "destination": "新五村",
            "seq": 2650
          }
        },
        {
          "node": {
            "id": "65436",
            "name": "6268F",
            "departure": "干城站",
            "description": "埔里→臺中(地理中心碑)[經國道6號]",
            "destination": "地理中心碑",
            "seq": 162686
          }
        },
        {
          "node": {
            "id": "142",
            "name": "142",
            "departure": "干城站",
            "description": "干城站-豐年社區五站-干城站",
            "destination": "豐年社區",
            "seq": 1420
          }
        },
        {
          "node": {
            "id": "212",
            "name": "212",
            "departure": "豐原",
            "description": "豐原  -  大甲體育場(水美)",
            "destination": "大甲體育場(水美)",
            "seq": 2120
          }
        },
        {
          "node": {
            "id": "655",
            "name": "655",
            "departure": "梧棲觀光漁港",
            "description": "高美濕地-新烏日車站-嶺東科技大學",
            "destination": "嶺東科技大學",
            "seq": 6550
          }
        },
        {
          "node": {
            "id": "65225",
            "name": "7506",
            "departure": "中部辦公室站",
            "description": "嘉義縣、市→嘉義交流道→國道1號→南屯交流道→臺中市南屯區(嘉義-台中)",
            "destination": "嘉義市轉運中心",
            "seq": 175060
          }
        },
        {
          "node": {
            "id": "183",
            "name": "183",
            "departure": "豐原",
            "description": "豐原  -  臺中港郵局(經新庄)",
            "destination": "臺中港郵局(經新庄)",
            "seq": 1830
          }
        },
        {
          "node": {
            "id": "61",
            "name": "61",
            "departure": "太平",
            "description": "太平 - 臺中車站 - 大雅",
            "destination": "大雅",
            "seq": 610
          }
        },
        {
          "node": {
            "id": "289",
            "name": "289",
            "departure": "臺中二中",
            "description": "臺中二中  -  東平社區",
            "destination": "東平社區",
            "seq": 2890
          }
        },
        {
          "node": {
            "id": "65439",
            "name": "6268A",
            "departure": "干城站",
            "description": "埔里→臺中(地理中心碑)[經中投公路]",
            "destination": "地理中心碑",
            "seq": 162681
          }
        },
        {
          "node": {
            "id": "308",
            "name": "308",
            "departure": "關連工業區",
            "description": "關連工業區-新民高中",
            "destination": "新民高中",
            "seq": 3080
          }
        },
        {
          "node": {
            "id": "2505",
            "name": "黃5",
            "departure": "慈明高中",
            "description": "慈明高中 - 黃竹里",
            "destination": "黃竹里",
            "seq": 10000
          }
        },
        {
          "node": {
            "id": "278",
            "name": "278",
            "departure": "東勢",
            "description": "東勢  -  水井(新社、大南)",
            "destination": "水井",
            "seq": 2780
          }
        },
        {
          "node": {
            "id": "65104",
            "name": "1872",
            "departure": "國光客運臺中車站",
            "description": "高雄→臺中",
            "destination": "高雄站(國光)",
            "seq": 118720
          }
        },
        {
          "node": {
            "id": "65201",
            "name": "1612D",
            "departure": "市府轉運站",
            "description": "臺南→國道3號→臺北[經安坑交流道，不停北港路站]",
            "destination": "兵配廠",
            "seq": 116124
          }
        },
        {
          "node": {
            "id": "65159",
            "name": "1632",
            "departure": "臺北轉運站",
            "description": "竹山→草屯→台北",
            "destination": "竹山",
            "seq": 116320
          }
        },
        {
          "node": {
            "id": "133",
            "name": "133",
            "departure": "高鐵臺中站",
            "description": "高鐵臺中站 - 朝陽科技大學",
            "destination": "朝陽科技大學",
            "seq": 1330
          }
        },
        {
          "node": {
            "id": "65226",
            "name": "7506B",
            "departure": "朝馬轉運站",
            "description": "嘉義→朝馬[直達車]",
            "destination": "嘉義市轉運中心",
            "seq": 175062
          }
        },
        {
          "node": {
            "id": "1151",
            "name": "151A",
            "departure": "朝陽科技大學",
            "description": "朝陽科技大學 - 臺中市議會",
            "destination": "臺中市議會",
            "seq": 1511
          }
        },
        {
          "node": {
            "id": "65434",
            "name": "9018",
            "departure": "臺中火車站(東站)",
            "description": "鹿港→彰化→彰化交流道→國道1號→臺中交流道→臺中市",
            "destination": "鹿港站",
            "seq": 190180
          }
        },
        {
          "node": {
            "id": "65432",
            "name": "6188",
            "departure": "臺中車站",
            "description": "竹山→臺3線→名間交流道→國道3號高速公路(中2高)→中投交流道→中投公路→臺中市",
            "destination": "竹山站",
            "seq": 161880
          }
        },
        {
          "node": {
            "id": "65473",
            "name": "6738A",
            "departure": "員客臺中站",
            "description": "王功→臺中(經二林)[經國道一號]",
            "destination": "南王功",
            "seq": 167381
          }
        },
        {
          "node": {
            "id": "65383",
            "name": "6883A",
            "departure": "高鐵臺中站",
            "description": "臺中→溪頭(經高鐵臺中站)[高鐵臺中站至溪頭]",
            "destination": "溪頭",
            "seq": 168831
          }
        },
        {
          "node": {
            "id": "465",
            "name": "65延",
            "departure": "南區公所",
            "description": "南區公所  -  大富路98巷口",
            "destination": "潭雅神綠園道",
            "seq": 651
          }
        },
        {
          "node": {
            "id": "853",
            "name": "153區",
            "departure": "高鐵臺中站",
            "description": "高鐵臺中站 - 東勢高工",
            "destination": "東勢高工",
            "seq": 1532
          }
        },
        {
          "node": {
            "id": "228",
            "name": "228",
            "departure": "大鵬國小",
            "description": "大鵬國小-豐原東站",
            "destination": "豐原東站",
            "seq": 2280
          }
        },
        {
          "node": {
            "id": "974",
            "name": "74繞",
            "departure": "太平",
            "description": "太平-新烏日車站-嶺東科技大學",
            "destination": "嶺東科技大學",
            "seq": 741
          }
        },
        {
          "node": {
            "id": "164",
            "name": "164",
            "departure": "清水國中",
            "description": "清水國中  -  楊厝里",
            "destination": "楊厝里",
            "seq": 1640
          }
        },
        {
          "node": {
            "id": "65384",
            "name": "6333B",
            "departure": "干城站",
            "description": "水里→中興→臺中[經高鐵臺中站]",
            "destination": "水里",
            "seq": 163332
          }
        },
        {
          "node": {
            "id": "209",
            "name": "209",
            "departure": "豐原",
            "description": "豐原  -  東勢林場",
            "destination": "東勢林場",
            "seq": 2090
          }
        },
        {
          "node": {
            "id": "65165",
            "name": "1628A",
            "departure": "臺北轉運站",
            "description": "漚汪→佳里、麻豆→台北[不停中壢站]",
            "destination": "漚汪",
            "seq": 116281
          }
        },
        {
          "node": {
            "id": "65524",
            "name": "6883",
            "departure": "臺中",
            "description": "溪頭→臺中(經高鐵臺中站)",
            "destination": "溪頭",
            "seq": 168830
          }
        },
        {
          "node": {
            "id": "65433",
            "name": "6187",
            "departure": "臺中車站",
            "description": "嘉義→竹崎交流道→國道3號→中興交流道→臺14線→臺63線→臺中車站",
            "destination": "嘉義市先期交通轉運中心",
            "seq": 161870
          }
        },
        {
          "node": {
            "id": "919",
            "name": "309區",
            "departure": "靜宜大學",
            "description": "靜宜大學 - 臺中車站 - 高美濕地",
            "destination": "臺中車站",
            "seq": 3091
          }
        },
        {
          "node": {
            "id": "65168",
            "name": "1625C",
            "departure": "臺中車站",
            "description": "臺南市→臺南交流道→國道1號→臺中交流道→臺中市[經永康交流道，不停麻豆站、新營站]",
            "destination": "兵配廠",
            "seq": 116253
          }
        },
        {
          "node": {
            "id": "1081",
            "name": "108區",
            "departure": "霧峰農工",
            "description": "霧峰農工-南開科技大學校區",
            "destination": "南開科技大學校區",
            "seq": 1081
          }
        },
        {
          "node": {
            "id": "219",
            "name": "219",
            "departure": "石岡國小",
            "description": "石岡國小 - 上龍興里",
            "destination": "上龍興里",
            "seq": 2190
          }
        },
        {
          "node": {
            "id": "65440",
            "name": "6333A",
            "departure": "干城站",
            "description": "水里→中興→臺中[經國道3號]",
            "destination": "水里",
            "seq": 163331
          }
        },
        {
          "node": {
            "id": "65429",
            "name": "9010",
            "departure": "臺中車站",
            "description": "新竹市→國道１號→臺中市",
            "destination": "新竹轉運站",
            "seq": 190100
          }
        },
        {
          "node": {
            "id": "213",
            "name": "213",
            "departure": "豐原",
            "description": "豐原  -  大甲體育場(經下后里)",
            "destination": "大甲體育場",
            "seq": 2130
          }
        },
        {
          "node": {
            "id": "77",
            "name": "77",
            "departure": "中科管理局",
            "description": "中科管理局 - 臺中慈濟醫院",
            "destination": "臺中慈濟醫院",
            "seq": 770
          }
        },
        {
          "node": {
            "id": "65530",
            "name": "6899D",
            "departure": "干城站",
            "description": "埔里→臺中[經國道6號]",
            "destination": "埔里站",
            "seq": 168994
          }
        },
        {
          "node": {
            "id": "69",
            "name": "69",
            "departure": "龍潭里",
            "description": "龍潭里  -  臺中國際機場",
            "destination": "臺中國際機場",
            "seq": 690
          }
        },
        {
          "node": {
            "id": "55",
            "name": "55",
            "departure": "地方法院",
            "description": "地方法院  -   豐原",
            "destination": "豐原",
            "seq": 550
          }
        },
        {
          "node": {
            "id": "2261",
            "name": "226延",
            "departure": "中社",
            "description": "中社 - 南村路 - 太平里",
            "destination": "太平里",
            "seq": 2262
          }
        },
        {
          "node": {
            "id": "28",
            "name": "28",
            "departure": "四張犁圖書館",
            "description": "四張犁圖書館 - 坪頂",
            "destination": "坪頂",
            "seq": 280
          }
        },
        {
          "node": {
            "id": "243",
            "name": "243",
            "departure": "東山高中(景賢六路)",
            "description": "東山高中(景賢六路) - 亞洲大學安藤館",
            "destination": "亞洲大學安藤館",
            "seq": 2430
          }
        },
        {
          "node": {
            "id": "65438",
            "name": "6268B",
            "departure": "逢甲大學",
            "description": "埔里→臺中(地理中心碑)[延駛逢甲大學、鯉魚潭]",
            "destination": "鯉魚潭",
            "seq": 162682
          }
        },
        {
          "node": {
            "id": "97",
            "name": "97",
            "departure": "國立苑裡高中",
            "description": "國立苑裡高中 - 大甲區銅安厝 - 臺中國際機場",
            "destination": "臺中國際機場",
            "seq": 970
          }
        },
        {
          "node": {
            "id": "65389",
            "name": "6670E",
            "departure": "台中航空站",
            "description": "日月潭→高鐵臺中站→臺中[台灣好行日月潭線延駛臺中航空站]",
            "destination": "日月潭",
            "seq": 166705
          }
        },
        {
          "node": {
            "id": "65175",
            "name": "1621B",
            "departure": "成功嶺站",
            "description": "高雄→烏日→台中[高雄至成功嶺班次]",
            "destination": "高雄火車站",
            "seq": 116212
          }
        },
        {
          "node": {
            "id": "65195",
            "name": "1616",
            "departure": "臺北轉運站",
            "description": "員林→台北[經蓮花池]",
            "destination": "員林轉運站",
            "seq": 116160
          }
        },
        {
          "node": {
            "id": "23",
            "name": "23",
            "departure": "中清同榮路口",
            "description": "中清同榮路口 - 市立大里高中",
            "destination": "市立大里高中",
            "seq": 230
          }
        },
        {
          "node": {
            "id": "2502",
            "name": "黃2",
            "departure": "東勢",
            "description": "東勢 - 民安",
            "destination": "民安",
            "seq": 10000
          }
        },
        {
          "node": {
            "id": "58",
            "name": "58",
            "departure": "大慶活動中心",
            "description": "大慶活動中心  -  潭子勝利運動公園",
            "destination": "潭子勝利運動公園",
            "seq": 580
          }
        },
        {
          "node": {
            "id": "109",
            "name": "9區",
            "departure": "大雅",
            "description": "大雅 - 三田國小",
            "destination": "三田國小",
            "seq": 91
          }
        },
        {
          "node": {
            "id": "8991",
            "name": "199延",
            "departure": "龍津高中",
            "description": "龍津高中-文修停車場",
            "destination": "文修停車場",
            "seq": 1991
          }
        },
        {
          "node": {
            "id": "107",
            "name": "107",
            "departure": "黎明新村",
            "description": "黎明新村  - 舊正",
            "destination": "舊正",
            "seq": 1070
          }
        },
        {
          "node": {
            "id": "906",
            "name": "306W",
            "departure": "臺中車站",
            "description": "臺中車站-梧棲-清水",
            "destination": "清水",
            "seq": 3061
          }
        },
        {
          "node": {
            "id": "73",
            "name": "73",
            "departure": "統聯轉運站",
            "description": "統聯轉運站  -  文英兒童公園",
            "destination": "文英兒童公園",
            "seq": 730
          }
        },
        {
          "node": {
            "id": "65101",
            "name": "1873",
            "departure": "國光客運臺中車站",
            "description": "屏東→臺中",
            "destination": "麟洛站",
            "seq": 118730
          }
        },
        {
          "node": {
            "id": "65140",
            "name": "1764",
            "departure": "羅東轉運站",
            "description": "梨山→羅東",
            "destination": "梨山",
            "seq": 117640
          }
        },
        {
          "node": {
            "id": "8",
            "name": "8",
            "departure": "臺中刑務所演武場",
            "description": "臺中刑務所演武場-逢甲大學(逢甲路)",
            "destination": "逢甲大學(逢甲路)",
            "seq": 80
          }
        },
        {
          "node": {
            "id": "25",
            "name": "25",
            "departure": "忠信國小(柳川西路)",
            "description": "忠信國小(柳川西路) - 僑光科技大學",
            "destination": "僑光科技大學",
            "seq": 250
          }
        },
        {
          "node": {
            "id": "951",
            "name": "95副",
            "departure": "六福公園",
            "description": "六福公園 - 外埔老人文康中心 - 土城",
            "destination": "土城",
            "seq": 951
          }
        },
        {
          "node": {
            "id": "235",
            "name": "235",
            "departure": "文化中心-南環",
            "description": "豐原圓環線",
            "destination": "文化中心-北環",
            "seq": 2350
          }
        },
        {
          "node": {
            "id": "938",
            "name": "305區",
            "departure": "巨業沙鹿站",
            "description": "巨業沙鹿站-大甲",
            "destination": "大甲",
            "seq": 3053
          }
        },
        {
          "node": {
            "id": "141",
            "name": "14副",
            "departure": "干城站",
            "description": "干城站-神岡區公所",
            "destination": "神岡區公所",
            "seq": 141
          }
        },
        {
          "node": {
            "id": "152",
            "name": "152",
            "departure": "臺中市議會",
            "description": "臺中市議會 - 陽明大樓",
            "destination": "陽明大樓",
            "seq": 1520
          }
        },
        {
          "node": {
            "id": "65158",
            "name": "1633",
            "departure": "臺北轉運站",
            "description": "四湖鄉→北港→台北[經尖山]",
            "destination": "三條崙",
            "seq": 116330
          }
        },
        {
          "node": {
            "id": "65405",
            "name": "6737A",
            "departure": "員客臺中站",
            "description": "西港→臺中[延駛麥寮長庚醫院]",
            "destination": "長庚醫院",
            "seq": 167371
          }
        },
        {
          "node": {
            "id": "215",
            "name": "215",
            "departure": "豐原",
            "description": "豐原-大甲體育場(經麗寶樂園)",
            "destination": "大甲體育場(經麗寶樂園)",
            "seq": 2150
          }
        },
        {
          "node": {
            "id": "65200",
            "name": "1613A",
            "departure": "臺北轉運站",
            "description": "屏東→台北[經三重交流道]",
            "destination": "麟洛站",
            "seq": 116131
          }
        },
        {
          "node": {
            "id": "65132",
            "name": "1828",
            "departure": "臺北轉運站",
            "description": "彰化→臺北",
            "destination": "彰化站",
            "seq": 118280
          }
        },
        {
          "node": {
            "id": "3",
            "name": "3",
            "departure": "東山高中",
            "description": "東山高中  -  高鐵臺中站",
            "destination": "高鐵臺中站",
            "seq": 30
          }
        },
        {
          "node": {
            "id": "65304",
            "name": "5666",
            "departure": "后里國中",
            "description": "三櫃→后里國中",
            "destination": "三櫃",
            "seq": 156660
          }
        },
        {
          "node": {
            "id": "65102",
            "name": "1873A",
            "departure": "朝馬站(國光)",
            "description": "屏東→臺中[屏東至朝馬]",
            "destination": "麟洛站",
            "seq": 118731
          }
        },
        {
          "node": {
            "id": "29",
            "name": "29",
            "departure": "臺中監獄",
            "description": "臺中監獄 - 第一廣場 - 臺中監獄",
            "destination": "第一廣場",
            "seq": 290
          }
        },
        {
          "node": {
            "id": "65528",
            "name": "9120",
            "departure": "台中",
            "description": "竹山→台中",
            "destination": "竹山站",
            "seq": 191200
          }
        },
        {
          "node": {
            "id": "105",
            "name": "105",
            "departure": "四張犁",
            "description": "四張犁 - 臺中車站 - 龍井",
            "destination": "龍井",
            "seq": 1050
          }
        },
        {
          "node": {
            "id": "157",
            "name": "157",
            "departure": "文心森林公園",
            "description": "文心森林公園 - 大甲區公所",
            "destination": "大甲區公所",
            "seq": 1571
          }
        },
        {
          "node": {
            "id": "9560",
            "name": "(本路線捷順交通956舊路線目前已停駛)",
            "departure": "豐原東站",
            "description": "豐原東站 - 光華高工",
            "destination": "光華高工",
            "seq": 9999
          }
        },
        {
          "node": {
            "id": "65529",
            "name": "9012",
            "departure": "松山車站(松山)",
            "description": "臺中市→國道3號→臺北市",
            "destination": "臺中車站",
            "seq": 190120
          }
        },
        {
          "node": {
            "id": "65",
            "name": "65",
            "departure": "南區公所",
            "description": "南區公所  -  潭雅神綠園道",
            "destination": "潭雅神綠園道",
            "seq": 650
          }
        },
        {
          "node": {
            "id": "914",
            "name": "152區",
            "departure": "臺中市議會",
            "description": "臺中市議會 - 圓環南永安街口",
            "destination": "圓環南永安街口",
            "seq": 1521
          }
        },
        {
          "node": {
            "id": "65220",
            "name": "3888G",
            "departure": "臺北轉運站",
            "description": "嘉義→台中→台北",
            "destination": "嘉義站",
            "seq": 138887
          }
        },
        {
          "node": {
            "id": "179",
            "name": "179",
            "departure": "清水車站",
            "description": "清水車站-高美濕地(經三美)",
            "destination": "高美濕地",
            "seq": 1790
          }
        },
        {
          "node": {
            "id": "65124",
            "name": "1832A",
            "departure": "臺北轉運站",
            "description": "埔里→國道1號→臺北[經國道6號]",
            "destination": "埔里站",
            "seq": 118321
          }
        },
        {
          "node": {
            "id": "65388",
            "name": "6899C",
            "departure": "干城站",
            "description": "埔里→臺中[經新庄里]",
            "destination": "埔里站",
            "seq": 168993
          }
        },
        {
          "node": {
            "id": "279",
            "name": "279",
            "departure": "新社",
            "description": "新社  -  水井",
            "destination": "水井",
            "seq": 2790
          }
        },
        {
          "node": {
            "id": "65223",
            "name": "3888",
            "departure": "臺北轉運站",
            "description": "嘉義→台北",
            "destination": "嘉義站",
            "seq": 138880
          }
        },
        {
          "node": {
            "id": "966",
            "name": "266繞",
            "departure": "東勢",
            "description": "東勢  -  谷關",
            "destination": "谷關",
            "seq": 2661
          }
        },
        {
          "node": {
            "id": "3575",
            "name": "(本路線捷順交通357舊路線目前已停駛)",
            "departure": "臺中榮總",
            "description": "臺中榮總(臺灣大道)-中台新村",
            "destination": "中台新村",
            "seq": 9999
          }
        },
        {
          "node": {
            "id": "93",
            "name": "93",
            "departure": "高鐵臺中站",
            "description": "海環線 (高鐵臺中站 - 大甲區銅安厝)",
            "destination": "銅安厝",
            "seq": 930
          }
        },
        {
          "node": {
            "id": "102",
            "name": "102",
            "departure": "干城站",
            "description": "干城站 - 沙鹿",
            "destination": "沙鹿",
            "seq": 1020
          }
        },
        {
          "node": {
            "id": "662",
            "name": "662",
            "departure": "大甲高中(水源路)",
            "description": "大甲高中(水源路) - 大甲車站(中山路) - 苑裡站",
            "destination": "苑裡站",
            "seq": 6620
          }
        },
        {
          "node": {
            "id": "65354",
            "name": "6735",
            "departure": "員客臺中站",
            "description": "二水→臺中",
            "destination": "二水站",
            "seq": 167350
          }
        },
        {
          "node": {
            "id": "65425",
            "name": "9016",
            "departure": "台中轉運站",
            "description": "四湖→西螺交流道→國道1號高速公路→中港交流道→臺中車站",
            "destination": "四湖",
            "seq": 190160
          }
        },
        {
          "node": {
            "id": "156",
            "name": "156",
            "departure": "臺中國際機場",
            "description": "臺中國際機場 - 高鐵臺中站",
            "destination": "高鐵臺中站",
            "seq": 1560
          }
        },
        {
          "node": {
            "id": "65197",
            "name": "1615A",
            "departure": "臺北轉運站",
            "description": "彰化→台北[不停朝馬站]",
            "destination": "彰化站",
            "seq": 116151
          }
        },
        {
          "node": {
            "id": "959",
            "name": "219延",
            "departure": "石岡國中",
            "description": "石岡國中 - 上龍興里",
            "destination": "上龍興里",
            "seq": 2191
          }
        },
        {
          "node": {
            "id": "65203",
            "name": "1612B",
            "departure": "市府轉運站",
            "description": "臺北→國道3號→臺南[經安坑交流道，不停三峽站、北港路站]",
            "destination": "兵配廠",
            "seq": 116122
          }
        },
        {
          "node": {
            "id": "940",
            "name": "306區2",
            "departure": "沙鹿站",
            "description": "巨業沙鹿站-梧棲-清水",
            "destination": "清水(經梧棲)",
            "seq": 3064
          }
        },
        {
          "node": {
            "id": "359",
            "name": "359",
            "departure": "東海別墅",
            "description": "東海別墅-文心森林公園",
            "destination": "文心森林公園",
            "seq": 3590
          }
        },
        {
          "node": {
            "id": "65216",
            "name": "3999",
            "departure": "臺北轉運站",
            "description": "高雄→台北",
            "destination": "高雄總站",
            "seq": 139990
          }
        },
        {
          "node": {
            "id": "65156",
            "name": "1633B",
            "departure": "臺北轉運站",
            "description": "四湖鄉→北港→台北[北港至台北班次]",
            "destination": "北港站",
            "seq": 116332
          }
        },
        {
          "node": {
            "id": "65145",
            "name": "1656",
            "departure": "板橋客運站",
            "description": "屏東→國道3號→板橋",
            "destination": "屏東轉運站",
            "seq": 116560
          }
        },
        {
          "node": {
            "id": "208",
            "name": "208",
            "departure": "豐原",
            "description": "豐原  -  卓蘭(東勢)",
            "destination": "卓蘭",
            "seq": 2080
          }
        },
        {
          "node": {
            "id": "260",
            "name": "260",
            "departure": "東勢",
            "description": "東勢  -  水尾(石城、明正里)",
            "destination": "水尾",
            "seq": 2600
          }
        },
        {
          "node": {
            "id": "65428",
            "name": "9010A",
            "departure": "臺中車站",
            "description": "新竹市→國道１號→臺中市[經大雅交流道]",
            "destination": "新竹轉運站",
            "seq": 190101
          }
        },
        {
          "node": {
            "id": "325",
            "name": "325",
            "departure": "大肚車站",
            "description": "大肚車站-臺中車站",
            "destination": "臺中車站",
            "seq": 3250
          }
        },
        {
          "node": {
            "id": "158",
            "name": "158",
            "departure": "高鐵臺中站",
            "description": "高鐵臺中站 - 朝陽科技大學",
            "destination": "朝陽科技大學",
            "seq": 1580
          }
        },
        {
          "node": {
            "id": "19",
            "name": "19",
            "departure": "慈濟醫院",
            "description": "慈濟醫院 - 旱溪 - 國立臺灣美術館",
            "destination": "國立臺灣美術館",
            "seq": 190
          }
        },
        {
          "node": {
            "id": "675",
            "name": "677區",
            "departure": "二號橋",
            "description": "沙鹿  -  二號橋",
            "destination": "沙鹿",
            "seq": 6771
          }
        },
        {
          "node": {
            "id": "65135",
            "name": "1826A",
            "departure": "",
            "description": "臺中→中清路→臺北[水湳至台北]",
            "destination": "",
            "seq": 118261
          }
        },
        {
          "node": {
            "id": "67",
            "name": "67",
            "departure": "秀泰影城",
            "description": "秀泰影城-東海街口",
            "destination": "東海街口",
            "seq": 670
          }
        },
        {
          "node": {
            "id": "270",
            "name": "270",
            "departure": "豐富公園",
            "description": "豐富公園-東勢(經大南)",
            "destination": "東勢",
            "seq": 2700
          }
        },
        {
          "node": {
            "id": "905",
            "name": "305E",
            "departure": "大甲",
            "description": "大甲-鹿寮-臺中車站",
            "destination": "臺中車站",
            "seq": 3052
          }
        },
        {
          "node": {
            "id": "65404",
            "name": "9120A",
            "departure": "台中",
            "description": "竹山→台中[經南投高中]",
            "destination": "竹山站",
            "seq": 191201
          }
        },
        {
          "node": {
            "id": "275",
            "name": "275",
            "departure": "東勢",
            "description": "東勢  -  中和",
            "destination": "中和",
            "seq": 2750
          }
        },
        {
          "node": {
            "id": "170",
            "name": "170",
            "departure": "大甲體育場",
            "description": "大甲體育場-梧棲",
            "destination": "梧棲",
            "seq": 1700
          }
        },
        {
          "node": {
            "id": "280",
            "name": "280",
            "departure": "臺中二中",
            "description": "臺中二中 - 修平科技大學",
            "destination": "修平科技大學",
            "seq": 2800
          }
        },
        {
          "node": {
            "id": "65173",
            "name": "1621D",
            "departure": "臺中車站",
            "description": "高雄→烏日→台中[不停岡山站、彰化交流道站]",
            "destination": "高雄火車站",
            "seq": 116214
          }
        },
        {
          "node": {
            "id": "65134",
            "name": "1827A",
            "departure": "",
            "description": "臺中→臺灣大道→臺北[朝馬至台北]",
            "destination": "",
            "seq": 118271
          }
        },
        {
          "node": {
            "id": "92",
            "name": "92",
            "departure": "豐原",
            "description": "豐原 - 大安國中",
            "destination": "大安國中",
            "seq": 920
          }
        },
        {
          "node": {
            "id": "65191",
            "name": "1617B",
            "departure": "臺北轉運站",
            "description": "東勢→豐原→台北[豐原至臺北班次，不停麗寶樂園(月眉)站、林口站]",
            "destination": "豐原站",
            "seq": 116172
          }
        },
        {
          "node": {
            "id": "932",
            "name": "105區1",
            "departure": "新興里",
            "description": "新興里-四張犁",
            "destination": "四張犁",
            "seq": 1051
          }
        },
        {
          "node": {
            "id": "357",
            "name": "357",
            "departure": "臺中榮總",
            "description": "臺中榮總(臺灣大道)-中台新村",
            "destination": "中台新村",
            "seq": 3570
          }
        },
        {
          "node": {
            "id": "616",
            "name": "616",
            "departure": "臺中港郵局",
            "description": "臺中港郵局 - 大甲銅安厝",
            "destination": "大甲銅安厝",
            "seq": 6160
          }
        },
        {
          "node": {
            "id": "900",
            "name": "900",
            "departure": "光明國中 ",
            "description": "光明國中 - 豐原高中",
            "destination": "豐原高中",
            "seq": 9000
          }
        },
        {
          "node": {
            "id": "65412",
            "name": "6933A",
            "departure": "鹿港乘車處",
            "description": "高鐵臺中站→彰化→鹿港",
            "destination": "高鐵臺中站",
            "seq": 169331
          }
        },
        {
          "node": {
            "id": "65207",
            "name": "1611A",
            "departure": "臺北轉運站",
            "description": "台南→台北[經三重交流道]",
            "destination": "兵配廠",
            "seq": 116111
          }
        },
        {
          "node": {
            "id": "151",
            "name": "151",
            "departure": "臺中市議會",
            "description": "臺中市議會 - 朝陽科技大學",
            "destination": "朝陽科技大學",
            "seq": 1510
          }
        },
        {
          "node": {
            "id": "65192",
            "name": "1617A",
            "departure": "臺北轉運站",
            "description": "東勢→豐原→台北[不停麗寶樂園(月眉)站、林口站]",
            "destination": "東勢站",
            "seq": 116171
          }
        },
        {
          "node": {
            "id": "180",
            "name": "180",
            "departure": "沙鹿",
            "description": "沙鹿-彰化",
            "destination": "彰化",
            "seq": 1800
          }
        },
        {
          "node": {
            "id": "161",
            "name": "161",
            "departure": "高鐵臺中站",
            "description": "高鐵臺中站  -  中科管理局",
            "destination": "中科管理局",
            "seq": 1610
          }
        },
        {
          "node": {
            "id": "65137",
            "name": "1806A",
            "departure": "基隆站(國光)",
            "description": "南投→基隆[繞駛公訓所中訓中心]",
            "destination": "南投車站",
            "seq": 118061
          }
        },
        {
          "node": {
            "id": "281",
            "name": "281",
            "departure": "新建國市場",
            "description": "新建國市場 - 霧峰農工",
            "destination": "霧峰農工",
            "seq": 2810
          }
        },
        {
          "node": {
            "id": "14200",
            "name": "(本路線台中客運142路舊路線已停駛)",
            "departure": "豐年社區",
            "description": "豐年社區三站-第一廣場-豐年社區一站",
            "destination": "第一廣場",
            "seq": 100000
          }
        },
        {
          "node": {
            "id": "65171",
            "name": "1623",
            "departure": "臺中車站",
            "description": "臺灣桃園國際機場→國道1號→臺中市",
            "destination": "第一航廈",
            "seq": 116230
          }
        },
        {
          "node": {
            "id": "411",
            "name": "310區2",
            "departure": "靜宜大學",
            "description": "臺中港旅客服務中心 - 臺中車站 - 靜宜大學",
            "destination": "臺中車站",
            "seq": 3102
          }
        },
        {
          "node": {
            "id": "943",
            "name": "20區",
            "departure": "干城",
            "description": "干城 - 東山高中",
            "destination": "東山高中",
            "seq": 201
          }
        },
        {
          "node": {
            "id": "65202",
            "name": "1612C",
            "departure": "景美站",
            "description": "臺北→國道3號→臺南[經安坑交流道，景美至臺南班次、不停北港路站]",
            "destination": "兵配廠",
            "seq": 116123
          }
        },
        {
          "node": {
            "id": "178",
            "name": "178",
            "departure": "清水車站",
            "description": "清水車站-高美濕地(經高美)",
            "destination": "高美濕地",
            "seq": 1780
          }
        },
        {
          "node": {
            "id": "65235",
            "name": "7500R",
            "departure": "北門總站",
            "description": "台北→台中→台南",
            "destination": "臺北轉運站",
            "seq": 175001
          }
        },
        {
          "node": {
            "id": "271",
            "name": "271",
            "departure": "豐富公園",
            "description": "豐富公園-東勢(經東興)",
            "destination": "東勢",
            "seq": 2710
          }
        },
        {
          "node": {
            "id": "250",
            "name": "250",
            "departure": "東勢",
            "description": "東勢  -  中坑坪(石角)",
            "destination": "中坑坪",
            "seq": 2500
          }
        },
        {
          "node": {
            "id": "65206",
            "name": "1611",
            "departure": "臺北轉運站",
            "description": "台南→台北[經臺北交流道]",
            "destination": "兵配廠",
            "seq": 116110
          }
        },
        {
          "node": {
            "id": "266",
            "name": "266",
            "departure": "東勢",
            "description": "東勢  -  谷關",
            "destination": "谷關",
            "seq": 2660
          }
        },
        {
          "node": {
            "id": "306",
            "name": "306",
            "departure": "清水",
            "description": "清水-梧棲-臺中車站",
            "destination": "臺中車站(經梧棲)",
            "seq": 3060
          }
        },
        {
          "node": {
            "id": "83",
            "name": "83",
            "departure": "捷運文心森林公園站",
            "description": "捷運文心森林公園站-仁友停車場",
            "destination": "仁友停車場",
            "seq": 830
          }
        },
        {
          "node": {
            "id": "65130",
            "name": "1830",
            "departure": "臺北轉運站",
            "description": "北斗→臺北",
            "destination": "明道大學",
            "seq": 118300
          }
        },
        {
          "node": {
            "id": "186",
            "name": "186",
            "departure": "豐原",
            "description": "豐原  -  臺中港郵局(經東山)",
            "destination": "臺中港郵局(經東山)",
            "seq": 1860
          }
        },
        {
          "node": {
            "id": "98",
            "name": "98",
            "departure": "神岡庄尾",
            "description": "神岡庄尾 - 臺中榮總 - 普濟寺",
            "destination": "普濟寺",
            "seq": 980
          }
        },
        {
          "node": {
            "id": "65411",
            "name": "6936",
            "departure": "高鐵臺中站",
            "description": "鹿港北區遊客中心→高鐵臺中站",
            "destination": "臺灣玻璃博物館",
            "seq": 169360
          }
        },
        {
          "node": {
            "id": "263",
            "name": "263",
            "departure": "東勢",
            "description": "東勢  -  崑山(土牛)",
            "destination": "崑山",
            "seq": 2630
          }
        },
        {
          "node": {
            "id": "65143",
            "name": "1660",
            "departure": "板橋客運站",
            "description": "高雄→板橋(板橋客運站)",
            "destination": "中華站",
            "seq": 116600
          }
        },
        {
          "node": {
            "id": "65177",
            "name": "1621",
            "departure": "臺中車站",
            "description": "高雄→烏日→台中",
            "destination": "高雄火車站",
            "seq": 116210
          }
        },
        {
          "node": {
            "id": "39",
            "name": "39",
            "departure": "高鐵臺中站",
            "description": "高鐵臺中站-立新國小",
            "destination": "立新國小",
            "seq": 390
          }
        },
        {
          "node": {
            "id": "65150",
            "name": "1638",
            "departure": "臺北轉運站",
            "description": "東石→朴子→台北",
            "destination": "東石",
            "seq": 116380
          }
        },
        {
          "node": {
            "id": "65214",
            "name": "1610",
            "departure": "臺北轉運站",
            "description": "高雄→台北[經臺北交流道]",
            "destination": "中華站",
            "seq": 116100
          }
        },
        {
          "node": {
            "id": "49",
            "name": "49",
            "departure": "老虎城",
            "description": "老虎城 - 協和國小分校",
            "destination": "協和國小分校",
            "seq": 490
          }
        },
        {
          "node": {
            "id": "131",
            "name": "131",
            "departure": "北屯區行政大樓",
            "description": "北屯區行政大樓  - 朝陽科技大學",
            "destination": "朝陽科技大學",
            "seq": 1310
          }
        },
        {
          "node": {
            "id": "3510",
            "name": "351",
            "departure": "統聯轉運站",
            "description": "統聯轉運站-臺中工業區",
            "destination": "臺中工業區",
            "seq": 3510
          }
        },
        {
          "node": {
            "id": "123",
            "name": "123",
            "departure": "臺中慈濟醫院",
            "description": "臺中慈濟醫院-梧棲觀光漁港",
            "destination": "梧棲觀光漁港",
            "seq": 1230
          }
        },
        {
          "node": {
            "id": "65114",
            "name": "1862",
            "departure": "桃園站(國光)",
            "description": "高雄→桃園",
            "destination": "高雄站(國光)",
            "seq": 118620
          }
        },
        {
          "node": {
            "id": "82",
            "name": "82",
            "departure": "水湳",
            "description": "水湳  -  高鐵臺中站",
            "destination": "高鐵臺中站",
            "seq": 820
          }
        },
        {
          "node": {
            "id": "812",
            "name": "812",
            "departure": "豐原東站 ",
            "description": "豐原東站 - 后里東站",
            "destination": "后里東站",
            "seq": 8120
          }
        },
        {
          "node": {
            "id": "20",
            "name": "20",
            "departure": "干城站",
            "description": "干城站 - 潭子車站",
            "destination": "潭子車站",
            "seq": 200
          }
        },
        {
          "node": {
            "id": "975",
            "name": "275區",
            "departure": "下新社",
            "description": "下新社  -  中和",
            "destination": "中和",
            "seq": 2751
          }
        },
        {
          "node": {
            "id": "65141",
            "name": "1751",
            "departure": "宜蘭轉運站",
            "description": "梨山→宜蘭",
            "destination": "梨山",
            "seq": 117510
          }
        },
        {
          "node": {
            "id": "65491",
            "name": "6933",
            "departure": "鹿港乘車處",
            "description": "台中→彰化→鹿港",
            "destination": "臺中",
            "seq": 169330
          }
        },
        {
          "node": {
            "id": "668",
            "name": "668",
            "departure": "日南車站",
            "description": "日南車站 - 大甲區公所",
            "destination": "大甲區公所",
            "seq": 6680
          }
        },
        {
          "node": {
            "id": "9211",
            "name": "921",
            "departure": "豐原東站",
            "description": "豐原東站 - 軍功松竹路口",
            "destination": "軍功松竹路口",
            "seq": 9210
          }
        },
        {
          "node": {
            "id": "1551",
            "name": "155副",
            "departure": "高鐵臺中站",
            "description": "高鐵臺中站 - 麗寶樂園",
            "destination": "麗寶樂園",
            "seq": 1551
          }
        },
        {
          "node": {
            "id": "160",
            "name": "160",
            "departure": "高鐵臺中站",
            "description": "高鐵臺中站 - 文修停車場",
            "destination": "文修停車場",
            "seq": 1600
          }
        },
        {
          "node": {
            "id": "220",
            "name": "220",
            "departure": "豐原",
            "description": "豐原  -  臺中榮總",
            "destination": "臺中榮總",
            "seq": 2200
          }
        },
        {
          "node": {
            "id": "65139",
            "name": "1805",
            "departure": "基隆站(國光)",
            "description": "臺中→基隆",
            "destination": "國光客運臺中車站",
            "seq": 118050
          }
        },
        {
          "node": {
            "id": "53",
            "name": "53",
            "departure": "太原車站",
            "description": "太原車站 - 省議會",
            "destination": "省議會",
            "seq": 530
          }
        },
        {
          "node": {
            "id": "239",
            "name": "239",
            "departure": "豐原",
            "description": "豐原  -  梧棲",
            "destination": "梧棲站",
            "seq": 2390
          }
        },
        {
          "node": {
            "id": "65099",
            "name": "7504B",
            "departure": "板橋客運站",
            "description": "台中(朝馬)→中壢→板橋",
            "destination": "朝馬站",
            "seq": 175042
          }
        },
        {
          "node": {
            "id": "65391",
            "name": "6670C",
            "departure": "臺中",
            "description": "日月潭→埔里→高鐵臺中站→臺中[經國道6號、繞駛中台禪寺]",
            "destination": "日月潭",
            "seq": 166703
          }
        },
        {
          "node": {
            "id": "283",
            "name": "283",
            "departure": "省議會",
            "description": "霧峰-大里仁愛醫院",
            "destination": " 大里仁愛醫院",
            "seq": 2830
          }
        },
        {
          "node": {
            "id": "65105",
            "name": "1871",
            "departure": "國光客運臺中車站",
            "description": "臺南→臺中",
            "destination": "兵配廠",
            "seq": 118710
          }
        },
        {
          "node": {
            "id": "6581",
            "name": "658延",
            "departure": "海墘國小",
            "description": "海墘國小-惠文高中(惠中路)",
            "destination": "惠文高中",
            "seq": 6581
          }
        },
        {
          "node": {
            "id": "65181",
            "name": "1619B",
            "departure": "臺北轉運站",
            "description": "台中→中港路→台北[不停竹科站]",
            "destination": "臺中車站",
            "seq": 116192
          }
        },
        {
          "node": {
            "id": "65110",
            "name": "1866A",
            "departure": "元培大學",
            "description": "臺中→新竹[經元培大學]",
            "destination": "朝馬站(國光)",
            "seq": 118661
          }
        },
        {
          "node": {
            "id": "581",
            "name": "58區1",
            "departure": "大慶活動中心",
            "description": "大慶活動中心  - 北屯區行政大樓",
            "destination": "北屯區行政大樓",
            "seq": 582
          }
        },
        {
          "node": {
            "id": "290",
            "name": "290",
            "departure": "干城站 ",
            "description": "干城站 -童綜合醫院(梧棲院區)",
            "destination": "童綜合醫院(梧棲院區)",
            "seq": 2900
          }
        },
        {
          "node": {
            "id": "326",
            "name": "326",
            "departure": "靜宜大學",
            "description": "靜宜大學-新民高中",
            "destination": "新民高中",
            "seq": 3260
          }
        },
        {
          "node": {
            "id": "451",
            "name": "45延",
            "departure": "中科實驗高中(平和路)",
            "description": "中科實驗高中(平和路)-第一廣場-中科實驗高中(平和路)",
            "destination": "干城站",
            "seq": 451
          }
        },
        {
          "node": {
            "id": "2507",
            "name": "黃7",
            "departure": "省議會",
            "description": "省議會 - 舊正里福德祠",
            "destination": "舊正里福德祠",
            "seq": 10000
          }
        },
        {
          "node": {
            "id": "26",
            "name": "26",
            "departure": "新民高中",
            "description": "新民高中  -  嶺東科技大學",
            "destination": "嶺東科技大學",
            "seq": 260
          }
        },
        {
          "node": {
            "id": "3541",
            "name": "354",
            "departure": "東海路思義教堂",
            "description": "東海路思義教堂 - 文修停車場",
            "destination": "文修停車場",
            "seq": 3540
          }
        },
        {
          "node": {
            "id": "65172",
            "name": "1623A",
            "departure": "朝馬站",
            "description": "臺灣桃園國際機場→國道1號→臺中市[機場至朝馬]",
            "destination": "第一航廈",
            "seq": 116231
          }
        },
        {
          "node": {
            "id": "207",
            "name": "207",
            "departure": "豐原",
            "description": "豐原  -  谷關",
            "destination": "谷關",
            "seq": 2070
          }
        },
        {
          "node": {
            "id": "960",
            "name": "220繞",
            "departure": "豐原",
            "description": "豐原 - 大華國中 - 臺中榮總",
            "destination": "臺中榮總",
            "seq": 2201
          }
        },
        {
          "node": {
            "id": "866",
            "name": "866",
            "departure": "天池達觀亭",
            "description": "天池達觀亭 - 武陵農場",
            "destination": "武陵農場",
            "seq": 8660
          }
        },
        {
          "node": {
            "id": "2821",
            "name": "282副",
            "departure": "亞洲大學安藤館",
            "description": "亞洲大學安藤館-桐林",
            "destination": "桐林",
            "seq": 2821
          }
        },
        {
          "node": {
            "id": "285",
            "name": "285",
            "departure": "新建國市場",
            "description": "新建國市場-銀聯一村",
            "destination": "銀聯一村",
            "seq": 2850
          }
        },
        {
          "node": {
            "id": "264",
            "name": "264",
            "departure": "東勢",
            "description": "東勢  -  東興  -  新五村",
            "destination": "新五村",
            "seq": 2640
          }
        },
        {
          "node": {
            "id": "261",
            "name": "261",
            "departure": "東勢",
            "description": "東勢  -  明正里",
            "destination": "明正里",
            "seq": 2610
          }
        },
        {
          "node": {
            "id": "65136",
            "name": "1826",
            "departure": "臺北轉運站",
            "description": "臺中→中清路→臺北",
            "destination": "國光客運臺中車站",
            "seq": 118260
          }
        },
        {
          "node": {
            "id": "65204",
            "name": "1612A",
            "departure": "市府轉運站",
            "description": "臺南→國道3號→臺北[經萬芳交流道]",
            "destination": "兵配廠",
            "seq": 116121
          }
        },
        {
          "node": {
            "id": "839",
            "name": "839",
            "departure": "泰安車站",
            "description": "泰安車站 - 國立苑裡高中",
            "destination": "國立苑裡高中",
            "seq": 8390
          }
        },
        {
          "node": {
            "id": "5",
            "name": "5",
            "departure": "臺中車站(民族路口)",
            "description": "臺中車站(民族路口) - 僑光科技大學",
            "destination": "僑光科技大學",
            "seq": 50
          }
        },
        {
          "node": {
            "id": "582",
            "name": "58區2",
            "departure": "明德高中",
            "description": "明德高中  -  潭子勝利運動公園",
            "destination": "潭子勝利運動公園",
            "seq": 583
          }
        },
        {
          "node": {
            "id": "356",
            "name": "356",
            "departure": "國安社區(宜寧中學)",
            "description": "國安社區(宜寧中學) -大慶車站",
            "destination": "大慶車站",
            "seq": 3560
          }
        },
        {
          "node": {
            "id": "66",
            "name": "66",
            "departure": "慈濟醫院-右環",
            "description": "左環(往天星大飯店)-右環(往亞哥花園)",
            "destination": "天星大飯店-左環",
            "seq": 660
          }
        },
        {
          "node": {
            "id": "59",
            "name": "59",
            "departure": "捷運松竹站",
            "description": "捷運松竹站 - 舊正",
            "destination": "舊正",
            "seq": 590
          }
        },
        {
          "node": {
            "id": "60",
            "name": "60",
            "departure": "大智公園(立德東街)",
            "description": "坪頂-大智公園(立德東街)",
            "destination": "坪頂",
            "seq": 600
          }
        },
        {
          "node": {
            "id": "202",
            "name": "202",
            "departure": "豐富公園(經樹德路、慈濟)",
            "description": "豐富公園-豐原(經樹德路)",
            "destination": "豐原(經樹德路、慈濟)",
            "seq": 2020
          }
        },
        {
          "node": {
            "id": "172",
            "name": "172",
            "departure": "大甲",
            "description": "大甲  -  福住里",
            "destination": "福住里",
            "seq": 1720
          }
        },
        {
          "node": {
            "id": "210",
            "name": "210",
            "departure": "大甲車站",
            "description": "大甲車站  -  土城 ",
            "destination": "土城",
            "seq": 2100
          }
        },
        {
          "node": {
            "id": "63",
            "name": "63",
            "departure": "豐原",
            "description": "豐原 - 臺中榮總",
            "destination": "臺中榮總",
            "seq": 630
          }
        },
        {
          "node": {
            "id": "65144",
            "name": "1657",
            "departure": "高鐵臺中站",
            "description": "南投→國道3號→高鐵臺中站",
            "destination": "稅捐處",
            "seq": 116570
          }
        },
        {
          "node": {
            "id": "661",
            "name": "661",
            "departure": "致用高中-右環",
            "description": "致用高中 - 大甲車站 - 永安國小",
            "destination": "致用高中-左環",
            "seq": 6610
          }
        },
        {
          "node": {
            "id": "65228",
            "name": "7500",
            "departure": "北門總站",
            "description": "臺北市→國道1號→臺南市(經三重交流道)",
            "destination": "臺北轉運站",
            "seq": 175000
          }
        },
        {
          "node": {
            "id": "937",
            "name": "306繞",
            "departure": "清水",
            "description": "清水-梧棲-臺中車站",
            "destination": "臺中車站(經梧棲)",
            "seq": 3065
          }
        },
        {
          "node": {
            "id": "65368",
            "name": "6670G",
            "departure": "臺中",
            "description": "埔里→草屯→高鐵臺中站→臺中",
            "destination": "埔里站",
            "seq": 166707
          }
        },
        {
          "node": {
            "id": "206",
            "name": "206",
            "departure": "豐原",
            "description": "豐原  -  東勢高工",
            "destination": "東勢高工",
            "seq": 2060
          }
        },
        {
          "node": {
            "id": "65224",
            "name": "7511",
            "departure": "臺北轉運站",
            "description": "臺中港→國道1號→臺北市",
            "destination": "臺中港旅客服務中心站",
            "seq": 175110
          }
        },
        {
          "node": {
            "id": "65369",
            "name": "6670F",
            "departure": "臺中",
            "description": "日月潭→高鐵臺中站→臺中[台灣好行日月潭線延駛向山行政中心]",
            "destination": "向山行政中心",
            "seq": 166706
          }
        },
        {
          "node": {
            "id": "912",
            "name": "21延1",
            "departure": "臺中刑務所演武場",
            "description": "臺中刑務所演武場-民德橋",
            "destination": "民德橋",
            "seq": 211
          }
        },
        {
          "node": {
            "id": "65106",
            "name": "1871A",
            "departure": "朝馬站(國光)",
            "description": "臺南→臺中[臺南至朝馬]",
            "destination": "兵配廠",
            "seq": 118711
          }
        },
        {
          "node": {
            "id": "91",
            "name": "91",
            "departure": "舊庄",
            "description": "舊庄 - 中興嶺停車場",
            "destination": "中興嶺停車場",
            "seq": 910
          }
        },
        {
          "node": {
            "id": "850",
            "name": "850",
            "departure": "臺中車站",
            "description": "臺中車站-谷關",
            "destination": "谷關",
            "seq": 8500
          }
        },
        {
          "node": {
            "id": "65108",
            "name": "1870",
            "departure": "國光客運臺中車站",
            "description": "嘉義→臺中",
            "destination": "嘉義市轉運中心",
            "seq": 118700
          }
        },
        {
          "node": {
            "id": "939",
            "name": "306區1",
            "departure": "靜宜大學",
            "description": "靜宜大學-梧棲-清水",
            "destination": "清水(經梧棲)",
            "seq": 3063
          }
        },
        {
          "node": {
            "id": "185",
            "name": "185",
            "departure": "豐原",
            "description": "豐原  -  清水(東山)",
            "destination": "清水(經東山)",
            "seq": 1850
          }
        },
        {
          "node": {
            "id": "989",
            "name": "989",
            "departure": "翁子",
            "description": "翁子-圳前仁愛公園",
            "destination": "圳前仁愛公園",
            "seq": 9890
          }
        },
        {
          "node": {
            "id": "947",
            "name": "617",
            "departure": "臺中港旅客服務中心",
            "description": "臺中港旅客服務中心 - 仁友停車場",
            "destination": "仁友停車場",
            "seq": 6170
          }
        },
        {
          "node": {
            "id": "751",
            "name": "75區1",
            "departure": "臺中女中",
            "description": "臺中女中 - 一江橋",
            "destination": "一江橋",
            "seq": 751
          }
        },
        {
          "node": {
            "id": "15",
            "name": "15",
            "departure": "臺中刑務所演武場",
            "description": "臺中刑務所演武場 -  國軍臺中總醫院",
            "destination": "國軍臺中總醫院",
            "seq": 150
          }
        },
        {
          "node": {
            "id": "9811",
            "name": "(本路線捷順交通811舊路線目前已停駛)",
            "departure": "豐原東站",
            "description": "豐原東站 - 大甲體育場",
            "destination": "大甲體育場",
            "seq": 9999
          }
        },
        {
          "node": {
            "id": "65533",
            "name": "6322",
            "departure": "干城站",
            "description": "水里→南崗→臺中",
            "destination": "水里",
            "seq": 163220
          }
        },
        {
          "node": {
            "id": "901",
            "name": "901",
            "departure": "豐原",
            "description": "豐原 - 明德高中",
            "destination": "明德高中",
            "seq": 9010
          }
        },
        {
          "node": {
            "id": "8995",
            "name": "(本路線捷順交通199延舊路線目前已停駛)",
            "departure": "龍津高中",
            "description": "龍津高中-文修停車場",
            "destination": "文修停車場",
            "seq": 9999
          }
        },
        {
          "node": {
            "id": "611",
            "name": "211區",
            "departure": "泰安",
            "description": "泰安 - 新政大安港路口",
            "destination": "新政大安港路口",
            "seq": 2111
          }
        },
        {
          "node": {
            "id": "672",
            "name": "272繞",
            "departure": "東勢",
            "description": "東勢-水井-茄苳寮",
            "destination": "茄苳寮",
            "seq": 2721
          }
        },
        {
          "node": {
            "id": "246",
            "name": "246",
            "departure": "秀泰影城",
            "description": "秀泰影城 - 國軍臺中總醫院",
            "destination": "國軍臺中總醫院",
            "seq": 2460
          }
        },
        {
          "node": {
            "id": "65199",
            "name": "1613",
            "departure": "臺北轉運站",
            "description": "屏東→台北[經臺北交流道]",
            "destination": "麟洛站",
            "seq": 116130
          }
        },
        {
          "node": {
            "id": "65109",
            "name": "1866",
            "departure": "新竹轉運站",
            "description": "臺中→新竹",
            "destination": "國光客運臺中車站",
            "seq": 118660
          }
        },
        {
          "node": {
            "id": "65180",
            "name": "1620A",
            "departure": "臺北轉運站",
            "description": "台中→中清路→台北[不停新竹站]",
            "destination": "臺中車站",
            "seq": 116201
          }
        },
        {
          "node": {
            "id": "65371",
            "name": "6882A",
            "departure": "",
            "description": "西螺→臺中[經明道大學]",
            "destination": "",
            "seq": 168821
          }
        },
        {
          "node": {
            "id": "3595",
            "name": "(本路線捷順交通359舊路線目前已停駛)",
            "departure": "東海別墅",
            "description": "東海別墅-文心森林公園",
            "destination": "文心森林公園",
            "seq": 9999
          }
        },
        {
          "node": {
            "id": "3565",
            "name": "(本路線捷順交通356舊路線目前已停駛)",
            "departure": "國安社區(宜寧中學)",
            "description": "國安社區(宜寧中學) -大慶車站",
            "destination": "大慶車站",
            "seq": 9999
          }
        },
        {
          "node": {
            "id": "5901",
            "name": "59延",
            "departure": "捷運松竹站",
            "description": "捷運松竹站 - 舊正社區停車場",
            "destination": "舊正社區停車場",
            "seq": 591
          }
        },
        {
          "node": {
            "id": "8663",
            "name": "866區3",
            "departure": "天池達觀亭",
            "description": "天池達觀亭-福壽山農場",
            "destination": "福壽山農場",
            "seq": 8663
          }
        },
        {
          "node": {
            "id": "65230",
            "name": "7500W",
            "departure": "北門總站",
            "description": "台北→台中→新營→台南",
            "destination": "臺北轉運站",
            "seq": 175004
          }
        },
        {
          "node": {
            "id": "52",
            "name": "52",
            "departure": "中興大學-左環",
            "description": "忠明進化建成線",
            "destination": "中興大學-右環",
            "seq": 520
          }
        },
        {
          "node": {
            "id": "65394",
            "name": "6670",
            "departure": "臺中",
            "description": "日月潭→埔里→草屯→高鐵臺中站→臺中",
            "destination": "日月潭",
            "seq": 166700
          }
        },
        {
          "node": {
            "id": "813",
            "name": "813",
            "departure": "后里車站",
            "description": "后里東站 - 文武公園",
            "destination": "文武公園",
            "seq": 8130
          }
        },
        {
          "node": {
            "id": "65205",
            "name": "1612",
            "departure": "市府轉運站",
            "description": "臺南→國道3號→臺北[經安坑交流道]",
            "destination": "兵配廠",
            "seq": 116120
          }
        },
        {
          "node": {
            "id": "216",
            "name": "216",
            "departure": "大甲",
            "description": "大甲  -  南埔",
            "destination": "南埔",
            "seq": 2160
          }
        },
        {
          "node": {
            "id": "129",
            "name": "128區",
            "departure": "大雅",
            "description": "大雅 - 梧棲",
            "destination": "梧棲",
            "seq": 1281
          }
        },
        {
          "node": {
            "id": "17",
            "name": "17",
            "departure": "草湖",
            "description": "草湖 - 霧峰農工 - 霧峰復興八街",
            "destination": "霧峰復興八街",
            "seq": 170
          }
        },
        {
          "node": {
            "id": "453",
            "name": "866區1",
            "departure": "福壽山農場",
            "description": "福壽山農場 - 梨山",
            "destination": "梨山",
            "seq": 8661
          }
        },
        {
          "node": {
            "id": "1811",
            "name": "181區1",
            "departure": "苑裡站",
            "description": "苑裡站  -  日南國中",
            "destination": "日南國中",
            "seq": 1811
          }
        },
        {
          "node": {
            "id": "155",
            "name": "155",
            "departure": "高鐵臺中站",
            "description": "高鐵臺中站 - 麗寶樂園",
            "destination": "麗寶樂園",
            "seq": 1550
          }
        },
        {
          "node": {
            "id": "681",
            "name": "68",
            "departure": "坪頂",
            "description": "坪頂 - 中臺科技大學 - 新桃花源橋",
            "destination": "中臺科技大學",
            "seq": 680
          }
        },
        {
          "node": {
            "id": "301",
            "name": "301",
            "departure": "靜宜大學",
            "description": "靜宜大學-新民高中",
            "destination": "新民高中",
            "seq": 3010
          }
        },
        {
          "node": {
            "id": "658",
            "name": "658",
            "departure": "大安濱海樂園",
            "description": "大安濱海樂園-惠文高中(惠文路)",
            "destination": "惠文高中(惠文路)",
            "seq": 6580
          }
        },
        {
          "node": {
            "id": "85",
            "name": "85",
            "departure": "新光里(新福路)",
            "description": "新光里 - 太原車站 - 一江橋",
            "destination": "一江橋",
            "seq": 850
          }
        },
        {
          "node": {
            "id": "95",
            "name": "95",
            "departure": "六福公園",
            "description": "六福公園 - 外埔老人文康中心",
            "destination": "外埔老人文康中心",
            "seq": 950
          }
        },
        {
          "node": {
            "id": "1541",
            "name": "154延",
            "departure": "臺中女中",
            "description": "臺中女中 - 頂店",
            "destination": "頂店",
            "seq": 1541
          }
        },
        {
          "node": {
            "id": "65125",
            "name": "1832",
            "departure": "臺北轉運站",
            "description": "埔里→臺北",
            "destination": "埔里站",
            "seq": 118320
          }
        },
        {
          "node": {
            "id": "65163",
            "name": "1630",
            "departure": "臺北轉運站",
            "description": "西港→台北",
            "destination": "西港站",
            "seq": 116300
          }
        },
        {
          "node": {
            "id": "3521",
            "name": "352延",
            "departure": "大肚",
            "description": "大肚-中科實驗高中(平和路)",
            "destination": "中科實驗高中(平和路)",
            "seq": 3521
          }
        },
        {
          "node": {
            "id": "65427",
            "name": "9010C",
            "departure": "臺中車站",
            "description": "新竹市→國道１號→臺中市 [繞駛中華大學]",
            "destination": "新竹轉運站",
            "seq": 190103
          }
        },
        {
          "node": {
            "id": "65437",
            "name": "6268D",
            "departure": "干城站",
            "description": "埔里→臺中(地理中心碑)[繞駛北山坑]",
            "destination": "地理中心碑",
            "seq": 162684
          }
        },
        {
          "node": {
            "id": "70",
            "name": "70",
            "departure": "嶺東科技大學",
            "description": "嶺東科技大學 - 第一廣場 - 嶺東科技大學",
            "destination": "第一廣場",
            "seq": 700
          }
        },
        {
          "node": {
            "id": "65149",
            "name": "1639",
            "departure": "臺北轉運站",
            "description": "布袋→台北",
            "destination": "布袋",
            "seq": 116390
          }
        },
        {
          "node": {
            "id": "65366",
            "name": "6354",
            "departure": "火車站前",
            "description": "通霄→大甲",
            "destination": "通霄站",
            "seq": 163540
          }
        },
        {
          "node": {
            "id": "2503",
            "name": "黃3",
            "departure": "新五村",
            "description": "新五村 - 茄苳寮",
            "destination": "茄苳寮",
            "seq": 10000
          }
        },
        {
          "node": {
            "id": "37",
            "name": "37",
            "departure": "橫山",
            "description": "橫山  -  高鐵臺中站",
            "destination": "高鐵臺中站",
            "seq": 370
          }
        },
        {
          "node": {
            "id": "50",
            "name": "50",
            "departure": "文英兒童公園",
            "description": "文英兒童公園 - 921地震教育園區",
            "destination": "921地震教育園區",
            "seq": 500
          }
        },
        {
          "node": {
            "id": "251",
            "name": "251",
            "departure": "東勢",
            "description": "東勢  -  橫流溪(石角)",
            "destination": "橫流溪",
            "seq": 2510
          }
        },
        {
          "node": {
            "id": "323",
            "name": "323",
            "departure": "臺中區監理所(遊園路)",
            "description": "臺中區監理所(遊園路) - 臺中車站(民族路口)",
            "destination": "臺中車站(民族路口)",
            "seq": 3230
          }
        },
        {
          "node": {
            "id": "653",
            "name": "153副",
            "departure": "新市政中心",
            "description": "新市政中心-谷關",
            "destination": "谷關",
            "seq": 1531
          }
        },
        {
          "node": {
            "id": "65231",
            "name": "7500V",
            "departure": "北門總站",
            "description": "台北→桃園→台中→麻豆→台南",
            "destination": "臺北轉運站",
            "seq": 175003
          }
        },
        {
          "node": {
            "id": "33",
            "name": "33",
            "departure": "僑光科技大學",
            "description": "僑光科技大學 - 高鐵臺中站",
            "destination": "高鐵臺中站",
            "seq": 330
          }
        },
        {
          "node": {
            "id": "1411",
            "name": "14副2",
            "departure": "干城站",
            "description": "干城-大圳厚生路口",
            "destination": "大圳厚生路口",
            "seq": 142
          }
        },
        {
          "node": {
            "id": "65157",
            "name": "1633A",
            "departure": "臺北轉運站",
            "description": "四湖鄉→北港→台北[經宜梧村]",
            "destination": "三條崙",
            "seq": 116331
          }
        },
        {
          "node": {
            "id": "677",
            "name": "677",
            "departure": "巨業沙鹿站-左環",
            "description": "沙鹿龍井環線",
            "destination": "巨業沙鹿站-右環",
            "seq": 6770
          }
        },
        {
          "node": {
            "id": "223",
            "name": "223",
            "departure": "豐原",
            "description": "豐原  -  公老坪頂",
            "destination": "公老坪頂",
            "seq": 2230
          }
        },
        {
          "node": {
            "id": "242",
            "name": "242",
            "departure": "五權車站",
            "description": "五權車站 - 一江橋",
            "destination": "一江橋",
            "seq": 2420
          }
        },
        {
          "node": {
            "id": "253",
            "name": "253",
            "departure": "東勢",
            "description": "東勢 - 士林村",
            "destination": "士林村",
            "seq": 2530
          }
        },
        {
          "node": {
            "id": "65215",
            "name": "1610A",
            "departure": "臺北轉運站",
            "description": "高雄→台北[經三重交流道]",
            "destination": "中華站",
            "seq": 116101
          }
        },
        {
          "node": {
            "id": "324",
            "name": "324",
            "departure": "臺中都會公園",
            "description": "臺中都會公園-遠東街-新民高中",
            "destination": "新民高中",
            "seq": 3240
          }
        },
        {
          "node": {
            "id": "211",
            "name": "211",
            "departure": "豐原",
            "description": "豐原  -  大甲體育場(經土城)",
            "destination": "大甲體育場",
            "seq": 2110
          }
        },
        {
          "node": {
            "id": "305",
            "name": "305",
            "departure": "大甲",
            "description": "大甲-鹿寮-臺中車站",
            "destination": "臺中車站",
            "seq": 3050
          }
        },
        {
          "node": {
            "id": "269",
            "name": "269",
            "departure": "谷關",
            "description": "谷關  -  八仙山森林遊樂區",
            "destination": "八仙山森林遊樂區",
            "seq": 2690
          }
        },
        {
          "node": {
            "id": "985",
            "name": "A1",
            "departure": "臺中國際機場",
            "description": "臺中國際機場 - 臺中公園",
            "destination": "臺中公園",
            "seq": -11000
          }
        },
        {
          "node": {
            "id": "2508",
            "name": "黃8",
            "departure": "霧峰郵局",
            "description": "霧峰郵局 - 喀哩",
            "destination": "喀哩",
            "seq": 10000
          }
        },
        {
          "node": {
            "id": "65183",
            "name": "1619",
            "departure": "臺北轉運站",
            "description": "台中→中港路→台北",
            "destination": "臺中車站",
            "seq": 116190
          }
        },
        {
          "node": {
            "id": "65217",
            "name": "3999A",
            "departure": "臺北轉運站",
            "description": "高雄→台北[經楠梓交流道、岡山、台中、桃園、林口]",
            "destination": "高雄總站",
            "seq": 139991
          }
        },
        {
          "node": {
            "id": "481",
            "name": "48區",
            "departure": "興大附農",
            "description": "興大附農 - 中港澄清醫院",
            "destination": "中港澄清醫院",
            "seq": 481
          }
        },
        {
          "node": {
            "id": "287",
            "name": "287",
            "departure": "國軍臺中總醫院",
            "description": "國軍臺中總醫院-茅埔",
            "destination": "茅埔",
            "seq": 2870
          }
        },
        {
          "node": {
            "id": "583",
            "name": "58副",
            "departure": "大慶活動中心",
            "description": "大慶活動中心  -  潭子勝利運動公園",
            "destination": "潭子勝利運動公園",
            "seq": 581
          }
        },
        {
          "node": {
            "id": "65386",
            "name": "6899",
            "departure": "干城站",
            "description": "埔里→臺中",
            "destination": "埔里站",
            "seq": 168990
          }
        },
        {
          "node": {
            "id": "276",
            "name": "276",
            "departure": "豐富公園",
            "description": "豐富公園-東勢(經興中山莊)",
            "destination": "東勢",
            "seq": 2760
          }
        },
        {
          "node": {
            "id": "300",
            "name": "300",
            "departure": "靜宜大學",
            "description": "靜宜大學 - 臺中車站 - 靜宜大學",
            "destination": "臺中車站",
            "seq": 3000
          }
        },
        {
          "node": {
            "id": "248",
            "name": "248",
            "departure": "新烏日車站",
            "description": "新烏日車站 - 修平科技大學",
            "destination": "修平科技大學",
            "seq": 2480
          }
        },
        {
          "node": {
            "id": "65232",
            "name": "7500U",
            "departure": "北門總站",
            "description": "台北→台中→麻豆→台南",
            "destination": "臺北轉運站",
            "seq": 175002
          }
        },
        {
          "node": {
            "id": "920",
            "name": "920",
            "departure": "八方國際夜市",
            "description": "八方國際夜市-育賢環中東/西路口",
            "destination": "育賢環中東/西路口",
            "seq": 9200
          }
        },
        {
          "node": {
            "id": "1",
            "name": "1",
            "departure": "臺中刑務所演武場",
            "description": "臺中刑務所演武場-中臺科技大學校區",
            "destination": "中臺科技大學校區",
            "seq": 10
          }
        },
        {
          "node": {
            "id": "162",
            "name": "162",
            "departure": "靜宜大學希嘉學苑",
            "description": "靜宜大學-嘉陽高中-靜宜大學",
            "destination": "嘉陽高中",
            "seq": 1620
          }
        },
        {
          "node": {
            "id": "40",
            "name": "40",
            "departure": "干城站",
            "description": "臺中車站-中台新村",
            "destination": "中台新村",
            "seq": 400
          }
        },
        {
          "node": {
            "id": "358",
            "name": "358",
            "departure": "仁友停車場",
            "description": "仁友停車場-文修停車場",
            "destination": "文修停車場",
            "seq": 3580
          }
        },
        {
          "node": {
            "id": "182",
            "name": "182",
            "departure": "豐原",
            "description": "豐原  -  清水(神岡,新庄)",
            "destination": "清水(經新庄)",
            "seq": 1820
          }
        },
        {
          "node": {
            "id": "48",
            "name": "48",
            "departure": "興大附農",
            "description": "興大附農 - 嶺東科技大學",
            "destination": "嶺東科技大學",
            "seq": 480
          }
        },
        {
          "node": {
            "id": "273",
            "name": "273",
            "departure": "新五村",
            "description": "新五村  -  茄苳寮",
            "destination": "茄苳寮",
            "seq": 2730
          }
        },
        {
          "node": {
            "id": "232",
            "name": "232",
            "departure": "豐原",
            "description": "豐原  -  東山",
            "destination": "東山",
            "seq": 2320
          }
        },
        {
          "node": {
            "id": "18",
            "name": "18",
            "departure": "朝馬轉運站",
            "description": "朝馬轉運站-大智公園",
            "destination": "大智公園",
            "seq": 180
          }
        },
        {
          "node": {
            "id": "65170",
            "name": "1625",
            "departure": "臺中車站",
            "description": "臺南市→臺南交流道→國道1號→臺中交流道→臺中市[經臺南交流道]",
            "destination": "兵配廠",
            "seq": 116250
          }
        },
        {
          "node": {
            "id": "987",
            "name": "A3",
            "departure": "臺中國際機場",
            "description": "臺中國際機場 - 逢甲夜市",
            "destination": "逢甲夜市",
            "seq": -10998
          }
        },
        {
          "node": {
            "id": "352",
            "name": "352",
            "departure": "大肚",
            "description": "大肚-中科管理局",
            "destination": "中科管理局",
            "seq": 3520
          }
        },
        {
          "node": {
            "id": "925",
            "name": "70B",
            "departure": "嶺東科技大學",
            "description": "嶺東科技大學 - 第一廣場",
            "destination": "第一廣場",
            "seq": 702
          }
        },
        {
          "node": {
            "id": "65161",
            "name": "1630B",
            "departure": "臺北轉運站",
            "description": "西港→台北[溪湖至台北班次]",
            "destination": "溪湖站",
            "seq": 116302
          }
        },
        {
          "node": {
            "id": "65138",
            "name": "1806",
            "departure": "基隆站(國光)",
            "description": "南投→基隆",
            "destination": "南投車站",
            "seq": 118060
          }
        },
        {
          "node": {
            "id": "950",
            "name": "90延",
            "departure": "豐原",
            "description": "豐原 - 和平衛生所",
            "destination": "和平衛生所",
            "seq": 901
          }
        },
        {
          "node": {
            "id": "65178",
            "name": "1620",
            "departure": "臺北轉運站",
            "description": "台中→中清路→台北",
            "destination": "臺中車站",
            "seq": 116200
          }
        },
        {
          "node": {
            "id": "7",
            "name": "7",
            "departure": "臺中一中",
            "description": "臺中一中 - 長億高中 - 永成公園(永富街)",
            "destination": "永成公園(永富街)",
            "seq": 70
          }
        },
        {
          "node": {
            "id": "355",
            "name": "355",
            "departure": "臺中區監理所(遊園路)",
            "description": "臺中區監理所(遊園路)－西苑高中",
            "destination": "西苑高中",
            "seq": 3550
          }
        },
        {
          "node": {
            "id": "904",
            "name": "305W",
            "departure": "臺中車站",
            "description": "臺中車站-鹿寮-大甲",
            "destination": "大甲",
            "seq": 3051
          }
        },
        {
          "node": {
            "id": "699",
            "name": "699",
            "departure": "大甲體育場",
            "description": "大甲體育場-南埔",
            "destination": "南埔",
            "seq": 6990
          }
        },
        {
          "node": {
            "id": "286",
            "name": "286",
            "departure": "臺中二中",
            "description": "臺中二中  -  崁頂(經車籠埔)",
            "destination": "崁頂",
            "seq": 2860
          }
        },
        {
          "node": {
            "id": "65152",
            "name": "1636",
            "departure": "臺北轉運站",
            "description": "四湖鄉→西螺→台北",
            "destination": "三條崙",
            "seq": 116360
          }
        },
        {
          "node": {
            "id": "450",
            "name": "866區2",
            "departure": "梨山",
            "description": "梨山 - 武陵農場",
            "destination": "武陵農場",
            "seq": 8662
          }
        },
        {
          "node": {
            "id": "30",
            "name": "30",
            "departure": "中台新村",
            "description": "中台新村-第一廣場-中台新村",
            "destination": "干城站",
            "seq": 300
          }
        },
        {
          "node": {
            "id": "691",
            "name": "69繞",
            "departure": "龍潭里",
            "description": "龍潭里 - 臺中國際機場",
            "destination": "臺中國際機場",
            "seq": 691
          }
        },
        {
          "node": {
            "id": "282",
            "name": "282",
            "departure": "亞洲大學安藤館",
            "description": "亞洲大學安藤館-桐林",
            "destination": "桐林",
            "seq": 2820
          }
        },
        {
          "node": {
            "id": "701",
            "name": "701",
            "departure": "豐原東站",
            "description": "豐原東站 - 新建國市場",
            "destination": "新建國市場",
            "seq": 7010
          }
        },
        {
          "node": {
            "id": "410",
            "name": "310區1",
            "departure": "臺中港旅客服務中心",
            "description": "靜宜大學 - 臺中車站 - 臺中港旅客服務中心",
            "destination": "臺中車站",
            "seq": 3101
          }
        },
        {
          "node": {
            "id": "459",
            "name": "45區",
            "departure": "中科管理局",
            "description": "中科管理局 - 第一廣場 - 中科管理局",
            "destination": "第一廣場",
            "seq": 453
          }
        },
        {
          "node": {
            "id": "65127",
            "name": "1831A",
            "departure": "臺北轉運站",
            "description": "南投→臺北[繞駛公訓所中訓中心]",
            "destination": "南投車站",
            "seq": 118311
          }
        },
        {
          "node": {
            "id": "65406",
            "name": "6735A",
            "departure": "員客臺中站",
            "description": "二水→臺中[繞駛彰化高商、彰化高中]",
            "destination": "二水站",
            "seq": 167351
          }
        },
        {
          "node": {
            "id": "908",
            "name": "306E",
            "departure": "清水",
            "description": "清水-梧棲-臺中車站",
            "destination": "臺中車站(經梧棲)",
            "seq": 3062
          }
        },
        {
          "node": {
            "id": "277",
            "name": "277",
            "departure": "豐富公園",
            "description": "豐富公園-東勢(經復盛里)",
            "destination": "東勢",
            "seq": 2770
          }
        },
        {
          "node": {
            "id": "65151",
            "name": "1637",
            "departure": "臺北轉運站",
            "description": "四湖鄉→林厝寮→西螺→台北",
            "destination": "三條崙",
            "seq": 116370
          }
        },
        {
          "node": {
            "id": "2851",
            "name": "285副",
            "departure": "新建國市場",
            "description": "新建國市場-竹仔坑",
            "destination": "竹仔坑",
            "seq": 2851
          }
        },
        {
          "node": {
            "id": "999",
            "name": "956",
            "departure": "豐原東站",
            "description": "豐原東站 - 光華高工",
            "destination": "光華高工",
            "seq": 9560
          }
        },
        {
          "node": {
            "id": "65435",
            "name": "6268G",
            "departure": "干城站",
            "description": "埔里→臺中(地理中心碑)[繞駛新庄里]",
            "destination": "地理中心碑",
            "seq": 162687
          }
        },
        {
          "node": {
            "id": "65126",
            "name": "1832B",
            "departure": "臺北轉運站",
            "description": "普台高中→國道1號→臺北[經國道6號]",
            "destination": "普台高中",
            "seq": 118322
          }
        },
        {
          "node": {
            "id": "51",
            "name": "51",
            "departure": "莒光新城",
            "description": "莒光新城 - 大坑9號步道",
            "destination": "大坑9號步道",
            "seq": 510
          }
        },
        {
          "node": {
            "id": "79",
            "name": "79",
            "departure": "中科管理局",
            "description": "中科管理局 - 大慶車站",
            "destination": "大慶車站",
            "seq": 790
          }
        },
        {
          "node": {
            "id": "65526",
            "name": "6870",
            "departure": "臺中",
            "description": "溪頭→臺中(經南投、鹿谷)",
            "destination": "溪頭",
            "seq": 168700
          }
        },
        {
          "node": {
            "id": "65118",
            "name": "1851",
            "departure": "板橋客運站",
            "description": "臺中→板橋",
            "destination": "國光客運臺中車站",
            "seq": 118510
          }
        },
        {
          "node": {
            "id": "865",
            "name": "865",
            "departure": "谷關",
            "description": "谷關 - 梨山",
            "destination": "梨山",
            "seq": 8650
          }
        },
        {
          "node": {
            "id": "111",
            "name": "111",
            "departure": "清水車站",
            "description": "清水車站 - 鰲峰山公園 - 梧棲觀光漁港",
            "destination": "梧棲觀光漁港",
            "seq": 1110
          }
        },
        {
          "node": {
            "id": "981",
            "name": "98繞",
            "departure": "神岡庄尾",
            "description": "神岡庄尾 - 臺中榮總 - 普濟寺",
            "destination": "普濟寺",
            "seq": 981
          }
        },
        {
          "node": {
            "id": "237",
            "name": "237",
            "departure": "豐原",
            "description": "豐原  -  大肚車站",
            "destination": "大肚車站",
            "seq": 2370
          }
        },
        {
          "node": {
            "id": "65131",
            "name": "1829",
            "departure": "臺北轉運站",
            "description": "員林→臺北",
            "destination": "員林轉運站",
            "seq": 118290
          }
        },
        {
          "node": {
            "id": "65133",
            "name": "1827",
            "departure": "臺北轉運站",
            "description": "臺中→臺灣大道→臺北",
            "destination": "國光客運臺中車站",
            "seq": 118270
          }
        },
        {
          "node": {
            "id": "200",
            "name": "200",
            "departure": "霧峰農會",
            "description": "霧峰農會 - 臺中女中",
            "destination": "臺中女中",
            "seq": 2000
          }
        },
        {
          "node": {
            "id": "65265",
            "name": "5814",
            "departure": "苗栗火車站",
            "description": "大甲→苗栗(經山腳)",
            "destination": "大甲站",
            "seq": 158140
          }
        },
        {
          "node": {
            "id": "65357",
            "name": "6737",
            "departure": "員客臺中站",
            "description": "西港→臺中",
            "destination": "西港",
            "seq": 167370
          }
        },
        {
          "node": {
            "id": "991",
            "name": "99延",
            "departure": "精武車站",
            "description": "精武車站 - 臺中區監理所(遊園路)",
            "destination": "臺中區監理所(遊園路)",
            "seq": 991
          }
        },
        {
          "node": {
            "id": "460",
            "name": "45延1",
            "departure": "中科實驗高中(平和路)",
            "description": "中科實驗高中(平和路) - 干城站-中科實驗高中(平和路)",
            "destination": "干城站",
            "seq": 452
          }
        },
        {
          "node": {
            "id": "144",
            "name": "14延",
            "departure": "干城站",
            "description": "干城站-南清宮",
            "destination": "南清宮",
            "seq": 143
          }
        },
        {
          "node": {
            "id": "65167",
            "name": "1625B",
            "departure": "朝馬站",
            "description": "臺南市→臺南交流道→國道1號→臺中交流道→臺中市[經永康交流道、臺南至朝馬班次]",
            "destination": "兵配廠",
            "seq": 116252
          }
        },
        {
          "node": {
            "id": "218",
            "name": "218",
            "departure": "豐原",
            "description": "豐原  -  神岡區公所",
            "destination": "神岡區公所",
            "seq": 2180
          }
        },
        {
          "node": {
            "id": "45",
            "name": "45",
            "departure": "中科管理局",
            "description": "中科管理局 - 第一廣場 - 中科管理局",
            "destination": "干城站",
            "seq": 450
          }
        },
        {
          "node": {
            "id": "41",
            "name": "41",
            "departure": "公共資訊圖書館",
            "description": "公共資訊圖書館 - 修平科技大學 ",
            "destination": "修平科技大學",
            "seq": 410
          }
        },
        {
          "node": {
            "id": "657",
            "name": "657",
            "departure": "大甲高工",
            "description": "大甲高工-建興",
            "destination": "建興",
            "seq": 6570
          }
        },
        {
          "node": {
            "id": "1994",
            "name": "(本路線捷順交通199舊路線目前已停駛)",
            "departure": "龍井竹坑口",
            "description": "龍井竹坑口-文修停車場",
            "destination": "文修停車場",
            "seq": 9999
          }
        },
        {
          "node": {
            "id": "127",
            "name": "127",
            "departure": "豐樂雕塑公園",
            "description": "豐樂雕塑公園 - 臺中洲際棒球場",
            "destination": "臺中洲際棒球場",
            "seq": 1270
          }
        },
        {
          "node": {
            "id": "21",
            "name": "21",
            "departure": "臺中刑務所演武場",
            "description": "臺中刑務所演武場 - 貴城山莊",
            "destination": "貴城山莊",
            "seq": 210
          }
        },
        {
          "node": {
            "id": "65532",
            "name": "6333",
            "departure": "干城站",
            "description": "水里→中興→臺中",
            "destination": "水里",
            "seq": 163330
          }
        },
        {
          "node": {
            "id": "1812",
            "name": "181區2",
            "departure": "苑裡站",
            "description": "苑裡站  -  日南國中",
            "destination": "日南國中",
            "seq": 1812
          }
        },
        {
          "node": {
            "id": "1071",
            "name": "107延",
            "departure": "黎明新村",
            "description": "黎明新村 - 舊正社區停車場",
            "destination": "舊正社區停車場",
            "seq": 1071
          }
        },
        {
          "node": {
            "id": "65244",
            "name": "7500H",
            "departure": "北門總站",
            "description": "台北→桃園→台中→新營→麻豆→台南",
            "destination": "臺北轉運站",
            "seq": 175008
          }
        },
        {
          "node": {
            "id": "309",
            "name": "309",
            "departure": "高美濕地",
            "description": "高美濕地 - 臺中車站 - 高美濕地",
            "destination": "臺中車站",
            "seq": 3090
          }
        },
        {
          "node": {
            "id": "910",
            "name": "901副",
            "departure": "豐原",
            "description": "豐原 - 明德高中",
            "destination": "明德高中",
            "seq": 9011
          }
        },
        {
          "node": {
            "id": "238",
            "name": "238",
            "departure": "豐原",
            "description": "豐原  -  臺中港郵局",
            "destination": "臺中港郵局(經沙鹿)",
            "seq": 2380
          }
        },
        {
          "node": {
            "id": "65100",
            "name": "1141",
            "departure": "花蓮火車站",
            "description": "臺中(梨山)→花蓮",
            "destination": "梨山",
            "seq": 111410
          }
        },
        {
          "node": {
            "id": "65179",
            "name": "1620B",
            "departure": "臺北轉運站",
            "description": "台中→中清路→台北[不停新竹站、林口站]",
            "destination": "臺中車站",
            "seq": 116202
          }
        },
        {
          "node": {
            "id": "65119",
            "name": "1839",
            "departure": "臺北轉運站",
            "description": "屏東→臺北",
            "destination": "麟洛站",
            "seq": 118390
          }
        },
        {
          "node": {
            "id": "35",
            "name": "35",
            "departure": "南區公所",
            "description": "南區公所 - 僑光科技大學",
            "destination": "僑光科技大學",
            "seq": 350
          }
        },
        {
          "node": {
            "id": "14",
            "name": "14",
            "departure": "干城站",
            "description": "干城站-舊庄(南清宮)",
            "destination": "舊庄",
            "seq": 140
          }
        },
        {
          "node": {
            "id": "65393",
            "name": "6670A",
            "departure": "臺中",
            "description": "日月潭→埔里→高鐵臺中站→臺中[經國道6號、九族文化村]",
            "destination": "日月潭",
            "seq": 166701
          }
        },
        {
          "node": {
            "id": "909",
            "name": "151區",
            "departure": "高鐵臺中站",
            "description": "高鐵臺中站-朝陽科技大學/亞洲大學",
            "destination": "朝陽科技大學",
            "seq": 1512
          }
        },
        {
          "node": {
            "id": "65160",
            "name": "1631",
            "departure": "臺北轉運站",
            "description": "竹山→二水→台北",
            "destination": "竹山",
            "seq": 116310
          }
        },
        {
          "node": {
            "id": "65182",
            "name": "1619A",
            "departure": "臺北轉運站",
            "description": "台中→中港路→台北[朝馬至台北班次、不停竹科站]",
            "destination": "朝馬站",
            "seq": 116191
          }
        },
        {
          "node": {
            "id": "65117",
            "name": "1852",
            "departure": "板橋客運站",
            "description": "臺中→國道3號→板橋",
            "destination": "國光客運臺中車站",
            "seq": 118520
          }
        },
        {
          "node": {
            "id": "956",
            "name": "206延",
            "departure": "石岡國中",
            "description": "石岡國中  -  東勢高工",
            "destination": "東勢高工",
            "seq": 2061
          }
        },
        {
          "node": {
            "id": "933",
            "name": "105區2",
            "departure": "臺中高工",
            "description": "臺中高工-四張犁",
            "destination": "四張犁",
            "seq": 1052
          }
        },
        {
          "node": {
            "id": "3231",
            "name": "323區",
            "departure": "東海別墅",
            "description": "東海別墅 - 臺中車站(民族路口)",
            "destination": "臺中車站(民族路口)",
            "seq": 3231
          }
        },
        {
          "node": {
            "id": "913",
            "name": "21延2",
            "departure": "臺中刑務所演武場",
            "description": "臺中刑務所演武場-中興嶺",
            "destination": "中興嶺",
            "seq": 212
          }
        },
        {
          "node": {
            "id": "89",
            "name": "89",
            "departure": "嶺東科技大學",
            "description": "嶺東科技大學  -  東門橋",
            "destination": "東門橋",
            "seq": 890
          }
        },
        {
          "node": {
            "id": "671",
            "name": "67繞",
            "departure": "秀泰影城",
            "description": "秀泰影城 - 東海街口",
            "destination": "東海街口",
            "seq": 671
          }
        },
        {
          "node": {
            "id": "108",
            "name": "108",
            "departure": "港尾",
            "description": "港尾 - 南開科技大學校區",
            "destination": "南開科技大學校區",
            "seq": 1080
          }
        },
        {
          "node": {
            "id": "310",
            "name": "310",
            "departure": "臺中港旅客服務中心",
            "description": "臺中港旅客服務中心 - 臺中車站",
            "destination": "臺中車站",
            "seq": 3100
          }
        },
        {
          "node": {
            "id": "688",
            "name": "688",
            "departure": "清水車站",
            "description": "清水車站-港區藝術中心-臺中港旅客服務中心",
            "destination": "臺中港旅客服務中心",
            "seq": 6880
          }
        },
        {
          "node": {
            "id": "911",
            "name": "91延",
            "departure": "臺中國際機場",
            "description": "臺中國際機場 - 中興嶺停車場",
            "destination": "中興嶺停車場",
            "seq": 911
          }
        },
        {
          "node": {
            "id": "65188",
            "name": "1618C",
            "departure": "",
            "description": "嘉義→台北[不停大林站]",
            "destination": "",
            "seq": 116183
          }
        },
        {
          "node": {
            "id": "65164",
            "name": "1629",
            "departure": "臺北轉運站",
            "description": "苓子寮→學甲→台北",
            "destination": "苓仔寮",
            "seq": 116290
          }
        },
        {
          "node": {
            "id": "272",
            "name": "272",
            "departure": "東勢",
            "description": "東勢  -  茄苳寮(土牛)",
            "destination": "茄苳寮",
            "seq": 2720
          }
        },
        {
          "node": {
            "id": "700",
            "name": "700",
            "departure": "明德高中(明德街)",
            "description": "明德高中(明德街) - 豐原高中",
            "destination": "豐原高中",
            "seq": 7000
          }
        },
        {
          "node": {
            "id": "65115",
            "name": "1861",
            "departure": "國光客運桃園停車場",
            "description": "臺中→桃園",
            "destination": "國光客運臺中車站",
            "seq": 118610
          }
        },
        {
          "node": {
            "id": "302",
            "name": "302",
            "departure": "臺中國際機場",
            "description": "臺中國際機場-臺中公園",
            "destination": "臺中公園",
            "seq": 3020
          }
        },
        {
          "node": {
            "id": "65116",
            "name": "1860",
            "departure": "第一航廈",
            "description": "臺中→臺灣桃園國際機場",
            "destination": "國光客運臺中車站",
            "seq": 118600
          }
        },
        {
          "node": {
            "id": "65198",
            "name": "1615",
            "departure": "臺北轉運站",
            "description": "彰化→台北",
            "destination": "彰化站",
            "seq": 116150
          }
        },
        {
          "node": {
            "id": "65489",
            "name": "6935",
            "departure": "水尾",
            "description": "臺中→彰化→水尾",
            "destination": "臺中",
            "seq": 169350
          }
        },
        {
          "node": {
            "id": "90",
            "name": "90",
            "departure": "豐原",
            "description": "豐原 - 豐原高中 - 東勢高工",
            "destination": "東勢高工",
            "seq": 900
          }
        },
        {
          "node": {
            "id": "65392",
            "name": "6670B",
            "departure": "臺中",
            "description": "日月潭→高鐵臺中站→臺中[台灣好行日月潭線]",
            "destination": "日月潭",
            "seq": 166702
          }
        },
        {
          "node": {
            "id": "800",
            "name": "800",
            "departure": "捷運北屯總站",
            "description": "捷運北屯總站 - 仁友停車場",
            "destination": "仁友停車場",
            "seq": 8000
          }
        },
        {
          "node": {
            "id": "65213",
            "name": "1610B",
            "departure": "臺北轉運站",
            "description": "高雄→台北[經三重交流道，不停岡山站、南崁站]",
            "destination": "中華站",
            "seq": 116102
          }
        },
        {
          "node": {
            "id": "65281",
            "name": "7138",
            "departure": "斗六後火車站",
            "description": "臺中朝馬→國道3號→竹山→林內→斗六",
            "destination": "第六分局(市政路)",
            "seq": 171380
          }
        },
        {
          "node": {
            "id": "65194",
            "name": "1616B",
            "departure": "臺北轉運站",
            "description": "員林→台北[經台76線]",
            "destination": "員林轉運站",
            "seq": 116162
          }
        },
        {
          "node": {
            "id": "307",
            "name": "307",
            "departure": "梧棲觀光漁港",
            "description": "梧棲觀光漁港-新民高中",
            "destination": "新民高中",
            "seq": 3070
          }
        },
        {
          "node": {
            "id": "65184",
            "name": "1618",
            "departure": "臺北轉運站",
            "description": "嘉義→台北",
            "destination": "嘉義轉運站",
            "seq": 116180
          }
        },
        {
          "node": {
            "id": "65174",
            "name": "1621C",
            "departure": "臺中車站",
            "description": "高雄→烏日→台中[不停楠梓站、岡山站]",
            "destination": "高雄火車站",
            "seq": 116213
          }
        },
        {
          "node": {
            "id": "811",
            "name": "811",
            "departure": "豐原東站",
            "description": "豐原東站 - 大甲體育場",
            "destination": "大甲體育場",
            "seq": 8110
          }
        },
        {
          "node": {
            "id": "229",
            "name": "229",
            "departure": "豐原",
            "description": "豐原  -  朝馬",
            "destination": "朝馬",
            "seq": 2290
          }
        },
        {
          "node": {
            "id": "304",
            "name": "304",
            "departure": "港區藝術中心",
            "description": "港區藝術中心-新民高中",
            "destination": "新民高中",
            "seq": 3040
          }
        },
        {
          "node": {
            "id": "27",
            "name": "27",
            "departure": "臺中車站(民族路口)",
            "description": "臺中車站(民族路口) - 嶺東三站",
            "destination": "嶺東三站",
            "seq": 270
          }
        },
        {
          "node": {
            "id": "159",
            "name": "159",
            "departure": "高鐵臺中站",
            "description": "高鐵臺中站  -  臺中公園",
            "destination": "臺中公園",
            "seq": 1590
          }
        },
        {
          "node": {
            "id": "531",
            "name": "53區",
            "departure": "文心市政路口",
            "description": "文心市政路口  -  省議會",
            "destination": "省議會",
            "seq": 531
          }
        },
        {
          "node": {
            "id": "65153",
            "name": "1635",
            "departure": "臺北轉運站",
            "description": "四湖鄉→虎尾→台北",
            "destination": "三條崙",
            "seq": 116350
          }
        },
        {
          "node": {
            "id": "500",
            "name": "500",
            "departure": "忠義里(中科實驗高中)",
            "description": "忠義里(中科實驗高中) - 臺中車站",
            "destination": "臺中車站",
            "seq": 5000
          }
        },
        {
          "node": {
            "id": "226",
            "name": "226",
            "departure": "中科后里辦公室",
            "description": "中科后里辦公室  -  太平里",
            "destination": "太平里",
            "seq": 2260
          }
        },
        {
          "node": {
            "id": "201",
            "name": "201",
            "departure": "柳豐路(亞大醫院)",
            "description": "柳豐路(亞大醫院) - 新民高中(健行路)",
            "destination": "新民高中(健行路)",
            "seq": 2010
          }
        },
        {
          "node": {
            "id": "986",
            "name": "A2",
            "departure": "臺中國際機場",
            "description": "臺中國際機場 - 臺中車站",
            "destination": "臺中車站",
            "seq": -10999
          }
        },
        {
          "node": {
            "id": "165",
            "name": "165",
            "departure": "清水國中",
            "description": "清水國中 - 舊庄",
            "destination": "舊庄",
            "seq": 1650
          }
        },
        {
          "node": {
            "id": "71",
            "name": "71",
            "departure": "臺中洲際棒球場",
            "description": "臺中洲際棒球場 - 植物園(西屯路)",
            "destination": "植物園(西屯路)",
            "seq": 710
          }
        },
        {
          "node": {
            "id": "659",
            "name": "659",
            "departure": "幼獅工業區服務中心",
            "description": "幼獅工業區服務中心-臺中榮總(臺灣大道)",
            "destination": "臺中榮總(臺灣大道)",
            "seq": 6590
          }
        },
        {
          "node": {
            "id": "899",
            "name": "199",
            "departure": "龍井竹坑口",
            "description": "龍井竹坑口-文修停車場",
            "destination": "文修停車場",
            "seq": 1990
          }
        },
        {
          "node": {
            "id": "65387",
            "name": "6871",
            "departure": "員客臺中站",
            "description": "杉林溪→臺中(經南投、鹿谷)",
            "destination": "杉林溪",
            "seq": 168710
          }
        },
        {
          "node": {
            "id": "65148",
            "name": "1652",
            "departure": "臺北轉運站",
            "description": "台北→鹿港→芳苑",
            "destination": "芳苑站",
            "seq": 116520
          }
        },
        {
          "node": {
            "id": "65390",
            "name": "6670D",
            "departure": "臺中",
            "description": "日月潭→埔里→高鐵臺中站→臺中[經國道6號]",
            "destination": "日月潭",
            "seq": 166704
          }
        },
        {
          "node": {
            "id": "65431",
            "name": "6188A",
            "departure": "臺中車站",
            "description": "竹山→快官交流道→台74→高鐵台中站→臺中市",
            "destination": "竹山站",
            "seq": 161881
          }
        },
        {
          "node": {
            "id": "12",
            "name": "12",
            "departure": "明德高中(明德街)",
            "description": "明德高中(明德街) - 豐原鎮清宮",
            "destination": "豐原鎮清宮",
            "seq": 120
          }
        },
        {
          "node": {
            "id": "171",
            "name": "171",
            "departure": "大甲",
            "description": "大甲  -  大安港",
            "destination": "大安港",
            "seq": 1710
          }
        },
        {
          "node": {
            "id": "65128",
            "name": "1831",
            "departure": "臺北轉運站",
            "description": "南投→臺北",
            "destination": "南投車站",
            "seq": 118310
          }
        },
        {
          "node": {
            "id": "592",
            "name": "91繞",
            "departure": "舊庄",
            "description": "舊庄 - 東興 - 中興嶺停車場",
            "destination": "中興嶺停車場",
            "seq": 912
          }
        },
        {
          "node": {
            "id": "227",
            "name": "227",
            "departure": "豐原",
            "description": "豐原  -  東陽里",
            "destination": "東陽里",
            "seq": 2270
          }
        },
        {
          "node": {
            "id": "65367",
            "name": "6606",
            "departure": "豐原",
            "description": "臺北→豐原",
            "destination": "臺北轉運站",
            "seq": 166060
          }
        },
        {
          "node": {
            "id": "65190",
            "name": "1617C",
            "departure": "臺北轉運站",
            "description": "東勢→豐原→台北[不停麗寶樂園(月眉)站]",
            "destination": "東勢站",
            "seq": 116173
          }
        },
        {
          "node": {
            "id": "11",
            "name": "11",
            "departure": "臺中車站-右環",
            "description": "綠能街車",
            "destination": "臺中車站-左環",
            "seq": 110
          }
        },
        {
          "node": {
            "id": "65187",
            "name": "1618D",
            "departure": "臺北轉運站",
            "description": "台北→嘉義[不停林口站]",
            "destination": "嘉義轉運站",
            "seq": 116184
          }
        },
        {
          "node": {
            "id": "74",
            "name": "74",
            "departure": "太平",
            "description": "太平-新烏日車站-嶺東科技大學",
            "destination": "嶺東科技大學",
            "seq": 740
          }
        },
        {
          "node": {
            "id": "1271",
            "name": "127延",
            "departure": "大慶車站",
            "description": "大慶車站 - 臺中洲際棒球場",
            "destination": "臺中洲際棒球場",
            "seq": 1271
          }
        },
        {
          "node": {
            "id": "65142",
            "name": "9019",
            "departure": "朝馬站",
            "description": "麥寮→二林→國道1號→臺中朝馬",
            "destination": "麥寮站",
            "seq": 190190
          }
        },
        {
          "node": {
            "id": "288",
            "name": "288",
            "departure": "臺中二中",
            "description": "臺中二中  -  茅埔",
            "destination": "茅埔",
            "seq": 2880
          }
        },
        {
          "node": {
            "id": "65360",
            "name": "6738",
            "departure": "員客臺中站",
            "description": "王功→臺中(經二林)",
            "destination": "南王功",
            "seq": 167380
          }
        },
        {
          "node": {
            "id": "154",
            "name": "154",
            "departure": "臺中女中",
            "description": "臺中女中 - 大甲區公所",
            "destination": "大甲區公所",
            "seq": 1540
          }
        },
        {
          "node": {
            "id": "252",
            "name": "252",
            "departure": "東勢",
            "description": "東勢  -  大雪山森林遊樂區",
            "destination": "大雪山森林遊樂區",
            "seq": 2520
          }
        },
        {
          "node": {
            "id": "258",
            "name": "258",
            "departure": "東勢",
            "description": "東勢  -  卓蘭",
            "destination": "卓蘭",
            "seq": 2580
          }
        },
        {
          "node": {
            "id": "821",
            "name": "821",
            "departure": "東勢",
            "description": "東勢 - 和平衛生所",
            "destination": "和平衛生所",
            "seq": 8200
          }
        },
        {
          "node": {
            "id": "132",
            "name": "132",
            "departure": "北屯區行政大樓",
            "description": "北屯區行政大樓-朝陽科技大學 ",
            "destination": "朝陽科技大學",
            "seq": 1320
          }
        },
        {
          "node": {
            "id": "54",
            "name": "54",
            "departure": "下港尾",
            "description": "下港尾  -  烏日停車場",
            "destination": "烏日停車場",
            "seq": 540
          }
        },
        {
          "node": {
            "id": "6",
            "name": "6",
            "departure": "干城站",
            "description": "干城站 - 忠義里",
            "destination": "忠義里",
            "seq": 60
          }
        },
        {
          "node": {
            "id": "924",
            "name": "70A",
            "departure": "第一廣場",
            "description": "第一廣場  -  嶺東科技大學",
            "destination": "嶺東科技大學",
            "seq": 701
          }
        },
        {
          "node": {
            "id": "682",
            "name": "68繞",
            "departure": "坪頂",
            "description": "坪頂 - 太原車站 - 中臺科技大學",
            "destination": "中臺科技大學",
            "seq": 681
          }
        },
        {
          "node": {
            "id": "65107",
            "name": "1870A",
            "departure": "朝馬站(國光)",
            "description": "嘉義→臺中[嘉義至朝馬]",
            "destination": "嘉義市轉運中心",
            "seq": 118701
          }
        },
        {
          "node": {
            "id": "2901",
            "name": "290繞",
            "departure": "干城站",
            "description": "干城站 -童綜合醫院(梧棲院區)",
            "destination": "童綜合醫院(梧棲院區)",
            "seq": 2901
          }
        },
        {
          "node": {
            "id": "65129",
            "name": "1830A",
            "departure": "臺北轉運站",
            "description": "北斗→臺北[不經明道大學]",
            "destination": "北斗",
            "seq": 118301
          }
        },
        {
          "node": {
            "id": "65358",
            "name": "6736",
            "departure": "員客臺中站",
            "description": "二林→臺中",
            "destination": "二林站",
            "seq": 167360
          }
        },
        {
          "node": {
            "id": "72",
            "name": "72",
            "departure": "仁友停車場",
            "description": "仁友停車場 - 慈濟醫院",
            "destination": "慈濟醫院",
            "seq": 720
          }
        },
        {
          "node": {
            "id": "2501",
            "name": "黃1",
            "departure": "豐原",
            "description": "豐原 - 豐原球場",
            "destination": "豐原球場",
            "seq": 10000
          }
        },
        {
          "node": {
            "id": "353",
            "name": "353",
            "departure": "天母櫻城",
            "description": "天母櫻城-沙鹿",
            "destination": "沙鹿",
            "seq": 3530
          }
        },
        {
          "node": {
            "id": "2506",
            "name": "黃6",
            "departure": "台影文化城",
            "description": "台影文化城 - 青桐林生態產業園區",
            "destination": "青桐林生態產業園區",
            "seq": 10000
          }
        },
        {
          "node": {
            "id": "65111",
            "name": "1863",
            "departure": "中壢站(國光)",
            "description": "臺中→中壢",
            "destination": "國光客運臺中車站",
            "seq": 118630
          }
        },
        {
          "node": {
            "id": "32",
            "name": "32",
            "departure": "大鵬新城",
            "description": "大鵬新城 - 旱溪東東英五街口",
            "destination": "旱溪東東英五街口",
            "seq": 320
          }
        },
        {
          "node": {
            "id": "65527",
            "name": "6606A",
            "departure": "豐原",
            "description": "臺北→豐原[繞駛中壢服務區]",
            "destination": "臺北轉運站",
            "seq": 166061
          }
        },
        {
          "node": {
            "id": "56",
            "name": "56",
            "departure": "干城站",
            "description": "干城站 - 新烏日車站",
            "destination": "新烏日車站",
            "seq": 560
          }
        },
        {
          "node": {
            "id": "65426",
            "name": "9015",
            "departure": "臺中車站",
            "description": "北港→斗南交流道→國道1號高速公路→中港交流道→臺中車站",
            "destination": "北港",
            "seq": 190150
          }
        },
        {
          "node": {
            "id": "65103",
            "name": "1872A",
            "departure": "朝馬站(國光)",
            "description": "高雄→臺中[高雄至朝馬]",
            "destination": "高雄站(國光)",
            "seq": 118721
          }
        },
        {
          "node": {
            "id": "303",
            "name": "303",
            "departure": "港區藝術中心",
            "description": "港區藝術中心-新民高中",
            "destination": "新民高中",
            "seq": 3030
          }
        },
        {
          "node": {
            "id": "2504",
            "name": "黃4",
            "departure": "朴子里活動中心",
            "description": "朴子里活動中心 - 豐原醫院",
            "destination": "豐原醫院",
            "seq": 10000
          }
        },
        {
          "node": {
            "id": "65123",
            "name": "1837",
            "departure": "臺北轉運站",
            "description": "臺南→臺北",
            "destination": "兵配廠",
            "seq": 118370
          }
        },
        {
          "node": {
            "id": "979",
            "name": "989延",
            "departure": "翁子",
            "description": "翁子-神圳國中",
            "destination": "神圳國中",
            "seq": 9891
          }
        },
        {
          "node": {
            "id": "2001",
            "name": "200延",
            "departure": "亞大醫院",
            "description": "亞大醫院 - 臺中女中",
            "destination": "臺中女中",
            "seq": 2001
          }
        },
        {
          "node": {
            "id": "75",
            "name": "75",
            "departure": "臺中榮總",
            "description": "臺中榮總  -  一江橋",
            "destination": "一江橋",
            "seq": 750
          }
        },
        {
          "node": {
            "id": "203",
            "name": "203",
            "departure": "豐富公園(經新田、慈濟)",
            "description": "豐富公園-豐原(經新田)",
            "destination": "豐原(經新田、慈濟)",
            "seq": 2030
          }
        },
        {
          "node": {
            "id": "214",
            "name": "214",
            "departure": "后里馬場",
            "description": "后里馬場 - 大甲車站",
            "destination": "大甲車站",
            "seq": 2140
          }
        },
        {
          "node": {
            "id": "467",
            "name": "65繞",
            "departure": "南區公所",
            "description": "南區公所  -  潭雅神綠園道",
            "destination": "潭雅神綠園道",
            "seq": 652
          }
        },
        {
          "node": {
            "id": "153",
            "name": "153",
            "departure": "高鐵臺中站",
            "description": "高鐵臺中站 - 谷關",
            "destination": "谷關",
            "seq": 1530
          }
        },
        {
          "node": {
            "id": "942",
            "name": "68延(繞國安國小)",
            "departure": "鹿寮國中",
            "description": "鹿寮國中 - 太原車站 - 中臺科技大學",
            "destination": "中臺科技大學(繞國安國小)",
            "seq": 683
          }
        },
        {
          "node": {
            "id": "81",
            "name": "81",
            "departure": "統聯轉運站",
            "description": "統聯轉運站 - 臺中車站 - 太平",
            "destination": "太平",
            "seq": 810
          }
        },
        {
          "node": {
            "id": "9",
            "name": "9",
            "departure": "公共資訊圖書館(建成路)",
            "description": "公共資訊圖書館(建成路)-清水",
            "destination": "清水",
            "seq": 90
          }
        },
        {
          "node": {
            "id": "65121",
            "name": "1838",
            "departure": "臺北轉運站",
            "description": "高雄→臺北(林口交流道)",
            "destination": "高雄站(國光)",
            "seq": 118380
          }
        },
        {
          "node": {
            "id": "65166",
            "name": "1628",
            "departure": "臺北轉運站",
            "description": "漚汪→佳里、麻豆→台北",
            "destination": "漚汪",
            "seq": 116280
          }
        },
        {
          "node": {
            "id": "65531",
            "name": "6268",
            "departure": "干城站",
            "description": "埔里→臺中(地理中心碑)",
            "destination": "地理中心碑",
            "seq": 162680
          }
        },
        {
          "node": {
            "id": "163",
            "name": "163",
            "departure": "華盛頓中學",
            "description": "華盛頓中學-第一廣場-華盛頓中學",
            "destination": "第一廣場",
            "seq": 1630
          }
        },
        {
          "node": {
            "id": "65169",
            "name": "1625A",
            "departure": "臺中車站",
            "description": "臺南市→臺南交流道→國道1號→臺中交流道→臺中市[經永康交流道]",
            "destination": "兵配廠",
            "seq": 116251
          }
        },
        {
          "node": {
            "id": "65430",
            "name": "6899A",
            "departure": "干城站",
            "description": "埔里→臺中[經中投公路]",
            "destination": "埔里站",
            "seq": 168991
          }
        },
        {
          "node": {
            "id": "65193",
            "name": "1617",
            "departure": "臺北轉運站",
            "description": "東勢→豐原→台北",
            "destination": "東勢站",
            "seq": 116170
          }
        },
        {
          "node": {
            "id": "65466",
            "name": "6882",
            "departure": "員客臺中站",
            "description": "西螺→臺中",
            "destination": "西螺站",
            "seq": 168820
          }
        },
        {
          "node": {
            "id": "65176",
            "name": "1621A",
            "departure": "臺中車站",
            "description": "高雄→烏日→台中[不停楠梓站、岡山站、彰化交流道站]",
            "destination": "高雄火車站",
            "seq": 116211
          }
        },
        {
          "node": {
            "id": "65209",
            "name": "1611E",
            "departure": "",
            "description": "台南→台北[經三重交流道、不停新營站、南崁站]",
            "destination": "",
            "seq": 116115
          }
        },
        {
          "node": {
            "id": "267",
            "name": "267",
            "departure": "東勢",
            "description": "東勢 - 上台電",
            "destination": "上台電",
            "seq": 2670
          }
        },
        {
          "node": {
            "id": "916",
            "name": "281副",
            "departure": "新建國市場",
            "description": "新建國市場 - 霧峰農工",
            "destination": "霧峰農工",
            "seq": 2811
          }
        },
        {
          "node": {
            "id": "65424",
            "name": "9016A",
            "departure": "台中轉運站",
            "description": "四湖→西螺交流道→國道1號高速公路→中港交流道→臺中車站[不經明道大學]",
            "destination": "四湖",
            "seq": 190161
          }
        },
        {
          "node": {
            "id": "181",
            "name": "181",
            "departure": "苑裡站",
            "description": "苑裡站  -  大甲區公所",
            "destination": "大甲區公所",
            "seq": 1810
          }
        },
        {
          "node": {
            "id": "65147",
            "name": "1652A",
            "departure": "臺北轉運站",
            "description": "芳苑→鹿港→台北[鹿港至台北班次]",
            "destination": "鹿港站",
            "seq": 116521
          }
        },
        {
          "node": {
            "id": "291",
            "name": "291",
            "departure": "山多綠社區",
            "description": "霧峰區山多綠社區-大里區環保公園",
            "destination": "環保公園",
            "seq": 2910
          }
        },
        {
          "node": {
            "id": "9221",
            "name": "922",
            "departure": "北屯區行政大樓",
            "description": "北屯區行政大樓 - 大坑九號步道",
            "destination": "大坑九號步道",
            "seq": 9220
          }
        },
        {
          "node": {
            "id": "284",
            "name": "284",
            "departure": "臺中車站",
            "description": "臺中車站- 修平科技大學",
            "destination": "修平科技大學",
            "seq": 2840
          }
        },
        {
          "node": {
            "id": "941",
            "name": "68延",
            "departure": "鹿寮國中",
            "description": "鹿寮國中 - 太原車站 - 中臺科技大學",
            "destination": "中臺科技大學",
            "seq": 682
          }
        },
        {
          "node": {
            "id": "99",
            "name": "99",
            "departure": "精武車站",
            "description": "精武車站 - 嶺東科技大學",
            "destination": "嶺東科技大學",
            "seq": 990
          }
        },
        {
          "node": {
            "id": "101",
            "name": "101",
            "departure": "敦化后庄七街口",
            "description": "彰化車站 - 敦化后庄七街口",
            "destination": "彰化車站",
            "seq": 1010
          }
        },
        {
          "node": {
            "id": "128",
            "name": "128",
            "departure": "大雅",
            "description": "大雅 - 清水",
            "destination": "清水",
            "seq": 1280
          }
        }
      ]
    }
  }
}

var routeGroup = [
  {
    "id": "36",
    "name": "黃線",
    "description": "黃線",
    "seq": 0,
    "routes": [
      {
        "ddesc": "澄清湖棒球場－捷運三多商圈",
        "seq": 60010,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/601/map/765/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 601,
        "NameZh": "黃1(平日行駛)",
        "DepartureZh": "澄清湖棒球場",
        "DestinationZh": "捷運三多商圈"
      },
      {
        "ddesc": "澄清湖棒球場－高雄展覽館",
        "seq": 60011,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/6011/map/954/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 6011,
        "NameZh": "黃1延(假日行駛)",
        "DepartureZh": "澄清湖棒球場",
        "DestinationZh": "高雄展覽館"
      },
      {
        "ddesc": "前鎮高中－忠誠路口",
        "seq": 60020,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/602/map/674/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 602,
        "NameZh": "黃2A",
        "DepartureZh": "前鎮高中",
        "DestinationZh": "忠誠路口"
      },
      {
        "ddesc": "小港站－忠誠路口－前鎮高中",
        "seq": 60021,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/6021/map/697/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 6021,
        "NameZh": "黃2B",
        "DepartureZh": "小港站",
        "DestinationZh": "忠誠路口"
      },
      {
        "ddesc": "前鎮高中－忠誠路口－小港站",
        "seq": 60022,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/6022/map/676/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 6022,
        "NameZh": "黃2C",
        "DepartureZh": "前鎮高中",
        "DestinationZh": "忠誠路口"
      }
    ]
  },
  {
    "id": "37",
    "name": "綠線",
    "description": "綠線",
    "seq": 0,
    "routes": [
      {
        "ddesc": "香蕉碼頭－正勤社區",
        "seq": 70001,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/7002/map/966/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 7002,
        "NameZh": "綠1區間",
        "DepartureZh": "香蕉碼頭(棧貳庫)",
        "DestinationZh": "正勤社區"
      },
      {
        "ddesc": "香蕉碼頭－輕軌凱旋中華站",
        "seq": 70001,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/7001/map/965/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 7001,
        "NameZh": "綠1",
        "DepartureZh": "香蕉碼頭(棧貳庫)",
        "DestinationZh": "輕軌凱旋中華站"
      }
    ]
  },
  {
    "id": "9",
    "name": "快線",
    "description": "快線",
    "seq": 1,
    "routes": [
      {
        "ddesc": "高鐵左營站－旗山轉運站",
        "seq": 40010,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/604/map/413/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 604,
        "NameZh": "E01A旗美國道快線(往旗山轉運站)",
        "DepartureZh": "高鐵左營站",
        "DestinationZh": "旗山轉運站"
      },
      {
        "ddesc": "高鐵左營站－美濃站",
        "seq": 40011,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/603/map/412/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 603,
        "NameZh": "E01B旗美國道快線(往美濃站)",
        "DepartureZh": "高鐵左營站",
        "DestinationZh": "美濃站"
      },
      {
        "ddesc": "高鐵左營站－佛光山",
        "seq": 40020,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/609/map/414/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 609,
        "NameZh": "E02哈佛快線",
        "DepartureZh": "高鐵左營站",
        "DestinationZh": "佛光山"
      },
      {
        "ddesc": "高鐵左營站－義大醫院",
        "seq": 40030,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/503/map/926/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 503,
        "NameZh": "E03A燕巢快線",
        "DepartureZh": "高鐵左營站",
        "DestinationZh": "義大醫院"
      },
      {
        "ddesc": "高鐵左營站－義大醫院",
        "seq": 40031,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/5031/map/927/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 5031,
        "NameZh": "E03B燕巢快線",
        "DepartureZh": "高鐵左營站",
        "DestinationZh": "義大醫院"
      },
      {
        "ddesc": "高鐵左營站－高師大燕巢校區",
        "seq": 40040,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/504/map/1003/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 504,
        "NameZh": "E04",
        "DepartureZh": "高鐵左營站",
        "DestinationZh": "高師大燕巢校區"
      },
      {
        "ddesc": "樹德科技大學－高師大燕巢校區",
        "seq": 40041,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/5041/map/1004/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 5041,
        "NameZh": "E04區間",
        "DepartureZh": "樹德科技大學",
        "DestinationZh": "高師大燕巢校區"
      },
      {
        "ddesc": "國軍高雄總醫院－樹德科技大學",
        "seq": 40090,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/509/map/1006/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 509,
        "NameZh": "E09鳳山燕巢城市快線",
        "DepartureZh": "國軍高雄總醫院",
        "DestinationZh": "樹德科技大學"
      },
      {
        "ddesc": "捷運草衙站－樹德科技大學",
        "seq": 40100,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/510/map/1008/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 510,
        "NameZh": "E10小港燕巢城市快線",
        "DepartureZh": "捷運草衙站",
        "DestinationZh": "樹德科技大學"
      },
      {
        "ddesc": "捷運大東站－高鐵新左營站",
        "seq": 40102,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/511/map/700/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 511,
        "NameZh": "E11A鳳山高鐵城市快線",
        "DepartureZh": "捷運大東站",
        "DestinationZh": "高鐵新左營站"
      },
      {
        "ddesc": "中崙國中－高鐵新左營站",
        "seq": 40110,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/5112/map/849/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 5112,
        "NameZh": "E11B鳳山高鐵城市快線",
        "DepartureZh": "中崙國中",
        "DestinationZh": "高鐵新左營站"
      },
      {
        "ddesc": "六龜新站 － 高雄車站",
        "seq": 40250,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8025/map/781/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8025,
        "NameZh": "E25高旗六龜快線(08:20前不行經高鐵左營站)",
        "DepartureZh": "六龜新站",
        "DestinationZh": "高雄車站"
      },
      {
        "ddesc": "高雄車站－美濃站",
        "seq": 40280,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8028/map/783/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8028,
        "NameZh": "E28高旗美濃快線",
        "DepartureZh": "高雄車站",
        "DestinationZh": "美濃站"
      },
      {
        "ddesc": "甲仙站－高雄車站",
        "seq": 40320,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8032/map/784/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8032,
        "NameZh": "E32高旗甲仙快線",
        "DepartureZh": "甲仙站",
        "DestinationZh": "高雄車站"
      }
    ]
  },
  {
    "id": "10",
    "name": "幹線",
    "description": "幹線",
    "seq": 2,
    "routes": [
      {
        "ddesc": "捷運衛武營站－鼓山輪渡站",
        "seq": 501,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/50/map/878/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 50,
        "NameZh": "50五福幹線",
        "DepartureZh": "捷運衛武營站",
        "DestinationZh": "鼓山輪渡站"
      },
      {
        "ddesc": "駁二藝術特區－夢裡活動中心",
        "seq": 600,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/60/map/793/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 60,
        "NameZh": "60覺民幹線(白天延駛澄清湖)",
        "DepartureZh": "駁二藝術特區",
        "DestinationZh": "夢裡活動中心"
      },
      {
        "ddesc": "小港站－高雄火車站",
        "seq": 690,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/69/map/421/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 69,
        "NameZh": "69A小港幹線",
        "DepartureZh": "小港站",
        "DestinationZh": "高雄火車站"
      },
      {
        "ddesc": "小港站－高雄火車站",
        "seq": 691,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/691/map/422/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 691,
        "NameZh": "69B小港幹線(延駛明鳳)",
        "DepartureZh": "小港站",
        "DestinationZh": "高雄火車站"
      },
      {
        "ddesc": "前鎮站－長庚紀念醫院",
        "seq": 700,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/70/map/424/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 70,
        "NameZh": "70A三多幹線(部分延駛澄清湖)",
        "DepartureZh": "前鎮站",
        "DestinationZh": "長庚紀念醫院"
      },
      {
        "ddesc": "前鎮站－長庚紀念醫院",
        "seq": 701,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/701/map/426/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 701,
        "NameZh": "70B三多幹線(延駛鎮州路)",
        "DepartureZh": "前鎮站",
        "DestinationZh": "長庚紀念醫院"
      },
      {
        "ddesc": "前鎮站－鳥松區公所",
        "seq": 703,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/703/map/428/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 703,
        "NameZh": "70D三多幹線(延駛仁美)",
        "DepartureZh": "前鎮站",
        "DestinationZh": "鳥松區公所"
      },
      {
        "ddesc": "金獅湖站－歷史博物館",
        "seq": 770,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/77/map/434/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 77,
        "NameZh": "77昌福幹線",
        "DepartureZh": "金獅湖站",
        "DestinationZh": "歷史博物館"
      },
      {
        "ddesc": "金獅湖站－高雄女中",
        "seq": 771,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/771/map/435/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 771,
        "NameZh": "77昌福幹線(繞三民高中)",
        "DepartureZh": "金獅湖站",
        "DestinationZh": "高雄女中"
      },
      {
        "ddesc": "鳳山轉運站－捷運鹽埕埔站",
        "seq": 880,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/122/map/311/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 122,
        "NameZh": "88建國幹線",
        "DepartureZh": "黃埔公園",
        "DestinationZh": "捷運鹽埕埔站"
      },
      {
        "ddesc": "捷運衛武營站－捷運鹽埕埔站",
        "seq": 881,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/1221/map/312/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 1221,
        "NameZh": "88建國幹線(延駛市議會)",
        "DepartureZh": "捷運衛武營站",
        "DestinationZh": "捷運鹽埕埔站"
      },
      {
        "ddesc": "臺鐵鳳山站－捷運鹽埕埔站",
        "seq": 882,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/882/map/738/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 882,
        "NameZh": "88建國幹線(區間)",
        "DepartureZh": "臺鐵鳳山站",
        "DestinationZh": "捷運鹽埕埔站"
      },
      {
        "ddesc": "高鐵左營站－捷運三多商圈站",
        "seq": 900,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/90/map/548/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 90,
        "NameZh": "90民族幹線",
        "DepartureZh": "高鐵左營站",
        "DestinationZh": "捷運三多商圈站"
      },
      {
        "ddesc": "高鐵左營站－高雄火車站(同愛街口)",
        "seq": 920,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/92/map/915/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 92,
        "NameZh": "92自由幹線",
        "DepartureZh": "高鐵左營站",
        "DestinationZh": "高雄火車站(同愛街口)"
      },
      {
        "ddesc": "瑞豐站－高雄火車站",
        "seq": 1000,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/100/map/898/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 100,
        "NameZh": "100百貨幹線",
        "DepartureZh": "瑞豐站",
        "DestinationZh": "高雄火車站"
      },
      {
        "ddesc": "金獅湖站－金獅湖站",
        "seq": 1680,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2061/map/912/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 2061,
        "NameZh": "168環西幹線",
        "DepartureZh": "金獅湖站",
        "DestinationZh": "輕軌夢時代站"
      },
      {
        "ddesc": "金獅湖站－金獅湖站",
        "seq": 1681,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2071/map/913/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 2071,
        "NameZh": "168環東幹線",
        "DepartureZh": "金獅湖站",
        "DestinationZh": "輕軌夢時代站"
      },
      {
        "ddesc": "加昌站－輕軌夢時代站",
        "seq": 2050,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/114/map/844/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 114,
        "NameZh": "205中華幹線",
        "DepartureZh": "加昌站",
        "DestinationZh": "輕軌夢時代站"
      },
      {
        "ddesc": "加昌站－高雄火車站",
        "seq": 2051,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/115/map/659/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 115,
        "NameZh": "205中華幹線區間車",
        "DepartureZh": "加昌站",
        "DestinationZh": "高雄火車站"
      },
      {
        "ddesc": "加昌站－鳥松區公所",
        "seq": 2170,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/217/map/736/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 217,
        "NameZh": "217A新昌幹線",
        "DepartureZh": "加昌站",
        "DestinationZh": "鳥松區公所"
      },
      {
        "ddesc": "高雄應用科技大學->加昌站",
        "seq": 2173,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2173/map/862/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 2173,
        "NameZh": "217D新昌幹線(高應大發車)",
        "DepartureZh": "高雄科大(建工)",
        "DestinationZh": "加昌站"
      },
      {
        "ddesc": "大華村->加昌站",
        "seq": 2174,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2174/map/863/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 2174,
        "NameZh": "217E新昌幹線(大華村發車)",
        "DepartureZh": "大華村",
        "DestinationZh": "加昌站"
      },
      {
        "ddesc": "小港站－林園區公所",
        "seq": 20030,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/211/map/335/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 211,
        "NameZh": "紅3A林園幹線(行經鳳鳴國小)",
        "DepartureZh": "小港站",
        "DestinationZh": "林園區公所"
      },
      {
        "ddesc": "小港站－林園區公所",
        "seq": 20031,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2111/map/336/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 2111,
        "NameZh": "紅3B林園幹線",
        "DepartureZh": "小港站",
        "DestinationZh": "林園區公所"
      },
      {
        "ddesc": "小港站－林園區公所",
        "seq": 20032,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2112/map/337/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 2112,
        "NameZh": "紅3C林園幹線(不經西溪里)",
        "DepartureZh": "小港站",
        "DestinationZh": "林園區公所"
      },
      {
        "ddesc": "小港站－林園區公所",
        "seq": 20033,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2113/map/900/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 2113,
        "NameZh": "紅3D林園幹線(不經西溪里.宏平路)",
        "DepartureZh": "小港站",
        "DestinationZh": "林園區公所"
      },
      {
        "ddesc": "小港站－林園區公所",
        "seq": 20034,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2114/map/339/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 2114,
        "NameZh": "紅3E林園幹線(延駛鳳鳴里)",
        "DepartureZh": "小港站",
        "DestinationZh": "林園區公所"
      },
      {
        "ddesc": "前鎮站－旗津輪渡站",
        "seq": 20090,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/116/map/304/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 116,
        "NameZh": "紅9A旗津幹線",
        "DepartureZh": "前鎮站",
        "DestinationZh": "旗津輪渡站"
      },
      {
        "ddesc": "旗津海水浴場－旗津海岸公園",
        "seq": 20092,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/1161/map/305/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 1161,
        "NameZh": "紅9B假日觀光公車(延駛旗津漁港)",
        "DepartureZh": "旗津海水浴場",
        "DestinationZh": "旗津海岸公園"
      },
      {
        "ddesc": "旗津海水浴場－旗津海岸公園",
        "seq": 20093,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/1162/map/306/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 1162,
        "NameZh": "紅9C假日觀光公車",
        "DepartureZh": "旗津海水浴場",
        "DestinationZh": "旗津海岸公園"
      },
      {
        "ddesc": "市立聯合醫院－鳳山商工",
        "seq": 20330,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/833/map/968/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 833,
        "NameZh": "紅33明誠幹線",
        "DepartureZh": "市立聯合醫院",
        "DestinationZh": "鳳山商工"
      },
      {
        "ddesc": "臺鐵新左營站－蚵仔寮漁港",
        "seq": 20531,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8531/map/921/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 8531,
        "NameZh": "紅53梓官幹線-主",
        "DepartureZh": "台鐵新左營站",
        "DestinationZh": "蚵仔寮漁港"
      },
      {
        "ddesc": "臺鐵新左營站－蚵仔寮漁港",
        "seq": 20534,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8533/map/922/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8533,
        "NameZh": "紅53梓官幹線-副",
        "DepartureZh": "台鐵新左營站",
        "DestinationZh": "蚵仔寮漁港"
      },
      {
        "ddesc": "臺鐵新左營站－援中國小",
        "seq": 20550,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8532/map/972/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 8532,
        "NameZh": "紅53梓官幹線-區間(原 紅55)",
        "DepartureZh": "台鐵新左營站",
        "DestinationZh": "中興里(楠梓)"
      },
      {
        "ddesc": "中崙社區－長庚紀念醫院",
        "seq": 90120,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/912/map/808/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 912,
        "NameZh": "橘12鳳青幹線(白天延駛澄清湖)",
        "DepartureZh": "中崙國中",
        "DestinationZh": "長庚醫院"
      },
      {
        "ddesc": "捷運鳳山西站－鳳新高中",
        "seq": 90121,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9121/map/557/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 9121,
        "NameZh": "橘12鳳青幹線區間車(寒暑假停駛)",
        "DepartureZh": "捷運鳳山西站",
        "DestinationZh": "鳳新高中 "
      }
    ]
  },
  {
    "id": "7",
    "name": "紅線",
    "description": "紅線",
    "seq": 3,
    "routes": [
      {
        "ddesc": "高雄餐旅大學－捷運小港站",
        "seq": 20010,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/801/map/440/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 801,
        "NameZh": "紅1",
        "DepartureZh": "高雄餐旅大學",
        "DestinationZh": "捷運小港站"
      },
      {
        "ddesc": "高雄餐旅大學－中鋼",
        "seq": 20011,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8100/map/479/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8100,
        "NameZh": "紅1(延駛中鋼公司)",
        "DepartureZh": "高雄餐旅大學",
        "DestinationZh": "中鋼"
      },
      {
        "ddesc": "高雄餐旅大學(校區)－捷運小港站",
        "seq": 20012,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8200/map/1020/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8200,
        "NameZh": "紅1(區間)",
        "DepartureZh": "高雄餐旅大學(校區)",
        "DestinationZh": "捷運小港站"
      },
      {
        "ddesc": "小港站－鳳鼻頭港",
        "seq": 20020,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/179/map/656/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 179,
        "NameZh": "紅2A",
        "DepartureZh": "小港站",
        "DestinationZh": "鳳鼻頭港"
      },
      {
        "ddesc": "小港站－鳳鼻頭港",
        "seq": 20021,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/1791/map/657/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 1791,
        "NameZh": "紅2B(延駛職訓中心)",
        "DepartureZh": "小港站",
        "DestinationZh": "鳳鼻頭港"
      },
      {
        "ddesc": "小港站－鳳鼻頭港",
        "seq": 20022,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/1792/map/658/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 1792,
        "NameZh": "紅2C(直達車)",
        "DepartureZh": "小港站",
        "DestinationZh": "鳳鼻頭港"
      },
      {
        "ddesc": "小港站－林園區公所",
        "seq": 20030,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/211/map/335/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 211,
        "NameZh": "紅3A林園幹線(行經鳳鳴國小)",
        "DepartureZh": "小港站",
        "DestinationZh": "林園區公所"
      },
      {
        "ddesc": "小港站－林園區公所",
        "seq": 20031,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2111/map/336/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 2111,
        "NameZh": "紅3B林園幹線",
        "DepartureZh": "小港站",
        "DestinationZh": "林園區公所"
      },
      {
        "ddesc": "小港站－林園區公所",
        "seq": 20032,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2112/map/337/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 2112,
        "NameZh": "紅3C林園幹線(不經西溪里)",
        "DepartureZh": "小港站",
        "DestinationZh": "林園區公所"
      },
      {
        "ddesc": "小港站－林園區公所",
        "seq": 20033,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2113/map/900/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 2113,
        "NameZh": "紅3D林園幹線(不經西溪里.宏平路)",
        "DepartureZh": "小港站",
        "DestinationZh": "林園區公所"
      },
      {
        "ddesc": "小港站－林園區公所",
        "seq": 20034,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2114/map/339/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 2114,
        "NameZh": "紅3E林園幹線(延駛鳳鳴里)",
        "DepartureZh": "小港站",
        "DestinationZh": "林園區公所"
      },
      {
        "ddesc": "小港站－林園區公所",
        "seq": 20035,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2115/map/586/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 2115,
        "NameZh": "紅3公車式小黃(西溪里)",
        "DepartureZh": "新光人壽",
        "DestinationZh": "林園區公所"
      },
      {
        "ddesc": "捷運小港站－中智路郵局",
        "seq": 20050,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/213/map/831/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 213,
        "NameZh": "紅5 ",
        "DepartureZh": "捷運小港站",
        "DestinationZh": "中智路郵局"
      },
      {
        "ddesc": "捷運草衙站－小港站",
        "seq": 20060,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/806/map/466/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 806,
        "NameZh": "紅6",
        "DepartureZh": "捷運草衙站",
        "DestinationZh": "小港站"
      },
      {
        "ddesc": "捷運草衙站－小港站",
        "seq": 20060,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8061/map/588/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8061,
        "NameZh": "紅6公車式小黃",
        "DepartureZh": "捷運草衙站",
        "DestinationZh": "小港站"
      },
      {
        "ddesc": "后安路－孔宅(不經紅毛港)",
        "seq": 20070,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/807/map/470/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 807,
        "NameZh": "紅7A(不經紅毛港)",
        "DepartureZh": "后安路",
        "DestinationZh": "孔宅"
      },
      {
        "ddesc": "后安路－孔宅(經紅毛港)",
        "seq": 20071,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8071/map/472/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 8071,
        "NameZh": "紅7B(經紅毛港)",
        "DepartureZh": "后安路",
        "DestinationZh": "孔宅"
      },
      {
        "ddesc": "后安路－孔宅(延駛漁業署)",
        "seq": 20072,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8072/map/473/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8072,
        "NameZh": "紅7C(延駛漁業署)",
        "DepartureZh": "后安路",
        "DestinationZh": "孔宅"
      },
      {
        "ddesc": "小港站－輔英科大校區",
        "seq": 20080,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/362/map/384/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 362,
        "NameZh": "紅8A",
        "DepartureZh": "小港站",
        "DestinationZh": "輔英科大校區"
      },
      {
        "ddesc": "小港站－輔英科大校區",
        "seq": 20081,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/3621/map/385/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 3621,
        "NameZh": "紅8B(去繞昭明)",
        "DepartureZh": "小港站",
        "DestinationZh": "輔英科大校區"
      },
      {
        "ddesc": "小港站－輔英科大校區",
        "seq": 20082,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/3623/map/387/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 3623,
        "NameZh": "紅8C(返繞昭明)",
        "DepartureZh": "小港站",
        "DestinationZh": "輔英科大校區"
      },
      {
        "ddesc": "小港站－昭明變電所站",
        "seq": 20084,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/3624/map/388/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 3624,
        "NameZh": "紅8E",
        "DepartureZh": "小港站",
        "DestinationZh": "昭明變電所站"
      },
      {
        "ddesc": "前鎮站－旗津輪渡站",
        "seq": 20090,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/116/map/304/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 116,
        "NameZh": "紅9A旗津幹線",
        "DepartureZh": "前鎮站",
        "DestinationZh": "旗津輪渡站"
      },
      {
        "ddesc": "旗津海水浴場－旗津海岸公園",
        "seq": 20092,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/1161/map/305/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 1161,
        "NameZh": "紅9B假日觀光公車(延駛旗津漁港)",
        "DepartureZh": "旗津海水浴場",
        "DestinationZh": "旗津海岸公園"
      },
      {
        "ddesc": "旗津海水浴場－旗津海岸公園",
        "seq": 20093,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/1162/map/306/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 1162,
        "NameZh": "紅9C假日觀光公車",
        "DepartureZh": "旗津海水浴場",
        "DestinationZh": "旗津海岸公園"
      },
      {
        "ddesc": "前鎮高中－臺鐵鳳山站",
        "seq": 20100,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/810/map/964/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 810,
        "NameZh": "紅10",
        "DepartureZh": "前鎮高中",
        "DestinationZh": "臺鐵鳳山站"
      },
      {
        "ddesc": "中崙四路－前鎮高中",
        "seq": 20100,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8103/map/1019/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 8103,
        "NameZh": "紅10(區間車)",
        "DepartureZh": "前鎮高中站",
        "DestinationZh": ""
      },
      {
        "ddesc": "過埤派出所－捷運前鎮高中站",
        "seq": 20110,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/811/map/780/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 811,
        "NameZh": "紅11A",
        "DepartureZh": "過埤派出所",
        "DestinationZh": "捷運前鎮高中站"
      },
      {
        "ddesc": "過埤派出所－捷運前鎮高中站",
        "seq": 20110,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8111/map/991/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 8111,
        "NameZh": "紅11B",
        "DepartureZh": "過埤派出所",
        "DestinationZh": "捷運前鎮高中站"
      },
      {
        "ddesc": "過埤派出所－捷運前鎮高中站",
        "seq": 20112,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8112/map/908/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8112,
        "NameZh": "紅11公車式小黃",
        "DepartureZh": "過埤派出所",
        "DestinationZh": "捷運前鎮高中站"
      },
      {
        "ddesc": "公車瑞豐站－公車前鎮站",
        "seq": 20120,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/812/map/482/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 812,
        "NameZh": "紅12A",
        "DepartureZh": "公車瑞豐站",
        "DestinationZh": "公車前鎮站"
      },
      {
        "ddesc": "公車瑞豐站－高雄加工出口區",
        "seq": 20121,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8121/map/990/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 8121,
        "NameZh": "紅12B(延駛加工出口區)",
        "DepartureZh": "公車瑞豐站",
        "DestinationZh": "公車前鎮站"
      },
      {
        "ddesc": "軟體園區－捷運三多商圈站",
        "seq": 20160,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8161/map/723/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8161,
        "NameZh": "紅16公車式小黃",
        "DepartureZh": "軟體園區",
        "DestinationZh": "捷運三多商圈站"
      },
      {
        "ddesc": "軟體園區－捷運三多商圈站",
        "seq": 20160,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/816/map/670/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 816,
        "NameZh": "紅16",
        "DepartureZh": "軟體園區",
        "DestinationZh": "捷運三多商圈站"
      },
      {
        "ddesc": "中崙社區－實踐大學",
        "seq": 20180,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/818/map/937/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 818,
        "NameZh": "紅18",
        "DepartureZh": "中崙社區",
        "DestinationZh": "實踐大學"
      },
      {
        "ddesc": "捷運衛武營站－捷運三多商圈站",
        "seq": 20210,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/821/map/719/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 821,
        "NameZh": "紅21",
        "DepartureZh": "捷運衛武營站",
        "DestinationZh": "捷運三多商圈站"
      },
      {
        "ddesc": "捷運衛武營站－新光路口",
        "seq": 20211,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/822/map/710/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 822,
        "NameZh": "紅22(原 紅21區)",
        "DepartureZh": "捷運衛武營站",
        "DestinationZh": "圖書總館"
      },
      {
        "ddesc": "左營南站－高雄火車站(同愛街口)",
        "seq": 20250,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/825/map/924/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 825,
        "NameZh": "紅25",
        "DepartureZh": "左營南站",
        "DestinationZh": "高雄火車站(同愛街口)"
      },
      {
        "ddesc": "左營南站－高雄火車站(同愛街口)",
        "seq": 20251,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8251/map/1001/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 8251,
        "NameZh": "紅25繞",
        "DepartureZh": "先勝路口",
        "DestinationZh": "高雄火車站"
      },
      {
        "ddesc": "高雄火車站－中都",
        "seq": 20271,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/827/map/606/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 827,
        "NameZh": "紅27",
        "DepartureZh": "高雄火車站",
        "DestinationZh": "中都街口"
      },
      {
        "ddesc": "客家文物館－汾陽路口",
        "seq": 20280,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8282/map/724/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8282,
        "NameZh": "紅28公車式小黃",
        "DepartureZh": "客家文物館",
        "DestinationZh": "汾陽路口"
      },
      {
        "ddesc": "客家文物館－汾陽路口",
        "seq": 20280,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/828/map/832/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 828,
        "NameZh": "紅28",
        "DepartureZh": "客家文物館",
        "DestinationZh": "汾陽路口"
      },
      {
        "ddesc": "中都濕地公園－汾陽路口",
        "seq": 20281,
        "routeMapImageUrl": "",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8283,
        "NameZh": "紅28公車式小黃(繞中都濕地公園)",
        "DepartureZh": "察哈爾街口",
        "DestinationZh": "汾陽路口"
      },
      {
        "ddesc": "中都濕地公園－汾陽路口",
        "seq": 20281,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8281/map/833/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8281,
        "NameZh": "紅28(繞中都濕地公園)",
        "DepartureZh": "察哈爾街口",
        "DestinationZh": "汾陽路口"
      },
      {
        "ddesc": "捷運後驛站－陽明國小",
        "seq": 20290,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/829/map/492/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 829,
        "NameZh": "紅29",
        "DepartureZh": "捷運後驛站",
        "DestinationZh": "陽明國小"
      },
      {
        "ddesc": "皓東路口－高雄火車站",
        "seq": 20300,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/831/map/988/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 831,
        "NameZh": "紅31",
        "DepartureZh": "皓東路口",
        "DestinationZh": "高雄火車站"
      },
      {
        "ddesc": "澄清湖棒球場－高雄火車站",
        "seq": 20300,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/830/map/985/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 830,
        "NameZh": "紅30",
        "DepartureZh": "忠誠路口",
        "DestinationZh": "高雄火車站"
      },
      {
        "ddesc": "高雄科大(建工)-捷運後驛站",
        "seq": 20301,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8301/map/989/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 8301,
        "NameZh": "紅30區",
        "DepartureZh": "高雄科大(建工)",
        "DestinationZh": "捷運後驛站"
      },
      {
        "ddesc": "捷運凹仔底站－大榮高中",
        "seq": 20320,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/832/map/496/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 832,
        "NameZh": "紅32A",
        "DepartureZh": "捷運凹子底站",
        "DestinationZh": "國泰市場"
      },
      {
        "ddesc": "捷運凹仔底站－鼓山高中",
        "seq": 20321,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8321/map/497/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8321,
        "NameZh": "紅32B(學校寒暑假停駛)",
        "DepartureZh": "捷運凹子底站",
        "DestinationZh": "鼓山高中"
      },
      {
        "ddesc": "市立聯合醫院－鳳山商工",
        "seq": 20330,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/833/map/968/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 833,
        "NameZh": "紅33明誠幹線",
        "DepartureZh": "市立聯合醫院",
        "DestinationZh": "鳳山商工"
      },
      {
        "ddesc": "金獅湖站－捷運凹子底站",
        "seq": 20350,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/210/map/834/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 210,
        "NameZh": "紅35A",
        "DepartureZh": "金獅湖站",
        "DestinationZh": "捷運凹子底站"
      },
      {
        "ddesc": "金獅湖站－捷運凹子底站",
        "seq": 20351,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2101/map/835/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 2101,
        "NameZh": "紅35B",
        "DepartureZh": "金獅湖站",
        "DestinationZh": "捷運凹子底站"
      },
      {
        "ddesc": "金獅湖站－捷運凹子底站",
        "seq": 20352,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2102/map/836/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 2102,
        "NameZh": "紅35A(繞崇實社區)",
        "DepartureZh": "金獅湖站",
        "DestinationZh": "捷運凹子底站"
      },
      {
        "ddesc": "左營海軍軍區－文藻外語",
        "seq": 20360,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/836/map/499/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 836,
        "NameZh": "紅36",
        "DepartureZh": "左營海軍軍區",
        "DestinationZh": "文藻外語大學"
      },
      {
        "ddesc": "左營海軍軍區－文藻外語",
        "seq": 20361,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8361/map/500/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 8361,
        "NameZh": "紅36(17:08－23:30繞文信路)",
        "DepartureZh": "左營海軍軍區",
        "DestinationZh": "文藻外語大學"
      },
      {
        "ddesc": "捷運生態園區－榮總側門",
        "seq": 20500,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/850/map/837/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 850,
        "NameZh": "紅50",
        "DepartureZh": "捷運生態園區",
        "DestinationZh": "榮總側門"
      },
      {
        "ddesc": "捷運生態園區－榮總側門",
        "seq": 20500,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8509/map/510/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 8509,
        "NameZh": "紅50公車式小黃",
        "DepartureZh": "捷運生態園區",
        "DestinationZh": "榮總側門"
      },
      {
        "ddesc": "臺鐵新左營站－蓮池潭",
        "seq": 20510,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/851/map/595/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 851,
        "NameZh": "紅51A",
        "DepartureZh": "台鐵新左營站",
        "DestinationZh": "蓮池潭"
      },
      {
        "ddesc": "臺鐵新左營站－蓮池潭",
        "seq": 20510,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8513/map/594/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8513,
        "NameZh": "紅51A公車式小黃",
        "DepartureZh": "台鐵新左營站",
        "DestinationZh": "蓮池潭"
      },
      {
        "ddesc": "臺鐵新左營站－捷運生態園區站",
        "seq": 20511,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8515/map/725/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8515,
        "NameZh": "紅51B公車式小黃",
        "DepartureZh": "台鐵新左營站",
        "DestinationZh": "捷運生態園區站"
      },
      {
        "ddesc": "臺鐵新左營站－捷運生態園區站",
        "seq": 20511,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8511/map/512/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8511,
        "NameZh": "紅51B",
        "DepartureZh": "台鐵新左營站",
        "DestinationZh": "捷運生態園區站"
      },
      {
        "ddesc": "臺鐵新左營站－高雄物產館",
        "seq": 20512,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8514/map/726/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8514,
        "NameZh": "紅51C公車式小黃",
        "DepartureZh": "台鐵新左營站",
        "DestinationZh": "高雄物產館"
      },
      {
        "ddesc": "臺鐵新左營站－中山大學",
        "seq": 20520,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/852/map/928/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 852,
        "NameZh": "紅52(原 E05)",
        "DepartureZh": "台鐵新左營站",
        "DestinationZh": "中山大學"
      },
      {
        "ddesc": "臺鐵新左營站－蚵仔寮漁港",
        "seq": 20531,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8531/map/921/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 8531,
        "NameZh": "紅53梓官幹線-主",
        "DepartureZh": "台鐵新左營站",
        "DestinationZh": "蚵仔寮漁港"
      },
      {
        "ddesc": "臺鐵新左營站－蚵仔寮漁港",
        "seq": 20534,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8533/map/922/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8533,
        "NameZh": "紅53梓官幹線-副",
        "DepartureZh": "台鐵新左營站",
        "DestinationZh": "蚵仔寮漁港"
      },
      {
        "ddesc": "臺鐵新左營站－援中國小",
        "seq": 20550,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8532/map/972/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 8532,
        "NameZh": "紅53梓官幹線-區間(原 紅55)",
        "DepartureZh": "台鐵新左營站",
        "DestinationZh": "中興里(楠梓)"
      },
      {
        "ddesc": "捷運楠梓加工區－高雄大學",
        "seq": 20560,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/856/map/840/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 856,
        "NameZh": "紅56A",
        "DepartureZh": "捷運楠梓加工區",
        "DestinationZh": "高雄大學"
      },
      {
        "ddesc": "捷運楠梓加工區－高雄大學",
        "seq": 20561,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8561/map/841/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 8561,
        "NameZh": "紅56B(延駛德民黃昏市場)",
        "DepartureZh": "捷運楠梓加工區",
        "DestinationZh": "高雄大學"
      },
      {
        "ddesc": "捷運楠梓加工區－高雄大學",
        "seq": 20562,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8562/map/842/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8562,
        "NameZh": "紅56C(學校寒暑假停駛)",
        "DepartureZh": "捷運楠梓加工區",
        "DestinationZh": "高雄大學"
      },
      {
        "ddesc": "東六街口－創意南路",
        "seq": 20570,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/857/map/621/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 857,
        "NameZh": "紅57",
        "DepartureZh": "東六街口",
        "DestinationZh": "創意南路"
      },
      {
        "ddesc": "捷運楠梓加工區－捷運楠梓加工區",
        "seq": 20571,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8571/map/622/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8571,
        "NameZh": "紅57(區間車)",
        "DepartureZh": "捷運楠梓加工區",
        "DestinationZh": "員工服務中心"
      },
      {
        "ddesc": "捷運都會公園站－高雄第一科大(西校區)",
        "seq": 20580,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/858/map/524/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 858,
        "NameZh": "紅58A",
        "DepartureZh": "捷運都會公園站",
        "DestinationZh": "高雄科大(第一西校區)"
      },
      {
        "ddesc": "捷運都會公園站－燕巢區公所",
        "seq": 20581,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8581/map/525/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 8581,
        "NameZh": "紅58B",
        "DepartureZh": "捷運都會公園站",
        "DestinationZh": "燕巢區公所"
      },
      {
        "ddesc": "捷運都會公園站－楠梓高中",
        "seq": 20582,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8582/map/526/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8582,
        "NameZh": "紅58C(學校寒暑假停駛)",
        "DepartureZh": "捷運都會公園站",
        "DestinationZh": "楠梓高中"
      },
      {
        "ddesc": "高鐵左營站－蓮池潭",
        "seq": 20590,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/859/map/1010/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 859,
        "NameZh": "紅59",
        "DepartureZh": "高鐵左營站",
        "DestinationZh": "蓮池潭"
      },
      {
        "ddesc": "左營高鐵站－仁武區公所",
        "seq": 20600,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/860/map/791/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 860,
        "NameZh": "紅60A",
        "DepartureZh": "左營高鐵站",
        "DestinationZh": "仁德街口(中華路)"
      },
      {
        "ddesc": "左營高鐵站－加昌站",
        "seq": 20601,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8601/map/792/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 8601,
        "NameZh": "紅60B",
        "DepartureZh": "左營高鐵站",
        "DestinationZh": "加昌站"
      },
      {
        "ddesc": "漢翔公司－捷運南岡山站",
        "seq": 20680,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/868/map/530/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 868,
        "NameZh": "紅68A",
        "DepartureZh": "漢翔公司",
        "DestinationZh": "捷運南岡山站"
      },
      {
        "ddesc": "大莊里－捷運南岡山站",
        "seq": 20681,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8682/map/532/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8682,
        "NameZh": "紅68A(延駛大莊里)",
        "DepartureZh": "大莊里",
        "DestinationZh": "捷運南岡山站"
      },
      {
        "ddesc": "捷運南岡山站－漢翔公司",
        "seq": 20682,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8681/map/531/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8681,
        "NameZh": "紅68B(部分班次延駛嘉興國中)",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "漢翔公司"
      },
      {
        "ddesc": "捷運南岡山站－燁聯鋼鐵",
        "seq": 20683,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/865/map/705/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 865,
        "NameZh": "紅65(原 紅68C(週一~週五延駛燁聯鋼鐵))",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "燁聯鋼鐵"
      },
      {
        "ddesc": "捷運南岡山站－岡山高中",
        "seq": 20690,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/869/map/717/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 869,
        "NameZh": "紅69 (原 紅69A)",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "岡山高中"
      },
      {
        "ddesc": "捷運南岡山站－岡山高中",
        "seq": 20690,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8695/map/727/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8695,
        "NameZh": "紅69A公車式小黃",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "岡山高中"
      },
      {
        "ddesc": "捷運南岡山站－高雄科學園區 ",
        "seq": 20691,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/867/map/708/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 867,
        "NameZh": "紅67A部分班次延駛中正堂(原 紅69B)",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "高雄科學園區"
      },
      {
        "ddesc": "捷運南岡山站－永安工業區",
        "seq": 20692,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/866/map/707/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 866,
        "NameZh": "紅66(原 紅69C(延駛本洲工業區))",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "永安工業區"
      },
      {
        "ddesc": "捷運南岡山站－順安宮",
        "seq": 20693,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8671/map/709/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8671,
        "NameZh": "紅67B(原 紅69D)",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "順安宮"
      },
      {
        "ddesc": "捷運南岡山站－田寮區公所",
        "seq": 20700,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/253/map/369/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 253,
        "NameZh": "紅70A",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "田寮區公所"
      },
      {
        "ddesc": "捷運南岡山站－隆后宮",
        "seq": 20701,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2533/map/370/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 2533,
        "NameZh": "紅70B(延駛隆后宮)",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "隆后宮"
      },
      {
        "ddesc": "捷運南岡山站－茄萣區公所",
        "seq": 20710,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/871/map/995/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 871,
        "NameZh": "紅71A",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "茄萣區公所"
      },
      {
        "ddesc": "捷運南岡山站－茄萣區公所",
        "seq": 20711,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8711/map/996/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8711,
        "NameZh": "紅71B1",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "茄萣區公所"
      },
      {
        "ddesc": "捷運南岡山站－茄萣區公所",
        "seq": 20712,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8712/map/541/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8712,
        "NameZh": "紅71B2",
        "DepartureZh": "興達國小",
        "DestinationZh": "茄萣區公所"
      },
      {
        "ddesc": "加昌站－捷運南岡山站",
        "seq": 20713,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8713/map/1011/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8713,
        "NameZh": "紅71C",
        "DepartureZh": "加昌站",
        "DestinationZh": "捷運南岡山站"
      },
      {
        "ddesc": "捷運南岡山站－東方設計學院",
        "seq": 20714,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8714/map/998/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8714,
        "NameZh": "紅71D",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "東方設計學院"
      },
      {
        "ddesc": "捷運橋頭火車站－彌陀國小",
        "seq": 20720,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/872/map/761/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 872,
        "NameZh": "紅72A(部份班次延駛安樂宮)",
        "DepartureZh": "捷運橋頭火車站",
        "DestinationZh": "彌陀國小"
      },
      {
        "ddesc": "捷運橋頭火車站－彌陀國小",
        "seq": 20721,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8723/map/757/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8723,
        "NameZh": "紅72A(延駛南安國小)",
        "DepartureZh": "捷運橋頭火車站",
        "DestinationZh": "彌陀國小"
      },
      {
        "ddesc": "捷運橋頭火車站－永安區公所",
        "seq": 20722,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8721/map/758/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 8721,
        "NameZh": "紅72B(部分班次延駛保寧)",
        "DepartureZh": "捷運橋頭火車站",
        "DestinationZh": "永安區公所"
      },
      {
        "ddesc": "捷運橋頭火車站－彌陀國小",
        "seq": 20723,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8722/map/759/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8722,
        "NameZh": "紅72C(部份班次延駛安樂宮)",
        "DepartureZh": "捷運橋頭火車站",
        "DestinationZh": "彌陀國小"
      },
      {
        "ddesc": "捷運橋頭火車站－彌陀國小",
        "seq": 20724,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8724/map/583/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8724,
        "NameZh": "紅72C(延駛南安國小)",
        "DepartureZh": "捷運橋頭火車站",
        "DestinationZh": "彌陀國小"
      },
      {
        "ddesc": "岡山轉運站－阿蓮區公所",
        "seq": 20730,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/221/map/999/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 221,
        "NameZh": "紅73A",
        "DepartureZh": "岡山轉運站",
        "DestinationZh": "阿蓮區公所"
      },
      {
        "ddesc": "岡山轉運站－阿蓮區公所",
        "seq": 20731,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8732/map/1000/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8732,
        "NameZh": "紅73B",
        "DepartureZh": "岡山轉運站",
        "DestinationZh": "阿蓮區公所"
      },
      {
        "ddesc": "捷運南岡山站－南寮",
        "seq": 20780,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/878/map/663/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 878,
        "NameZh": "紅78",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "南寮"
      },
      {
        "ddesc": "捷運南岡山站－塭仔邊",
        "seq": 20790,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/879/map/662/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 879,
        "NameZh": "紅79",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "塭仔邊"
      }
    ]
  },
  {
    "id": "8",
    "name": "橘線",
    "description": "橘線",
    "seq": 4,
    "routes": [
      {
        "ddesc": "捷運西子灣2號出口－中山大學行政大樓",
        "seq": 90010,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/901/map/825/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 901,
        "NameZh": "橘1A",
        "DepartureZh": "捷運西子灣2號出口",
        "DestinationZh": "中山大學行政大樓"
      },
      {
        "ddesc": "捷運西子灣2號出口－西子灣",
        "seq": 90011,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9011/map/826/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 9011,
        "NameZh": "橘1B(平日停駛)",
        "DepartureZh": "捷運西子灣2號出口",
        "DestinationZh": "中山大學"
      },
      {
        "ddesc": "捷運西子灣2號出口－中山大學文學院",
        "seq": 90012,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9012/map/827/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 9012,
        "NameZh": "橘1C(延駛中山大學文學院)",
        "DepartureZh": "捷運西子灣2號出口",
        "DestinationZh": "中山大學文學院"
      },
      {
        "ddesc": "捷運衛武營站－舊鐵橋濕地",
        "seq": 90070,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/1601/map/820/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 1601,
        "NameZh": "橘7A(不延駛)",
        "DepartureZh": "捷運衛武營站",
        "DestinationZh": "舊鐵橋濕地"
      },
      {
        "ddesc": "大樹區公所->捷運衛武營站",
        "seq": 90071,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/1602/map/957/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 1602,
        "NameZh": "橘7A(去程放空)",
        "DepartureZh": "",
        "DestinationZh": ""
      },
      {
        "ddesc": "捷運衛武營站－大樹衛生所",
        "seq": 90072,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/160/map/821/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 160,
        "NameZh": "橘7A(延駛大樹衛生所)",
        "DepartureZh": "捷運衛武營站",
        "DestinationZh": "大樹衛生所"
      },
      {
        "ddesc": "捷運衛武營站－大樹衛生所",
        "seq": 90073,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9702/map/822/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 9702,
        "NameZh": "橘7B(不延駛佛陀紀念館)",
        "DepartureZh": "捷運衛武營站",
        "DestinationZh": "大樹衛生所"
      },
      {
        "ddesc": "捷運衛武營站－佛陀紀念館",
        "seq": 90074,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9701/map/823/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 9701,
        "NameZh": "橘7B(假日停駛)",
        "DepartureZh": "捷運衛武營站",
        "DestinationZh": "佛陀紀念館"
      },
      {
        "ddesc": "捷運衛武營站－過埤派出所",
        "seq": 90080,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/908/map/981/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 908,
        "NameZh": "橘8",
        "DepartureZh": "捷運衛武營站",
        "DestinationZh": "過埤派出所"
      },
      {
        "ddesc": "捷運衛武營站－鳳山轉運站",
        "seq": 90100,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/910/map/903/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 910,
        "NameZh": "橘10A(假日停駛)",
        "DepartureZh": "捷運衛武營站",
        "DestinationZh": "臺鐵鳳山站"
      },
      {
        "ddesc": "捷運衛武營站－鳳山轉運站",
        "seq": 90101,
        "routeMapImageUrl": "",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 9103,
        "NameZh": "橘10B公車式小黃",
        "DepartureZh": "捷運衛武營站",
        "DestinationZh": "臺鐵鳳山站"
      },
      {
        "ddesc": "林園站－臺鐵鳳山站",
        "seq": 90110,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9112/map/664/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 9112,
        "NameZh": "橘11A(區間車)",
        "DepartureZh": "林園站",
        "DestinationZh": "臺鐵鳳山站"
      },
      {
        "ddesc": "林園站－捷運衛武營站",
        "seq": 90110,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/911/map/864/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 911,
        "NameZh": "橘11A",
        "DepartureZh": "林園站",
        "DestinationZh": "捷運衛武營站"
      },
      {
        "ddesc": "林園站－捷運衛武營站",
        "seq": 90111,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9111/map/865/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 9111,
        "NameZh": "橘11B",
        "DepartureZh": "林園站",
        "DestinationZh": "捷運衛武營站"
      },
      {
        "ddesc": "中崙社區－長庚紀念醫院",
        "seq": 90120,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/912/map/808/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 912,
        "NameZh": "橘12鳳青幹線(白天延駛澄清湖)",
        "DepartureZh": "中崙國中",
        "DestinationZh": "長庚醫院"
      },
      {
        "ddesc": "捷運鳳山西站－鳳新高中",
        "seq": 90121,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9121/map/557/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 9121,
        "NameZh": "橘12鳳青幹線區間車(寒暑假停駛)",
        "DepartureZh": "捷運鳳山西站",
        "DestinationZh": "鳳新高中 "
      },
      {
        "ddesc": "鳳山轉運站－仁武區公所",
        "seq": 90160,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/916/map/752/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 916,
        "NameZh": "橘16",
        "DepartureZh": "臺鐵鳳山站",
        "DestinationZh": "仁武高中"
      },
      {
        "ddesc": "鳳山轉運站－大社區公所",
        "seq": 90161,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9161/map/753/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 9161,
        "NameZh": "橘16(延駛大社)",
        "DepartureZh": "臺鐵鳳山站",
        "DestinationZh": "大社區公所"
      },
      {
        "ddesc": "鳳山轉運站－長庚醫院",
        "seq": 90170,
        "routeMapImageUrl": "",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 9171,
        "NameZh": "橘17(延駛)",
        "DepartureZh": "鳳山轉運站",
        "DestinationZh": "長庚醫院"
      },
      {
        "ddesc": "鳳山轉運站－長庚醫院",
        "seq": 90170,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/917/map/712/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 917,
        "NameZh": "橘17(原 橘7C)",
        "DepartureZh": "鳳山轉運站",
        "DestinationZh": "長庚醫院"
      },
      {
        "ddesc": "捷運大寮站－大寮農會",
        "seq": 90200,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/920/map/561/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 920,
        "NameZh": "橘20A",
        "DepartureZh": "捷運大寮站",
        "DestinationZh": "大寮農會"
      },
      {
        "ddesc": "捷運大寮站－上寮里",
        "seq": 90201,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9201/map/562/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 9201,
        "NameZh": "橘20B",
        "DepartureZh": "捷運大寮站",
        "DestinationZh": "上寮路口(大寮路)"
      },
      {
        "ddesc": "捷運大寮站－上寮里",
        "seq": 90202,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9205/map/566/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 9205,
        "NameZh": "橘20B(延駛輔英科大)",
        "DepartureZh": "捷運大寮站",
        "DestinationZh": "上寮路口(大寮路)"
      },
      {
        "ddesc": "捷運大寮站－輔英科大",
        "seq": 90203,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9206/map/806/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 9206,
        "NameZh": "橘20C(直達車)",
        "DepartureZh": "捷運大寮站",
        "DestinationZh": "輔英科大"
      },
      {
        "ddesc": "捷運大寮站－輔英科大",
        "seq": 90203,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9202/map/807/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 9202,
        "NameZh": "橘20C",
        "DepartureZh": "捷運大寮站",
        "DestinationZh": "輔英科大"
      },
      {
        "ddesc": "捷運大寮站－潮寮國中",
        "seq": 90204,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9203/map/564/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 9203,
        "NameZh": "橘20D(假日停駛)",
        "DepartureZh": "捷運大寮站",
        "DestinationZh": "潮寮國中"
      },
      {
        "ddesc": "和春技術學院(大發校區)－捷運大寮站",
        "seq": 90210,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/921/map/567/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 921,
        "NameZh": "橘21A公車式小黃",
        "DepartureZh": "和春技術學院(大發校區)",
        "DestinationZh": "捷運大寮站"
      },
      {
        "ddesc": "月天寺－捷運大寮站",
        "seq": 90211,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9211/map/568/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 9211,
        "NameZh": "橘21B公車式小黃",
        "DepartureZh": "月天寺",
        "DestinationZh": "捷運大寮站"
      },
      {
        "ddesc": "江山社區活動中心－捷運大寮站",
        "seq": 90220,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/922/map/569/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 922,
        "NameZh": "橘22公車式小黃",
        "DepartureZh": "後庄(鳳屏一路)",
        "DestinationZh": "捷運大寮站"
      },
      {
        "ddesc": "捷運大寮站－中庄",
        "seq": 90230,
        "routeMapImageUrl": "",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 9231,
        "NameZh": "橘23公車式小黃",
        "DepartureZh": "捷運大寮站",
        "DestinationZh": "中庄"
      }
    ]
  },
  {
    "id": "12",
    "name": "一般",
    "description": "一般",
    "seq": 5,
    "routes": [
      {
        "ddesc": "金獅湖站－金獅湖站",
        "seq": 10,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/1431/map/911/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 1431,
        "NameZh": "0北",
        "DepartureZh": "金獅湖站",
        "DestinationZh": "捷運鹽埕埔站"
      },
      {
        "ddesc": "金獅湖站－金獅湖站",
        "seq": 20,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/1421/map/914/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 1421,
        "NameZh": "0南",
        "DepartureZh": "金獅湖站",
        "DestinationZh": "捷運鹽埕埔站"
      },
      {
        "ddesc": "警廣站 －凹仔底森林公園",
        "seq": 30,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/3/map/871/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 3,
        "NameZh": "3",
        "DepartureZh": "警廣站",
        "DestinationZh": "凹仔底森林公園"
      },
      {
        "ddesc": "警廣站 －凹仔底森林公園",
        "seq": 31,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/31/map/872/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 31,
        "NameZh": "3繞(17:00－20:30繞文信路)",
        "DepartureZh": "警廣站",
        "DestinationZh": "凹仔底森林公園"
      },
      {
        "ddesc": "鳳山轉運站－北昌三街口",
        "seq": 50,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/51/map/940/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 51,
        "NameZh": "5(區間車)",
        "DepartureZh": "鳳山轉運站",
        "DestinationZh": "北昌三街口"
      },
      {
        "ddesc": "鳳山轉運站－關帝廟",
        "seq": 50,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/5/map/802/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 5,
        "NameZh": "5",
        "DepartureZh": "鳳山轉運站",
        "DestinationZh": "關帝廟"
      },
      {
        "ddesc": "左營南站－萬金路",
        "seq": 60,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/6/map/410/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 6,
        "NameZh": "6",
        "DepartureZh": "左營南站",
        "DestinationZh": "萬金路"
      },
      {
        "ddesc": "建楠路－左營南站",
        "seq": 61,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/61/map/716/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 61,
        "NameZh": "6(星展銀行發車)",
        "DepartureZh": "建楠路",
        "DestinationZh": "左營南站"
      },
      {
        "ddesc": "加昌站－高雄師範大學",
        "seq": 70,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/7/map/852/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 7,
        "NameZh": "7A",
        "DepartureZh": "加昌站",
        "DestinationZh": "高雄師範大學"
      },
      {
        "ddesc": "加昌站－樹德科技大學",
        "seq": 71,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/730/map/853/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 730,
        "NameZh": "7B(區間車)",
        "DepartureZh": "加昌站",
        "DestinationZh": "樹德科技大學"
      },
      {
        "ddesc": "加昌站－高雄科大(燕巢)",
        "seq": 72,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/731/map/854/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 731,
        "NameZh": "7C",
        "DepartureZh": "加昌站",
        "DestinationZh": "高雄科大(燕巢)"
      },
      {
        "ddesc": "加昌站－樹德科技大學",
        "seq": 73,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/732/map/760/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 732,
        "NameZh": "7D",
        "DepartureZh": "加昌站",
        "DestinationZh": "樹德科技大學"
      },
      {
        "ddesc": "瑞豐站－捷運鹽埕埔站",
        "seq": 110,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/11/map/295/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 11,
        "NameZh": "11",
        "DepartureZh": "瑞豐站",
        "DestinationZh": "捷運鹽埕埔站"
      },
      {
        "ddesc": "小港站－高雄火車站",
        "seq": 120,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/12/map/309/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 12,
        "NameZh": "12A",
        "DepartureZh": "小港站",
        "DestinationZh": "高雄火車站"
      },
      {
        "ddesc": "小港站－高雄火車站",
        "seq": 121,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/123/map/313/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 123,
        "NameZh": "12B(去程不繞駛飛機路)",
        "DepartureZh": "小港站",
        "DestinationZh": "高雄火車站"
      },
      {
        "ddesc": "小港站－高雄火車站",
        "seq": 122,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/121/map/310/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 121,
        "NameZh": "12C(延駛經大坪頂)",
        "DepartureZh": "小港站",
        "DestinationZh": "高雄火車站"
      },
      {
        "ddesc": "前鎮站－前鎮高中",
        "seq": 150,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/15/map/320/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 15,
        "NameZh": "15",
        "DepartureZh": "前鎮站",
        "DestinationZh": "前鎮高中"
      },
      {
        "ddesc": "小港站－前鎮站",
        "seq": 151,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/151/map/644/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 151,
        "NameZh": "15公車式小黃",
        "DepartureZh": "小港站",
        "DestinationZh": "前鎮站"
      },
      {
        "ddesc": "警廣站－高雄科大(建工)",
        "seq": 160,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/16/map/686/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 16,
        "NameZh": "16A(假日去程部分班次延駛高雄靈糧堂)",
        "DepartureZh": "警廣站",
        "DestinationZh": "鼎金國小"
      },
      {
        "ddesc": "警廣站－高雄科大(建工)",
        "seq": 161,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/161/map/687/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 161,
        "NameZh": "16A繞(1700-2150繞文信路口)",
        "DepartureZh": "警廣站",
        "DestinationZh": "鼎金國小"
      },
      {
        "ddesc": "警廣站－高雄科大(建工)",
        "seq": 162,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/162/map/688/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 162,
        "NameZh": "16B(假日去程部分班次延駛高雄靈糧堂)",
        "DepartureZh": "警廣站",
        "DestinationZh": "鼎金國小"
      },
      {
        "ddesc": "鳳山站－圓照寺",
        "seq": 230,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/23/map/745/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 23,
        "NameZh": "23(部分延駛文山高中)",
        "DepartureZh": "高雄客運鳳山站",
        "DestinationZh": "圓照寺"
      },
      {
        "ddesc": "鳳山站－圓照寺",
        "seq": 230,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/231/map/721/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 231,
        "NameZh": "23公車式小黃",
        "DepartureZh": "高雄客運鳳山站",
        "DestinationZh": "圓照寺"
      },
      {
        "ddesc": "圓照寺－捷運巨蛋站",
        "seq": 240,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/24/map/718/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 24,
        "NameZh": "24(原 24A)",
        "DepartureZh": "圓照寺",
        "DestinationZh": "捷運巨蛋站"
      },
      {
        "ddesc": "楠梓站－歷史博物館",
        "seq": 241,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/224/map/701/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 224,
        "NameZh": "224(原 24B)",
        "DepartureZh": "楠梓站",
        "DestinationZh": "歷史博物館"
      },
      {
        "ddesc": "瑞豐站－歷史博物館",
        "seq": 250,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/25/map/365/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 25,
        "NameZh": "25",
        "DepartureZh": "瑞豐站",
        "DestinationZh": "歷史博物館"
      },
      {
        "ddesc": "瑞豐站－高雄火車站",
        "seq": 260,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/26/map/778/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 26,
        "NameZh": "26A",
        "DepartureZh": "瑞豐站",
        "DestinationZh": "高雄火車站"
      },
      {
        "ddesc": "瑞豐站－高雄火車站",
        "seq": 260,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/260/map/776/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 260,
        "NameZh": "26B",
        "DepartureZh": "瑞豐站",
        "DestinationZh": "高雄火車站"
      },
      {
        "ddesc": "加昌站－高雄火車站",
        "seq": 280,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/28/map/771/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 28,
        "NameZh": "28",
        "DepartureZh": "加昌站",
        "DestinationZh": "高雄火車站"
      },
      {
        "ddesc": "左營南站－楠梓高中",
        "seq": 290,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/29/map/374/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 29,
        "NameZh": "29A",
        "DepartureZh": "左營南站",
        "DestinationZh": "楠梓高中"
      },
      {
        "ddesc": "左營南站－楠梓高中",
        "seq": 290,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/292/map/376/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 292,
        "NameZh": "29C",
        "DepartureZh": "左營南站",
        "DestinationZh": "楠梓高中"
      },
      {
        "ddesc": "左營南站－楠梓高中",
        "seq": 291,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/291/map/375/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 291,
        "NameZh": "29B(延駛創新路)",
        "DepartureZh": "左營南站",
        "DestinationZh": "楠梓高中"
      },
      {
        "ddesc": "小港站－長庚醫院",
        "seq": 300,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/30/map/858/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 30,
        "NameZh": "30",
        "DepartureZh": "小港站",
        "DestinationZh": "長庚醫院"
      },
      {
        "ddesc": "金獅湖－捷運鹽埕埔站",
        "seq": 330,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/331/map/593/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 331,
        "NameZh": "33公車式小黃",
        "DepartureZh": "金獅湖站",
        "DestinationZh": "捷運鹽埕埔站"
      },
      {
        "ddesc": "金獅湖－捷運鹽埕埔站",
        "seq": 330,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/33/map/382/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 33,
        "NameZh": "33(06:33延駛後備指揮部)",
        "DepartureZh": "金獅湖站",
        "DestinationZh": "捷運鹽埕埔站"
      },
      {
        "ddesc": "前鎮站－旗津海水浴場",
        "seq": 350,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/35/map/729/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 35,
        "NameZh": "35A(原 紅9D(延駛小港國高中))",
        "DepartureZh": "前鎮站",
        "DestinationZh": "旗津海水浴場"
      },
      {
        "ddesc": "小港站－旗津海水浴場",
        "seq": 351,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/351/map/730/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 351,
        "NameZh": "35B",
        "DepartureZh": "小港站",
        "DestinationZh": "旗津海水浴場"
      },
      {
        "ddesc": "前鎮站－高雄火車站",
        "seq": 360,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/36/map/803/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 36,
        "NameZh": "36",
        "DepartureZh": "前鎮站",
        "DestinationZh": "高雄火車站"
      },
      {
        "ddesc": "前鎮站－育英醫專",
        "seq": 370,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/37/map/855/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 37,
        "NameZh": "37",
        "DepartureZh": "前鎮站",
        "DestinationZh": "育英醫專"
      },
      {
        "ddesc": "育英醫專－前鎮站",
        "seq": 371,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/371/map/683/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 371,
        "NameZh": "37(育英發車)",
        "DepartureZh": "",
        "DestinationZh": ""
      },
      {
        "ddesc": "左營南站－高雄榮民總醫院",
        "seq": 380,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/38/map/391/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 38,
        "NameZh": "38A",
        "DepartureZh": "左營南站",
        "DestinationZh": "高雄榮民總醫院"
      },
      {
        "ddesc": "左營南站－高雄榮民總醫院",
        "seq": 381,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/381/map/392/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 381,
        "NameZh": "38B(去程延駛鼓山三路)",
        "DepartureZh": "左營南站",
        "DestinationZh": "高雄榮民總醫院"
      },
      {
        "ddesc": "左營南站－高雄榮民總醫院",
        "seq": 382,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/382/map/393/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 382,
        "NameZh": "38C(返程延駛鼓山三路)",
        "DepartureZh": "左營南站",
        "DestinationZh": "高雄榮民總醫院"
      },
      {
        "ddesc": "左營南站－高雄榮民總醫院",
        "seq": 383,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/383/map/394/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 383,
        "NameZh": "38D(去返程延駛鼓山三路)",
        "DepartureZh": "左營南站",
        "DestinationZh": "高雄榮民總醫院"
      },
      {
        "ddesc": "加昌站－高雄榮民總醫院",
        "seq": 390,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/39/map/698/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 39,
        "NameZh": "39",
        "DepartureZh": "加昌站",
        "DestinationZh": "高雄榮民總醫院"
      },
      {
        "ddesc": "捷運衛武營站－鼓山輪渡站",
        "seq": 501,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/50/map/878/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 50,
        "NameZh": "50五福幹線",
        "DepartureZh": "捷運衛武營站",
        "DestinationZh": "鼓山輪渡站"
      },
      {
        "ddesc": "建軍站－高雄火車站",
        "seq": 520,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/52/map/992/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 52,
        "NameZh": "52A",
        "DepartureZh": "建軍站",
        "DestinationZh": "高雄火車站"
      },
      {
        "ddesc": "建軍站－高雄火車站",
        "seq": 521,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/521/map/993/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 521,
        "NameZh": "52B(不延駛市議會)",
        "DepartureZh": "建軍站",
        "DestinationZh": "高雄火車站"
      },
      {
        "ddesc": "高雄火車站－高雄女中",
        "seq": 522,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/523/map/405/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 523,
        "NameZh": "52C(捷運中央公園站－高雄女中)",
        "DepartureZh": "捷運中央公園",
        "DestinationZh": "捷運中央公園站"
      },
      {
        "ddesc": "捷運衛武營站－本館里",
        "seq": 530,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/531/map/754/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 531,
        "NameZh": "53A",
        "DepartureZh": "捷運衛武營站",
        "DestinationZh": "本館里"
      },
      {
        "ddesc": "捷運衛武營站－高雄火車站",
        "seq": 531,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/532/map/755/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 532,
        "NameZh": "53B",
        "DepartureZh": "捷運衛武營站",
        "DestinationZh": "高雄火車站"
      },
      {
        "ddesc": "高雄火車站－壽山動物園",
        "seq": 560,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/56/map/874/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 56,
        "NameZh": "56",
        "DepartureZh": "高雄火車站",
        "DestinationZh": "壽山動物園"
      },
      {
        "ddesc": "捷運鹽埕埔站－壽山動物園",
        "seq": 561,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/561/map/875/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 561,
        "NameZh": "56區間(例假日行駛)",
        "DepartureZh": "捷運鹽埕埔站",
        "DestinationZh": "壽山動物園"
      },
      {
        "ddesc": "駁二藝術特區－夢裡活動中心",
        "seq": 600,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/60/map/793/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 60,
        "NameZh": "60覺民幹線(白天延駛澄清湖)",
        "DepartureZh": "駁二藝術特區",
        "DestinationZh": "夢裡活動中心"
      },
      {
        "ddesc": "小港站－大坪頂",
        "seq": 621,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/629/map/722/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 629,
        "NameZh": "62公車式小黃",
        "DepartureZh": "小港站",
        "DestinationZh": "大坪頂"
      },
      {
        "ddesc": "小港站－高坪11路口",
        "seq": 631,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/631/map/994/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 631,
        "NameZh": "63北",
        "DepartureZh": "小港站",
        "DestinationZh": "高松陸橋"
      },
      {
        "ddesc": "小港站－高坪11路口",
        "seq": 632,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/6311/map/959/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 6311,
        "NameZh": "63南",
        "DepartureZh": "小港站",
        "DestinationZh": "九龍寺"
      },
      {
        "ddesc": "小港站－高雄火車站",
        "seq": 690,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/69/map/421/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 69,
        "NameZh": "69A小港幹線",
        "DepartureZh": "小港站",
        "DestinationZh": "高雄火車站"
      },
      {
        "ddesc": "小港站－高雄火車站",
        "seq": 691,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/691/map/422/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 691,
        "NameZh": "69B小港幹線(延駛明鳳)",
        "DepartureZh": "小港站",
        "DestinationZh": "高雄火車站"
      },
      {
        "ddesc": "前鎮站－長庚紀念醫院",
        "seq": 700,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/70/map/424/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 70,
        "NameZh": "70A三多幹線(部分延駛澄清湖)",
        "DepartureZh": "前鎮站",
        "DestinationZh": "長庚紀念醫院"
      },
      {
        "ddesc": "前鎮站－長庚紀念醫院",
        "seq": 701,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/701/map/426/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 701,
        "NameZh": "70B三多幹線(延駛鎮州路)",
        "DepartureZh": "前鎮站",
        "DestinationZh": "長庚紀念醫院"
      },
      {
        "ddesc": "前鎮站－鳥松區公所",
        "seq": 703,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/703/map/428/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 703,
        "NameZh": "70D三多幹線(延駛仁美)",
        "DepartureZh": "前鎮站",
        "DestinationZh": "鳥松區公所"
      },
      {
        "ddesc": "金獅湖站－中正高工",
        "seq": 720,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/72/map/429/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 72,
        "NameZh": "72A",
        "DepartureZh": "金獅湖站",
        "DestinationZh": "中正高工"
      },
      {
        "ddesc": "金獅湖站－正勤社區",
        "seq": 721,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/721/map/430/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 721,
        "NameZh": "72B(延駛正勤社區)",
        "DepartureZh": "金獅湖站",
        "DestinationZh": "正勤社區"
      },
      {
        "ddesc": "捷運衛武營站－左營區公所",
        "seq": 731,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/73/map/847/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 73,
        "NameZh": "73",
        "DepartureZh": "捷運衛武營站",
        "DestinationZh": "左營區公所"
      },
      {
        "ddesc": "金獅湖站－歷史博物館",
        "seq": 760,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/76/map/433/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 76,
        "NameZh": "76",
        "DepartureZh": "金獅湖站",
        "DestinationZh": "歷史博物館"
      },
      {
        "ddesc": "金獅湖站－歷史博物館",
        "seq": 770,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/77/map/434/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 77,
        "NameZh": "77昌福幹線",
        "DepartureZh": "金獅湖站",
        "DestinationZh": "歷史博物館"
      },
      {
        "ddesc": "金獅湖站－高雄女中",
        "seq": 771,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/771/map/435/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 771,
        "NameZh": "77昌福幹線(繞三民高中)",
        "DepartureZh": "金獅湖站",
        "DestinationZh": "高雄女中"
      },
      {
        "ddesc": "瑞豐站－育英醫專",
        "seq": 810,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/81/map/684/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 81,
        "NameZh": "81",
        "DepartureZh": "瑞豐站",
        "DestinationZh": "育英醫專"
      },
      {
        "ddesc": "瑞豐站－三山國王廟",
        "seq": 820,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/82/map/486/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 82,
        "NameZh": "82A",
        "DepartureZh": "瑞豐站",
        "DestinationZh": "三山國王廟"
      },
      {
        "ddesc": "瑞豐站－捷運西子灣站",
        "seq": 821,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/823/map/487/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 823,
        "NameZh": "82B",
        "DepartureZh": "瑞豐站",
        "DestinationZh": "捷運西子灣站"
      },
      {
        "ddesc": "瑞豐站－三山國王廟",
        "seq": 822,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/824/map/699/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 824,
        "NameZh": "82A公車式小黃",
        "DepartureZh": "瑞豐站",
        "DestinationZh": "三山國王廟"
      },
      {
        "ddesc": "瑞豐站－高雄火車站",
        "seq": 830,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/83/map/980/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 83,
        "NameZh": "83",
        "DepartureZh": "瑞豐站",
        "DestinationZh": "高雄火車站"
      },
      {
        "ddesc": "鳳山轉運站－捷運衛武營站",
        "seq": 870,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/87/map/660/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 87,
        "NameZh": "87",
        "DepartureZh": "鳳山轉運站",
        "DestinationZh": "捷運衛武營站"
      },
      {
        "ddesc": "鳳山轉運站－捷運鹽埕埔站",
        "seq": 880,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/122/map/311/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 122,
        "NameZh": "88建國幹線",
        "DepartureZh": "黃埔公園",
        "DestinationZh": "捷運鹽埕埔站"
      },
      {
        "ddesc": "捷運衛武營站－捷運鹽埕埔站",
        "seq": 881,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/1221/map/312/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 1221,
        "NameZh": "88建國幹線(延駛市議會)",
        "DepartureZh": "捷運衛武營站",
        "DestinationZh": "捷運鹽埕埔站"
      },
      {
        "ddesc": "臺鐵鳳山站－捷運鹽埕埔站",
        "seq": 882,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/882/map/738/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 882,
        "NameZh": "88建國幹線(區間)",
        "DepartureZh": "臺鐵鳳山站",
        "DestinationZh": "捷運鹽埕埔站"
      },
      {
        "ddesc": "高鐵左營站－捷運三多商圈站",
        "seq": 900,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/90/map/548/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 90,
        "NameZh": "90民族幹線",
        "DepartureZh": "高鐵左營站",
        "DestinationZh": "捷運三多商圈站"
      },
      {
        "ddesc": "高鐵左營站－高雄火車站(同愛街口)",
        "seq": 920,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/92/map/915/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 92,
        "NameZh": "92自由幹線",
        "DepartureZh": "高鐵左營站",
        "DestinationZh": "高雄火車站(同愛街口)"
      },
      {
        "ddesc": "義大世界站－義大醫院",
        "seq": 960,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/96/map/766/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 96,
        "NameZh": "96A",
        "DepartureZh": "義大世界站",
        "DestinationZh": "義大醫院"
      },
      {
        "ddesc": "仁林路口(澄觀路二段)－義大醫院",
        "seq": 961,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/961/map/930/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 961,
        "NameZh": "96B",
        "DepartureZh": "義大醫院",
        "DestinationZh": "林路口(澄觀路二段)"
      },
      {
        "ddesc": "捷運都會公園站－樹德科大",
        "seq": 970,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/97/map/868/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 97,
        "NameZh": "97",
        "DepartureZh": "捷運都會公園站",
        "DestinationZh": "樹德科大"
      },
      {
        "ddesc": "捷運青埔站－捷運楠梓加工區",
        "seq": 981,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/98/map/943/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 98,
        "NameZh": "98",
        "DepartureZh": "捷運青埔站",
        "DestinationZh": "捷運楠梓加工區"
      },
      {
        "ddesc": "香蕉棚(棧二庫棧二之一庫)－慈德堂",
        "seq": 990,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/99/map/984/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 99,
        "NameZh": "99",
        "DepartureZh": "香蕉棚(棧二庫棧二之一庫)",
        "DestinationZh": "慈德堂"
      },
      {
        "ddesc": "哈瑪星旅運接駁中心－西子灣",
        "seq": 991,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/991/map/907/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 991,
        "NameZh": "99區間",
        "DepartureZh": "哈瑪星旅運接駁中心",
        "DestinationZh": "捷運西子灣站"
      },
      {
        "ddesc": "瑞豐站－高雄火車站",
        "seq": 1000,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/100/map/898/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 100,
        "NameZh": "100百貨幹線",
        "DepartureZh": "瑞豐站",
        "DestinationZh": "高雄火車站"
      },
      {
        "ddesc": "金獅湖站－金獅湖站",
        "seq": 1680,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2061/map/912/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 2061,
        "NameZh": "168環西幹線",
        "DepartureZh": "金獅湖站",
        "DestinationZh": "輕軌夢時代站"
      },
      {
        "ddesc": "金獅湖站－金獅湖站",
        "seq": 1681,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2071/map/913/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 2071,
        "NameZh": "168環東幹線",
        "DepartureZh": "金獅湖站",
        "DestinationZh": "輕軌夢時代站"
      },
      {
        "ddesc": "加昌站－輕軌夢時代站",
        "seq": 2050,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/114/map/844/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 114,
        "NameZh": "205中華幹線",
        "DepartureZh": "加昌站",
        "DestinationZh": "輕軌夢時代站"
      },
      {
        "ddesc": "加昌站－高雄火車站",
        "seq": 2051,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/115/map/659/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 115,
        "NameZh": "205中華幹線區間車",
        "DepartureZh": "加昌站",
        "DestinationZh": "高雄火車站"
      },
      {
        "ddesc": "小港站－歷史博物館",
        "seq": 2141,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/214/map/341/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 214,
        "NameZh": "214A",
        "DepartureZh": "小港站",
        "DestinationZh": "歷史博物館"
      },
      {
        "ddesc": "前鎮站－捷運鹽埕埔站",
        "seq": 2142,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/215/map/342/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 215,
        "NameZh": "214B(06:10前鎮站至鹽埕埔)",
        "DepartureZh": "前鎮站",
        "DestinationZh": "捷運鹽埕埔站"
      },
      {
        "ddesc": "加昌站－鳥松區公所",
        "seq": 2170,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/217/map/736/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 217,
        "NameZh": "217A新昌幹線",
        "DepartureZh": "加昌站",
        "DestinationZh": "鳥松區公所"
      },
      {
        "ddesc": "高雄應用科技大學->加昌站",
        "seq": 2173,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2173/map/862/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 2173,
        "NameZh": "217D新昌幹線(高應大發車)",
        "DepartureZh": "高雄科大(建工)",
        "DestinationZh": "加昌站"
      },
      {
        "ddesc": "大華村->加昌站",
        "seq": 2174,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2174/map/863/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 2174,
        "NameZh": "217E新昌幹線(大華村發車)",
        "DepartureZh": "大華村",
        "DestinationZh": "加昌站"
      },
      {
        "ddesc": "加昌站－高雄火車站",
        "seq": 2180,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/218/map/348/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 218,
        "NameZh": "218A",
        "DepartureZh": "加昌站",
        "DestinationZh": "高雄火車站"
      },
      {
        "ddesc": "加昌站－高雄火車站",
        "seq": 2181,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2181/map/349/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 2181,
        "NameZh": "218B(延駛合群社區)",
        "DepartureZh": "加昌站",
        "DestinationZh": "高雄火車站"
      },
      {
        "ddesc": "加昌站－捷運鹽埕埔站",
        "seq": 2190,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/219/map/350/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 219,
        "NameZh": "219A",
        "DepartureZh": "加昌站",
        "DestinationZh": "大智路(光榮國小)"
      },
      {
        "ddesc": "加昌站－捷運鹽埕埔站",
        "seq": 2191,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2192/map/351/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 2192,
        "NameZh": "219B",
        "DepartureZh": "捷運西子灣站",
        "DestinationZh": "大智路(光榮國小)"
      },
      {
        "ddesc": "茄萣站－台南火車站",
        "seq": 2390,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/239/map/846/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 239,
        "NameZh": "239",
        "DepartureZh": "茄萣站",
        "DestinationZh": "台南火車站"
      },
      {
        "ddesc": "加昌站－臺鐵新左營站",
        "seq": 2450,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/245/map/941/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 245,
        "NameZh": "245A",
        "DepartureZh": "加昌站",
        "DestinationZh": "臺鐵新左營站"
      },
      {
        "ddesc": "加昌站－臺鐵新左營站",
        "seq": 2451,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2451/map/942/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 2451,
        "NameZh": "245B(延駛高雄大學)",
        "DepartureZh": "加昌站",
        "DestinationZh": "臺鐵新左營站"
      },
      {
        "ddesc": "捷運衛武營站－鼓山輪渡站",
        "seq": 2480,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/248/map/787/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 248,
        "NameZh": "248",
        "DepartureZh": "捷運衛武營站",
        "DestinationZh": "濱海二路"
      },
      {
        "ddesc": "高雄火車站－鼓山輪渡站",
        "seq": 2481,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2481/map/788/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 2481,
        "NameZh": "248火車站",
        "DepartureZh": "高雄火車站",
        "DestinationZh": "濱海二路"
      },
      {
        "ddesc": "高雄火車站－鼓山輪渡站",
        "seq": 2483,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2482/map/1002/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 2482,
        "NameZh": "248區間車",
        "DepartureZh": "高雄火車站",
        "DestinationZh": "濱海二路"
      },
      {
        "ddesc": "義大醫院站－旗山轉運站",
        "seq": 2610,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/261/map/767/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 261,
        "NameZh": "261",
        "DepartureZh": "義大醫院站",
        "DestinationZh": "旗山轉運站"
      },
      {
        "ddesc": "加昌站－高雄火車站",
        "seq": 3010,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/301/map/772/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 301,
        "NameZh": "301",
        "DepartureZh": "加昌站",
        "DestinationZh": "高雄火車站"
      },
      {
        "ddesc": "台鐵新左營站－高雄火車站",
        "seq": 3011,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/3011/map/773/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 3011,
        "NameZh": "301區間",
        "DepartureZh": "台鐵新左營站",
        "DestinationZh": "高雄火車站"
      },
      {
        "ddesc": "歷史博物館－林園站",
        "seq": 8010,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8001/map/879/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8001,
        "NameZh": "8001",
        "DepartureZh": "歷史博物館",
        "DestinationZh": "林園站"
      },
      {
        "ddesc": "高雄客運鳳山站－大樹",
        "seq": 8060,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8006/map/925/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8006,
        "NameZh": "8006",
        "DepartureZh": "捷運鳳山站",
        "DestinationZh": "大樹"
      },
      {
        "ddesc": "岡山轉運站－高雄車站",
        "seq": 8080,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8008/map/880/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8008,
        "NameZh": "8008",
        "DepartureZh": "岡山轉運站",
        "DestinationZh": "高雄車站"
      },
      {
        "ddesc": "旗山北站－高雄車站(經澄清湖)",
        "seq": 8090,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8009/map/691/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8009,
        "NameZh": "8009",
        "DepartureZh": "旗山北站",
        "DestinationZh": "高雄車站"
      },
      {
        "ddesc": "高雄車站－旗山北站",
        "seq": 8100,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8010/map/882/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8010,
        "NameZh": "8010",
        "DepartureZh": "高雄車站",
        "DestinationZh": "旗山北站"
      },
      {
        "ddesc": "鳳山轉運站－旗山北站",
        "seq": 8101,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8062/map/883/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8062,
        "NameZh": "8010(區間車)",
        "DepartureZh": "鳳山轉運站",
        "DestinationZh": "旗山北站"
      },
      {
        "ddesc": "捷運南岡山站－旗山北站",
        "seq": 8120,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8012/map/601/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8012,
        "NameZh": "8012",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "旗山北站"
      },
      {
        "ddesc": "捷運南岡山站－田寮",
        "seq": 8130,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8013/map/976/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8013,
        "NameZh": "8013(假日延駛石頭廟)",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "田寮"
      },
      {
        "ddesc": "岡山轉運站(岡山火車站)－海青工商",
        "seq": 8150,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8015/map/669/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8015,
        "NameZh": "8015",
        "DepartureZh": "岡山轉運站(岡山火車站)",
        "DestinationZh": "海青工商"
      },
      {
        "ddesc": "台鐵新左營站－岡山轉運站",
        "seq": 8170,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8017/map/818/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8017,
        "NameZh": "8017",
        "DepartureZh": "台鐵新左營站",
        "DestinationZh": "岡山轉運站"
      },
      {
        "ddesc": "義大醫院－捷運南岡山站",
        "seq": 8200,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8020/map/885/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8020,
        "NameZh": "8020",
        "DepartureZh": "義大醫院",
        "DestinationZh": "捷運南岡山站"
      },
      {
        "ddesc": "高雄客運鳳山站－彌陀國小",
        "seq": 8210,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8021/map/886/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8021,
        "NameZh": "8021",
        "DepartureZh": "高雄客運鳳山站",
        "DestinationZh": "彌陀國小"
      },
      {
        "ddesc": "高雄車站－旗山轉運站",
        "seq": 8230,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8023/map/794/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8023,
        "NameZh": "8023",
        "DepartureZh": "高雄車站",
        "DestinationZh": "旗山北站"
      },
      {
        "ddesc": "楠梓站－旗山轉運站",
        "seq": 8231,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8063/map/795/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8063,
        "NameZh": "8023(區間車)",
        "DepartureZh": "楠梓站",
        "DestinationZh": "旗山北站"
      },
      {
        "ddesc": "旗山轉運站－木梓",
        "seq": 8260,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8026/map/777/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8026,
        "NameZh": "8026",
        "DepartureZh": "旗山轉運站",
        "DestinationZh": "玄龍宮"
      },
      {
        "ddesc": "甲仙－桃源",
        "seq": 8290,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8029/map/796/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8029,
        "NameZh": "8029",
        "DepartureZh": "甲仙站",
        "DestinationZh": "桃源"
      },
      {
        "ddesc": "六龜新站－不老溫泉",
        "seq": 8330,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8033/map/779/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8033,
        "NameZh": "8033",
        "DepartureZh": "六龜新站",
        "DestinationZh": "美崙山"
      },
      {
        "ddesc": "旗山轉運站－內門",
        "seq": 8350,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8074/map/887/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8074,
        "NameZh": "8035(區間車)",
        "DepartureZh": "旗山轉運站",
        "DestinationZh": "內門"
      },
      {
        "ddesc": "旗山轉運站－南化",
        "seq": 8351,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8035/map/888/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8035,
        "NameZh": "8035(部分延駛實踐大學)",
        "DepartureZh": "旗山轉運站",
        "DestinationZh": "南化"
      },
      {
        "ddesc": "旗山轉運站－竹峰寺",
        "seq": 8360,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8036/map/889/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8036,
        "NameZh": "8036",
        "DepartureZh": "旗山轉運站",
        "DestinationZh": "竹峰寺"
      },
      {
        "ddesc": "岡山轉運站－高雄車站",
        "seq": 8400,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8040/map/977/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8040,
        "NameZh": "8040",
        "DepartureZh": "岡山轉運站",
        "DestinationZh": "高雄車站"
      },
      {
        "ddesc": "林園站－高客自立站",
        "seq": 8410,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8069/map/918/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8069,
        "NameZh": "8041A",
        "DepartureZh": "林園站",
        "DestinationZh": "高客自立站"
      },
      {
        "ddesc": "捷運鳳山站－岡山轉運站",
        "seq": 8411,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8070/map/978/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8070,
        "NameZh": "8041B",
        "DepartureZh": "捷運鳳山站",
        "DestinationZh": "岡山轉運站"
      },
      {
        "ddesc": "鳳山站－茄萣站",
        "seq": 8412,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8041/map/969/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8041,
        "NameZh": "8041C(部分延駛樹人醫專)",
        "DepartureZh": "捷運鳳山站",
        "DestinationZh": "茄萣站"
      },
      {
        "ddesc": "實踐大學－高鐵台南站",
        "seq": 8420,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8042/map/932/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 8042,
        "NameZh": "8042(原 E07 假日延駛博物館、航空站)",
        "DepartureZh": "實踐大學",
        "DestinationZh": "高鐵台南站"
      },
      {
        "ddesc": "茄萣站－高雄車站",
        "seq": 8430,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8043/map/894/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8043,
        "NameZh": "8043",
        "DepartureZh": "茄萣站",
        "DestinationZh": "高雄車站"
      },
      {
        "ddesc": "高雄車站－台南火車站",
        "seq": 8460,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8046/map/970/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8046,
        "NameZh": "8046A",
        "DepartureZh": "高雄車站",
        "DestinationZh": "台南火車站"
      },
      {
        "ddesc": "高鐵左營站－台南火車站",
        "seq": 8461,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8073/map/971/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8073,
        "NameZh": "8046B(部分延駛樹人醫專)",
        "DepartureZh": "高鐵左營站",
        "DestinationZh": "台南火車站"
      },
      {
        "ddesc": "捷運都會公園站－屏東火車站",
        "seq": 8480,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8048/map/916/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8048,
        "NameZh": "8048",
        "DepartureZh": "捷運都會公園站",
        "DestinationZh": "屏東火車站"
      },
      {
        "ddesc": "崗安路－鳳山",
        "seq": 8490,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8049/map/979/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8049,
        "NameZh": "8049",
        "DepartureZh": "崗安路",
        "DestinationZh": "高雄客運鳳山站"
      },
      {
        "ddesc": "崗安路－捷運南岡山站",
        "seq": 8491,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8075/map/897/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8075,
        "NameZh": "8049(區間車)",
        "DepartureZh": "崗安路",
        "DestinationZh": "捷運南岡山站"
      },
      {
        "ddesc": "高鐵左營站－義大世界",
        "seq": 8510,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8501/map/811/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 8501,
        "NameZh": "8501(部分班次延駛義守大學)",
        "DepartureZh": "高鐵左營站",
        "DestinationZh": "義大世界站"
      },
      {
        "ddesc": "高鐵左營站－佛陀紀念館",
        "seq": 8511,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8507/map/812/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8507,
        "NameZh": "8501延駛佛光山",
        "DepartureZh": "高鐵左營站",
        "DestinationZh": "佛陀紀念館"
      },
      {
        "ddesc": "高雄美術館－義大世界",
        "seq": 8520,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8502/map/810/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 8502,
        "NameZh": "8502A",
        "DepartureZh": "高雄美術館",
        "DestinationZh": "義大世界站"
      },
      {
        "ddesc": "高雄市政大樓－義大世界",
        "seq": 8530,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8503/map/620/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 8503,
        "NameZh": "8503",
        "DepartureZh": "市政大樓",
        "DestinationZh": "義大世界站"
      },
      {
        "ddesc": "鳳陽社區－義大世界",
        "seq": 8540,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8504/map/741/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 8504,
        "NameZh": "8504",
        "DepartureZh": "鳳陽社區",
        "DestinationZh": "義大世界站"
      },
      {
        "ddesc": "前鎮高中站－義大世界",
        "seq": 8550,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8505/map/873/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 8505,
        "NameZh": "8505",
        "DepartureZh": "前鎮高中站",
        "DestinationZh": "義大世界"
      },
      {
        "ddesc": "岡山轉運站－義大世界",
        "seq": 8560,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8506/map/809/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 8506,
        "NameZh": "8506",
        "DepartureZh": "岡山轉運站",
        "DestinationZh": "義大世界"
      },
      {
        "ddesc": "敬老院－樹德科技大學",
        "seq": 10000,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/4511/map/1017/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 4511,
        "NameZh": "H51",
        "DepartureZh": "敬老院",
        "DestinationZh": "樹德科技大學"
      },
      {
        "ddesc": "大學西路口－捷運橋頭火車站",
        "seq": 10000,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9511/map/1014/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 9511,
        "NameZh": "95B",
        "DepartureZh": "大學西路口",
        "DestinationZh": "捷運橋頭火車站"
      },
      {
        "ddesc": "捷運橋頭火車站－高雄科大",
        "seq": 10000,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9512/map/1015/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 9512,
        "NameZh": "95C",
        "DepartureZh": "捷運橋頭火車站",
        "DestinationZh": "高雄科大"
      },
      {
        "ddesc": "大學西路口－新庄",
        "seq": 10000,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9513/map/1016/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 9513,
        "NameZh": "95D",
        "DepartureZh": "大學西路口",
        "DestinationZh": "新庄"
      },
      {
        "ddesc": "大學西路口－義大醫院",
        "seq": 10000,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9510/map/1013/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 9510,
        "NameZh": "95A",
        "DepartureZh": "大學西路口",
        "DestinationZh": "義大醫院"
      },
      {
        "ddesc": "高雄餐旅大學－捷運小港站",
        "seq": 20010,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/801/map/440/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 801,
        "NameZh": "紅1",
        "DepartureZh": "高雄餐旅大學",
        "DestinationZh": "捷運小港站"
      },
      {
        "ddesc": "高雄餐旅大學－中鋼",
        "seq": 20011,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8100/map/479/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8100,
        "NameZh": "紅1(延駛中鋼公司)",
        "DepartureZh": "高雄餐旅大學",
        "DestinationZh": "中鋼"
      },
      {
        "ddesc": "高雄餐旅大學(校區)－捷運小港站",
        "seq": 20012,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8200/map/1020/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8200,
        "NameZh": "紅1(區間)",
        "DepartureZh": "高雄餐旅大學(校區)",
        "DestinationZh": "捷運小港站"
      },
      {
        "ddesc": "小港站－鳳鼻頭港",
        "seq": 20020,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/179/map/656/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 179,
        "NameZh": "紅2A",
        "DepartureZh": "小港站",
        "DestinationZh": "鳳鼻頭港"
      },
      {
        "ddesc": "小港站－鳳鼻頭港",
        "seq": 20021,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/1791/map/657/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 1791,
        "NameZh": "紅2B(延駛職訓中心)",
        "DepartureZh": "小港站",
        "DestinationZh": "鳳鼻頭港"
      },
      {
        "ddesc": "小港站－鳳鼻頭港",
        "seq": 20022,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/1792/map/658/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 1792,
        "NameZh": "紅2C(直達車)",
        "DepartureZh": "小港站",
        "DestinationZh": "鳳鼻頭港"
      },
      {
        "ddesc": "小港站－林園區公所",
        "seq": 20030,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/211/map/335/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 211,
        "NameZh": "紅3A林園幹線(行經鳳鳴國小)",
        "DepartureZh": "小港站",
        "DestinationZh": "林園區公所"
      },
      {
        "ddesc": "小港站－林園區公所",
        "seq": 20031,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2111/map/336/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 2111,
        "NameZh": "紅3B林園幹線",
        "DepartureZh": "小港站",
        "DestinationZh": "林園區公所"
      },
      {
        "ddesc": "小港站－林園區公所",
        "seq": 20032,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2112/map/337/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 2112,
        "NameZh": "紅3C林園幹線(不經西溪里)",
        "DepartureZh": "小港站",
        "DestinationZh": "林園區公所"
      },
      {
        "ddesc": "小港站－林園區公所",
        "seq": 20033,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2113/map/900/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 2113,
        "NameZh": "紅3D林園幹線(不經西溪里.宏平路)",
        "DepartureZh": "小港站",
        "DestinationZh": "林園區公所"
      },
      {
        "ddesc": "小港站－林園區公所",
        "seq": 20034,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2114/map/339/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 2114,
        "NameZh": "紅3E林園幹線(延駛鳳鳴里)",
        "DepartureZh": "小港站",
        "DestinationZh": "林園區公所"
      },
      {
        "ddesc": "小港站－林園區公所",
        "seq": 20035,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2115/map/586/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 2115,
        "NameZh": "紅3公車式小黃(西溪里)",
        "DepartureZh": "新光人壽",
        "DestinationZh": "林園區公所"
      },
      {
        "ddesc": "捷運小港站－中智路郵局",
        "seq": 20050,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/213/map/831/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 213,
        "NameZh": "紅5 ",
        "DepartureZh": "捷運小港站",
        "DestinationZh": "中智路郵局"
      },
      {
        "ddesc": "捷運草衙站－小港站",
        "seq": 20060,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/806/map/466/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 806,
        "NameZh": "紅6",
        "DepartureZh": "捷運草衙站",
        "DestinationZh": "小港站"
      },
      {
        "ddesc": "捷運草衙站－小港站",
        "seq": 20060,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8061/map/588/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8061,
        "NameZh": "紅6公車式小黃",
        "DepartureZh": "捷運草衙站",
        "DestinationZh": "小港站"
      },
      {
        "ddesc": "后安路－孔宅(不經紅毛港)",
        "seq": 20070,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/807/map/470/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 807,
        "NameZh": "紅7A(不經紅毛港)",
        "DepartureZh": "后安路",
        "DestinationZh": "孔宅"
      },
      {
        "ddesc": "后安路－孔宅(經紅毛港)",
        "seq": 20071,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8071/map/472/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 8071,
        "NameZh": "紅7B(經紅毛港)",
        "DepartureZh": "后安路",
        "DestinationZh": "孔宅"
      },
      {
        "ddesc": "后安路－孔宅(延駛漁業署)",
        "seq": 20072,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8072/map/473/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8072,
        "NameZh": "紅7C(延駛漁業署)",
        "DepartureZh": "后安路",
        "DestinationZh": "孔宅"
      },
      {
        "ddesc": "小港站－輔英科大校區",
        "seq": 20080,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/362/map/384/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 362,
        "NameZh": "紅8A",
        "DepartureZh": "小港站",
        "DestinationZh": "輔英科大校區"
      },
      {
        "ddesc": "小港站－輔英科大校區",
        "seq": 20081,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/3621/map/385/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 3621,
        "NameZh": "紅8B(去繞昭明)",
        "DepartureZh": "小港站",
        "DestinationZh": "輔英科大校區"
      },
      {
        "ddesc": "小港站－輔英科大校區",
        "seq": 20082,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/3623/map/387/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 3623,
        "NameZh": "紅8C(返繞昭明)",
        "DepartureZh": "小港站",
        "DestinationZh": "輔英科大校區"
      },
      {
        "ddesc": "小港站－昭明變電所站",
        "seq": 20084,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/3624/map/388/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 3624,
        "NameZh": "紅8E",
        "DepartureZh": "小港站",
        "DestinationZh": "昭明變電所站"
      },
      {
        "ddesc": "前鎮站－旗津輪渡站",
        "seq": 20090,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/116/map/304/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 116,
        "NameZh": "紅9A旗津幹線",
        "DepartureZh": "前鎮站",
        "DestinationZh": "旗津輪渡站"
      },
      {
        "ddesc": "旗津海水浴場－旗津海岸公園",
        "seq": 20092,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/1161/map/305/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 1161,
        "NameZh": "紅9B假日觀光公車(延駛旗津漁港)",
        "DepartureZh": "旗津海水浴場",
        "DestinationZh": "旗津海岸公園"
      },
      {
        "ddesc": "旗津海水浴場－旗津海岸公園",
        "seq": 20093,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/1162/map/306/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 1162,
        "NameZh": "紅9C假日觀光公車",
        "DepartureZh": "旗津海水浴場",
        "DestinationZh": "旗津海岸公園"
      },
      {
        "ddesc": "前鎮高中－臺鐵鳳山站",
        "seq": 20100,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/810/map/964/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 810,
        "NameZh": "紅10",
        "DepartureZh": "前鎮高中",
        "DestinationZh": "臺鐵鳳山站"
      },
      {
        "ddesc": "中崙四路－前鎮高中",
        "seq": 20100,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8103/map/1019/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 8103,
        "NameZh": "紅10(區間車)",
        "DepartureZh": "前鎮高中站",
        "DestinationZh": ""
      },
      {
        "ddesc": "過埤派出所－捷運前鎮高中站",
        "seq": 20110,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/811/map/780/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 811,
        "NameZh": "紅11A",
        "DepartureZh": "過埤派出所",
        "DestinationZh": "捷運前鎮高中站"
      },
      {
        "ddesc": "過埤派出所－捷運前鎮高中站",
        "seq": 20110,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8111/map/991/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 8111,
        "NameZh": "紅11B",
        "DepartureZh": "過埤派出所",
        "DestinationZh": "捷運前鎮高中站"
      },
      {
        "ddesc": "過埤派出所－捷運前鎮高中站",
        "seq": 20112,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8112/map/908/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8112,
        "NameZh": "紅11公車式小黃",
        "DepartureZh": "過埤派出所",
        "DestinationZh": "捷運前鎮高中站"
      },
      {
        "ddesc": "公車瑞豐站－公車前鎮站",
        "seq": 20120,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/812/map/482/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 812,
        "NameZh": "紅12A",
        "DepartureZh": "公車瑞豐站",
        "DestinationZh": "公車前鎮站"
      },
      {
        "ddesc": "公車瑞豐站－高雄加工出口區",
        "seq": 20121,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8121/map/990/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 8121,
        "NameZh": "紅12B(延駛加工出口區)",
        "DepartureZh": "公車瑞豐站",
        "DestinationZh": "公車前鎮站"
      },
      {
        "ddesc": "捷運小港站－紅毛港文化園區",
        "seq": 20130,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/813/map/1009/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 813,
        "NameZh": "紅13(紅毛港文化園區)",
        "DepartureZh": "捷運小港站",
        "DestinationZh": "紅毛港文化園區"
      },
      {
        "ddesc": "軟體園區－捷運三多商圈站",
        "seq": 20160,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8161/map/723/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8161,
        "NameZh": "紅16公車式小黃",
        "DepartureZh": "軟體園區",
        "DestinationZh": "捷運三多商圈站"
      },
      {
        "ddesc": "軟體園區－捷運三多商圈站",
        "seq": 20160,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/816/map/670/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 816,
        "NameZh": "紅16",
        "DepartureZh": "軟體園區",
        "DestinationZh": "捷運三多商圈站"
      },
      {
        "ddesc": "中崙社區－實踐大學",
        "seq": 20180,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/818/map/937/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 818,
        "NameZh": "紅18",
        "DepartureZh": "中崙社區",
        "DestinationZh": "實踐大學"
      },
      {
        "ddesc": "捷運衛武營站－捷運三多商圈站",
        "seq": 20210,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/821/map/719/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 821,
        "NameZh": "紅21",
        "DepartureZh": "捷運衛武營站",
        "DestinationZh": "捷運三多商圈站"
      },
      {
        "ddesc": "捷運衛武營站－新光路口",
        "seq": 20211,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/822/map/710/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 822,
        "NameZh": "紅22(原 紅21區)",
        "DepartureZh": "捷運衛武營站",
        "DestinationZh": "圖書總館"
      },
      {
        "ddesc": "左營南站－高雄火車站(同愛街口)",
        "seq": 20250,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/825/map/924/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 825,
        "NameZh": "紅25",
        "DepartureZh": "左營南站",
        "DestinationZh": "高雄火車站(同愛街口)"
      },
      {
        "ddesc": "左營南站－高雄火車站(同愛街口)",
        "seq": 20251,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8251/map/1001/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 8251,
        "NameZh": "紅25繞",
        "DepartureZh": "先勝路口",
        "DestinationZh": "高雄火車站"
      },
      {
        "ddesc": "高雄火車站－中都",
        "seq": 20271,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/827/map/606/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 827,
        "NameZh": "紅27",
        "DepartureZh": "高雄火車站",
        "DestinationZh": "中都街口"
      },
      {
        "ddesc": "客家文物館－汾陽路口",
        "seq": 20280,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/828/map/832/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 828,
        "NameZh": "紅28",
        "DepartureZh": "客家文物館",
        "DestinationZh": "汾陽路口"
      },
      {
        "ddesc": "客家文物館－汾陽路口",
        "seq": 20280,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8282/map/724/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8282,
        "NameZh": "紅28公車式小黃",
        "DepartureZh": "客家文物館",
        "DestinationZh": "汾陽路口"
      },
      {
        "ddesc": "中都濕地公園－汾陽路口",
        "seq": 20281,
        "routeMapImageUrl": "",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8283,
        "NameZh": "紅28公車式小黃(繞中都濕地公園)",
        "DepartureZh": "察哈爾街口",
        "DestinationZh": "汾陽路口"
      },
      {
        "ddesc": "中都濕地公園－汾陽路口",
        "seq": 20281,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8281/map/833/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8281,
        "NameZh": "紅28(繞中都濕地公園)",
        "DepartureZh": "察哈爾街口",
        "DestinationZh": "汾陽路口"
      },
      {
        "ddesc": "捷運後驛站－陽明國小",
        "seq": 20290,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/829/map/492/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 829,
        "NameZh": "紅29",
        "DepartureZh": "捷運後驛站",
        "DestinationZh": "陽明國小"
      },
      {
        "ddesc": "澄清湖棒球場－高雄火車站",
        "seq": 20300,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/830/map/985/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 830,
        "NameZh": "紅30",
        "DepartureZh": "忠誠路口",
        "DestinationZh": "高雄火車站"
      },
      {
        "ddesc": "皓東路口－高雄火車站",
        "seq": 20300,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/831/map/988/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 831,
        "NameZh": "紅31",
        "DepartureZh": "皓東路口",
        "DestinationZh": "高雄火車站"
      },
      {
        "ddesc": "高雄科大(建工)-捷運後驛站",
        "seq": 20301,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8301/map/989/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 8301,
        "NameZh": "紅30區",
        "DepartureZh": "高雄科大(建工)",
        "DestinationZh": "捷運後驛站"
      },
      {
        "ddesc": "捷運凹仔底站－大榮高中",
        "seq": 20320,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/832/map/496/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 832,
        "NameZh": "紅32A",
        "DepartureZh": "捷運凹子底站",
        "DestinationZh": "國泰市場"
      },
      {
        "ddesc": "捷運凹仔底站－鼓山高中",
        "seq": 20321,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8321/map/497/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8321,
        "NameZh": "紅32B(學校寒暑假停駛)",
        "DepartureZh": "捷運凹子底站",
        "DestinationZh": "鼓山高中"
      },
      {
        "ddesc": "市立聯合醫院－鳳山商工",
        "seq": 20330,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/833/map/968/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 833,
        "NameZh": "紅33明誠幹線",
        "DepartureZh": "市立聯合醫院",
        "DestinationZh": "鳳山商工"
      },
      {
        "ddesc": "金獅湖站－捷運凹子底站",
        "seq": 20350,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/210/map/834/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 210,
        "NameZh": "紅35A",
        "DepartureZh": "金獅湖站",
        "DestinationZh": "捷運凹子底站"
      },
      {
        "ddesc": "金獅湖站－捷運凹子底站",
        "seq": 20351,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2101/map/835/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 2101,
        "NameZh": "紅35B",
        "DepartureZh": "金獅湖站",
        "DestinationZh": "捷運凹子底站"
      },
      {
        "ddesc": "金獅湖站－捷運凹子底站",
        "seq": 20352,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2102/map/836/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 2102,
        "NameZh": "紅35A(繞崇實社區)",
        "DepartureZh": "金獅湖站",
        "DestinationZh": "捷運凹子底站"
      },
      {
        "ddesc": "左營海軍軍區－文藻外語",
        "seq": 20360,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/836/map/499/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 836,
        "NameZh": "紅36",
        "DepartureZh": "左營海軍軍區",
        "DestinationZh": "文藻外語大學"
      },
      {
        "ddesc": "左營海軍軍區－文藻外語",
        "seq": 20361,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8361/map/500/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 8361,
        "NameZh": "紅36(17:08－23:30繞文信路)",
        "DepartureZh": "左營海軍軍區",
        "DestinationZh": "文藻外語大學"
      },
      {
        "ddesc": "捷運生態園區－榮總側門",
        "seq": 20500,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8509/map/510/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 8509,
        "NameZh": "紅50公車式小黃",
        "DepartureZh": "捷運生態園區",
        "DestinationZh": "榮總側門"
      },
      {
        "ddesc": "捷運生態園區－榮總側門",
        "seq": 20500,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/850/map/837/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 850,
        "NameZh": "紅50",
        "DepartureZh": "捷運生態園區",
        "DestinationZh": "榮總側門"
      },
      {
        "ddesc": "臺鐵新左營站－蓮池潭",
        "seq": 20510,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8513/map/594/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8513,
        "NameZh": "紅51A公車式小黃",
        "DepartureZh": "台鐵新左營站",
        "DestinationZh": "蓮池潭"
      },
      {
        "ddesc": "臺鐵新左營站－蓮池潭",
        "seq": 20510,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/851/map/595/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 851,
        "NameZh": "紅51A",
        "DepartureZh": "台鐵新左營站",
        "DestinationZh": "蓮池潭"
      },
      {
        "ddesc": "臺鐵新左營站－捷運生態園區站",
        "seq": 20511,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8515/map/725/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8515,
        "NameZh": "紅51B公車式小黃",
        "DepartureZh": "台鐵新左營站",
        "DestinationZh": "捷運生態園區站"
      },
      {
        "ddesc": "臺鐵新左營站－捷運生態園區站",
        "seq": 20511,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8511/map/512/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8511,
        "NameZh": "紅51B",
        "DepartureZh": "台鐵新左營站",
        "DestinationZh": "捷運生態園區站"
      },
      {
        "ddesc": "臺鐵新左營站－高雄物產館",
        "seq": 20512,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8514/map/726/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8514,
        "NameZh": "紅51C公車式小黃",
        "DepartureZh": "台鐵新左營站",
        "DestinationZh": "高雄物產館"
      },
      {
        "ddesc": "臺鐵新左營站－中山大學",
        "seq": 20520,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/852/map/928/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 852,
        "NameZh": "紅52(原 E05)",
        "DepartureZh": "台鐵新左營站",
        "DestinationZh": "中山大學"
      },
      {
        "ddesc": "臺鐵新左營站－蚵仔寮漁港",
        "seq": 20531,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8531/map/921/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 8531,
        "NameZh": "紅53梓官幹線-主",
        "DepartureZh": "台鐵新左營站",
        "DestinationZh": "蚵仔寮漁港"
      },
      {
        "ddesc": "臺鐵新左營站－蚵仔寮漁港",
        "seq": 20534,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8533/map/922/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8533,
        "NameZh": "紅53梓官幹線-副",
        "DepartureZh": "台鐵新左營站",
        "DestinationZh": "蚵仔寮漁港"
      },
      {
        "ddesc": "臺鐵新左營站－援中國小",
        "seq": 20550,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8532/map/972/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 8532,
        "NameZh": "紅53梓官幹線-區間(原 紅55)",
        "DepartureZh": "台鐵新左營站",
        "DestinationZh": "中興里(楠梓)"
      },
      {
        "ddesc": "捷運楠梓加工區－高雄大學",
        "seq": 20560,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/856/map/840/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 856,
        "NameZh": "紅56A",
        "DepartureZh": "捷運楠梓加工區",
        "DestinationZh": "高雄大學"
      },
      {
        "ddesc": "捷運楠梓加工區－高雄大學",
        "seq": 20561,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8561/map/841/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 8561,
        "NameZh": "紅56B(延駛德民黃昏市場)",
        "DepartureZh": "捷運楠梓加工區",
        "DestinationZh": "高雄大學"
      },
      {
        "ddesc": "捷運楠梓加工區－高雄大學",
        "seq": 20562,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8562/map/842/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8562,
        "NameZh": "紅56C(學校寒暑假停駛)",
        "DepartureZh": "捷運楠梓加工區",
        "DestinationZh": "高雄大學"
      },
      {
        "ddesc": "東六街口－創意南路",
        "seq": 20570,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/857/map/621/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 857,
        "NameZh": "紅57",
        "DepartureZh": "東六街口",
        "DestinationZh": "創意南路"
      },
      {
        "ddesc": "捷運楠梓加工區－捷運楠梓加工區",
        "seq": 20571,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8571/map/622/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8571,
        "NameZh": "紅57(區間車)",
        "DepartureZh": "捷運楠梓加工區",
        "DestinationZh": "員工服務中心"
      },
      {
        "ddesc": "捷運都會公園站－高雄第一科大(西校區)",
        "seq": 20580,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/858/map/524/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 858,
        "NameZh": "紅58A",
        "DepartureZh": "捷運都會公園站",
        "DestinationZh": "高雄科大(第一西校區)"
      },
      {
        "ddesc": "捷運都會公園站－燕巢區公所",
        "seq": 20581,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8581/map/525/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 8581,
        "NameZh": "紅58B",
        "DepartureZh": "捷運都會公園站",
        "DestinationZh": "燕巢區公所"
      },
      {
        "ddesc": "捷運都會公園站－楠梓高中",
        "seq": 20582,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8582/map/526/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8582,
        "NameZh": "紅58C(學校寒暑假停駛)",
        "DepartureZh": "捷運都會公園站",
        "DestinationZh": "楠梓高中"
      },
      {
        "ddesc": "高鐵左營站－蓮池潭",
        "seq": 20590,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/859/map/1010/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 859,
        "NameZh": "紅59",
        "DepartureZh": "高鐵左營站",
        "DestinationZh": "蓮池潭"
      },
      {
        "ddesc": "左營高鐵站－仁武區公所",
        "seq": 20600,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/860/map/791/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 860,
        "NameZh": "紅60A",
        "DepartureZh": "左營高鐵站",
        "DestinationZh": "仁德街口(中華路)"
      },
      {
        "ddesc": "左營高鐵站－加昌站",
        "seq": 20601,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8601/map/792/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 8601,
        "NameZh": "紅60B",
        "DepartureZh": "左營高鐵站",
        "DestinationZh": "加昌站"
      },
      {
        "ddesc": "漢翔公司－捷運南岡山站",
        "seq": 20680,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/868/map/530/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 868,
        "NameZh": "紅68A",
        "DepartureZh": "漢翔公司",
        "DestinationZh": "捷運南岡山站"
      },
      {
        "ddesc": "大莊里－捷運南岡山站",
        "seq": 20681,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8682/map/532/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8682,
        "NameZh": "紅68A(延駛大莊里)",
        "DepartureZh": "大莊里",
        "DestinationZh": "捷運南岡山站"
      },
      {
        "ddesc": "捷運南岡山站－漢翔公司",
        "seq": 20682,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8681/map/531/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8681,
        "NameZh": "紅68B(部分班次延駛嘉興國中)",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "漢翔公司"
      },
      {
        "ddesc": "捷運南岡山站－燁聯鋼鐵",
        "seq": 20683,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/865/map/705/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 865,
        "NameZh": "紅65(原 紅68C(週一~週五延駛燁聯鋼鐵))",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "燁聯鋼鐵"
      },
      {
        "ddesc": "捷運南岡山站－岡山高中",
        "seq": 20690,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/869/map/717/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 869,
        "NameZh": "紅69 (原 紅69A)",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "岡山高中"
      },
      {
        "ddesc": "捷運南岡山站－岡山高中",
        "seq": 20690,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8695/map/727/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8695,
        "NameZh": "紅69A公車式小黃",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "岡山高中"
      },
      {
        "ddesc": "捷運南岡山站－高雄科學園區 ",
        "seq": 20691,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/867/map/708/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 867,
        "NameZh": "紅67A部分班次延駛中正堂(原 紅69B)",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "高雄科學園區"
      },
      {
        "ddesc": "捷運南岡山站－永安工業區",
        "seq": 20692,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/866/map/707/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 866,
        "NameZh": "紅66(原 紅69C(延駛本洲工業區))",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "永安工業區"
      },
      {
        "ddesc": "捷運南岡山站－順安宮",
        "seq": 20693,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8671/map/709/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8671,
        "NameZh": "紅67B(原 紅69D)",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "順安宮"
      },
      {
        "ddesc": "捷運南岡山站－田寮區公所",
        "seq": 20700,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/253/map/369/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 253,
        "NameZh": "紅70A",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "田寮區公所"
      },
      {
        "ddesc": "捷運南岡山站－隆后宮",
        "seq": 20701,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/2533/map/370/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 2533,
        "NameZh": "紅70B(延駛隆后宮)",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "隆后宮"
      },
      {
        "ddesc": "捷運南岡山站－茄萣區公所",
        "seq": 20710,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/871/map/995/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 871,
        "NameZh": "紅71A",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "茄萣區公所"
      },
      {
        "ddesc": "捷運南岡山站－茄萣區公所",
        "seq": 20711,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8711/map/996/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8711,
        "NameZh": "紅71B1",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "茄萣區公所"
      },
      {
        "ddesc": "捷運南岡山站－茄萣區公所",
        "seq": 20712,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8712/map/541/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8712,
        "NameZh": "紅71B2",
        "DepartureZh": "興達國小",
        "DestinationZh": "茄萣區公所"
      },
      {
        "ddesc": "加昌站－捷運南岡山站",
        "seq": 20713,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8713/map/1011/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8713,
        "NameZh": "紅71C",
        "DepartureZh": "加昌站",
        "DestinationZh": "捷運南岡山站"
      },
      {
        "ddesc": "捷運南岡山站－東方設計學院",
        "seq": 20714,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8714/map/998/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8714,
        "NameZh": "紅71D",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "東方設計學院"
      },
      {
        "ddesc": "捷運橋頭火車站－彌陀國小",
        "seq": 20720,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/872/map/761/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 872,
        "NameZh": "紅72A(部份班次延駛安樂宮)",
        "DepartureZh": "捷運橋頭火車站",
        "DestinationZh": "彌陀國小"
      },
      {
        "ddesc": "捷運橋頭火車站－彌陀國小",
        "seq": 20721,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8723/map/757/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8723,
        "NameZh": "紅72A(延駛南安國小)",
        "DepartureZh": "捷運橋頭火車站",
        "DestinationZh": "彌陀國小"
      },
      {
        "ddesc": "捷運橋頭火車站－永安區公所",
        "seq": 20722,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8721/map/758/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 8721,
        "NameZh": "紅72B(部分班次延駛保寧)",
        "DepartureZh": "捷運橋頭火車站",
        "DestinationZh": "永安區公所"
      },
      {
        "ddesc": "捷運橋頭火車站－彌陀國小",
        "seq": 20723,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8722/map/759/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8722,
        "NameZh": "紅72C(部份班次延駛安樂宮)",
        "DepartureZh": "捷運橋頭火車站",
        "DestinationZh": "彌陀國小"
      },
      {
        "ddesc": "捷運橋頭火車站－彌陀國小",
        "seq": 20724,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8724/map/583/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8724,
        "NameZh": "紅72C(延駛南安國小)",
        "DepartureZh": "捷運橋頭火車站",
        "DestinationZh": "彌陀國小"
      },
      {
        "ddesc": "岡山轉運站－阿蓮區公所",
        "seq": 20730,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/221/map/999/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 221,
        "NameZh": "紅73A",
        "DepartureZh": "岡山轉運站",
        "DestinationZh": "阿蓮區公所"
      },
      {
        "ddesc": "岡山轉運站－阿蓮區公所",
        "seq": 20731,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8732/map/1000/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8732,
        "NameZh": "紅73B",
        "DepartureZh": "岡山轉運站",
        "DestinationZh": "阿蓮區公所"
      },
      {
        "ddesc": "捷運南岡山站－南寮",
        "seq": 20780,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/878/map/663/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 878,
        "NameZh": "紅78",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "南寮"
      },
      {
        "ddesc": "捷運南岡山站－塭仔邊",
        "seq": 20790,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/879/map/662/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 879,
        "NameZh": "紅79",
        "DepartureZh": "捷運南岡山站",
        "DestinationZh": "塭仔邊"
      },
      {
        "ddesc": "高鐵左營站－旗山轉運站",
        "seq": 40010,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/604/map/413/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 604,
        "NameZh": "E01A旗美國道快線(往旗山轉運站)",
        "DepartureZh": "高鐵左營站",
        "DestinationZh": "旗山轉運站"
      },
      {
        "ddesc": "高鐵左營站－美濃站",
        "seq": 40011,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/603/map/412/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 603,
        "NameZh": "E01B旗美國道快線(往美濃站)",
        "DepartureZh": "高鐵左營站",
        "DestinationZh": "美濃站"
      },
      {
        "ddesc": "高鐵左營站－佛光山",
        "seq": 40020,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/609/map/414/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 609,
        "NameZh": "E02哈佛快線",
        "DepartureZh": "高鐵左營站",
        "DestinationZh": "佛光山"
      },
      {
        "ddesc": "高鐵左營站－義大醫院",
        "seq": 40030,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/503/map/926/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 503,
        "NameZh": "E03A燕巢快線",
        "DepartureZh": "高鐵左營站",
        "DestinationZh": "義大醫院"
      },
      {
        "ddesc": "高鐵左營站－義大醫院",
        "seq": 40031,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/5031/map/927/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 5031,
        "NameZh": "E03B燕巢快線",
        "DepartureZh": "高鐵左營站",
        "DestinationZh": "義大醫院"
      },
      {
        "ddesc": "高鐵左營站－高師大燕巢校區",
        "seq": 40040,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/504/map/1003/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 504,
        "NameZh": "E04",
        "DepartureZh": "高鐵左營站",
        "DestinationZh": "高師大燕巢校區"
      },
      {
        "ddesc": "樹德科技大學－高師大燕巢校區",
        "seq": 40041,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/5041/map/1004/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 5041,
        "NameZh": "E04區間",
        "DepartureZh": "樹德科技大學",
        "DestinationZh": "高師大燕巢校區"
      },
      {
        "ddesc": "國軍高雄總醫院－樹德科技大學",
        "seq": 40090,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/509/map/1006/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 509,
        "NameZh": "E09鳳山燕巢城市快線",
        "DepartureZh": "國軍高雄總醫院",
        "DestinationZh": "樹德科技大學"
      },
      {
        "ddesc": "捷運草衙站－樹德科技大學",
        "seq": 40100,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/510/map/1008/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 510,
        "NameZh": "E10小港燕巢城市快線",
        "DepartureZh": "捷運草衙站",
        "DestinationZh": "樹德科技大學"
      },
      {
        "ddesc": "捷運大東站－高鐵新左營站",
        "seq": 40102,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/511/map/700/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 511,
        "NameZh": "E11A鳳山高鐵城市快線",
        "DepartureZh": "捷運大東站",
        "DestinationZh": "高鐵新左營站"
      },
      {
        "ddesc": "中崙國中－高鐵新左營站",
        "seq": 40110,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/5112/map/849/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 5112,
        "NameZh": "E11B鳳山高鐵城市快線",
        "DepartureZh": "中崙國中",
        "DestinationZh": "高鐵新左營站"
      },
      {
        "ddesc": "六龜新站 － 高雄車站",
        "seq": 40250,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8025/map/781/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8025,
        "NameZh": "E25高旗六龜快線(08:20前不行經高鐵左營站)",
        "DepartureZh": "六龜新站",
        "DestinationZh": "高雄車站"
      },
      {
        "ddesc": "高雄車站－美濃站",
        "seq": 40280,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8028/map/783/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8028,
        "NameZh": "E28高旗美濃快線",
        "DepartureZh": "高雄車站",
        "DestinationZh": "美濃站"
      },
      {
        "ddesc": "甲仙站－高雄車站",
        "seq": 40320,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/8032/map/784/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 8032,
        "NameZh": "E32高旗甲仙快線",
        "DepartureZh": "甲仙站",
        "DestinationZh": "高雄車站"
      },
      {
        "ddesc": "桃源區－旗山南站－義大醫院－長庚醫院",
        "seq": 50110,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/411/map/769/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 411,
        "NameZh": "JOY公車H11",
        "DepartureZh": "桃源區公所",
        "DestinationZh": "長庚醫院"
      },
      {
        "ddesc": "寶來－義大醫院－長庚醫院",
        "seq": 50111,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/1111/map/297/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 1111,
        "NameZh": "JOY公車H11A",
        "DepartureZh": "寶來7-11",
        "DestinationZh": "長庚醫院"
      },
      {
        "ddesc": "六龜－桃源",
        "seq": 50113,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/133/map/797/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 133,
        "NameZh": "JOY公車H11區間(六龜-桃源)",
        "DepartureZh": "六龜新站",
        "DestinationZh": "桃源"
      },
      {
        "ddesc": "六龜－寶來",
        "seq": 50114,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/132/map/798/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 132,
        "NameZh": "JOY公車H11區間(六龜-寶來)",
        "DepartureZh": "六龜新站",
        "DestinationZh": "寶來"
      },
      {
        "ddesc": "旗山轉運站－新開不老溫泉",
        "seq": 50115,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/4134/map/799/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 4134,
        "NameZh": "JOY公車H11區間(旗山轉運站-新開不老溫泉)",
        "DepartureZh": "旗山轉運站",
        "DestinationZh": "美崙山"
      },
      {
        "ddesc": "桃源區－旗山南站－榮民總醫院－高雄醫學院",
        "seq": 50120,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/112/map/800/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 112,
        "NameZh": "JOY公車H12",
        "DepartureZh": "桃源區公所",
        "DestinationZh": "高醫"
      },
      {
        "ddesc": "五里埔(小林國小)－高醫",
        "seq": 50210,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/85/map/947/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 85,
        "NameZh": "JOY公車H21",
        "DepartureZh": "五里埔(小林國小)",
        "DestinationZh": "高醫"
      },
      {
        "ddesc": "旗山轉運站－溪洲",
        "seq": 50211,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/1123/map/301/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 1123,
        "NameZh": "JOY公車H21區間",
        "DepartureZh": "旗山轉運站",
        "DestinationZh": "溪洲"
      },
      {
        "ddesc": "甲仙站 － 旗山轉運站",
        "seq": 50212,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/1122/map/300/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 1122,
        "NameZh": "JOY公車H21區間(甲仙站-旗山)",
        "DepartureZh": "甲仙站",
        "DestinationZh": "旗山轉運站"
      },
      {
        "ddesc": "那瑪夏區公所－甲仙站",
        "seq": 50213,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/1121/map/299/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 1121,
        "NameZh": "JOY公車H21區間(那瑪夏-甲仙站)",
        "DepartureZh": "那瑪夏區公所",
        "DestinationZh": "甲仙站"
      },
      {
        "ddesc": "那瑪夏區－旗山南站－義大醫院－長庚醫院",
        "seq": 50220,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/86/map/527/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 86,
        "NameZh": "JOY公車H22",
        "DepartureZh": "那瑪夏",
        "DestinationZh": "長庚醫院"
      },
      {
        "ddesc": "多納－旗山轉運站",
        "seq": 50310,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/131/map/975/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 131,
        "NameZh": "JOY公車H31",
        "DepartureZh": "多納",
        "DestinationZh": "旗山轉運站"
      },
      {
        "ddesc": "臺鐵鳳山站－佛陀紀念館",
        "seq": 56820,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/682/map/419/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 682,
        "NameZh": "大樹祈福線A",
        "DepartureZh": "臺鐵鳳山站",
        "DestinationZh": "佛陀紀念館"
      },
      {
        "ddesc": "鳳山轉運站－佛陀紀念館",
        "seq": 56821,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/6821/map/420/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 6821,
        "NameZh": "大樹祈福線B",
        "DepartureZh": "鳳山轉運站",
        "DestinationZh": "佛陀紀念館"
      },
      {
        "ddesc": "澄清湖棒球場－捷運三多商圈",
        "seq": 60010,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/601/map/765/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 601,
        "NameZh": "黃1(平日行駛)",
        "DepartureZh": "澄清湖棒球場",
        "DestinationZh": "捷運三多商圈"
      },
      {
        "ddesc": "澄清湖棒球場－高雄展覽館",
        "seq": 60011,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/6011/map/954/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 6011,
        "NameZh": "黃1延(假日行駛)",
        "DepartureZh": "澄清湖棒球場",
        "DestinationZh": "高雄展覽館"
      },
      {
        "ddesc": "前鎮高中－忠誠路口",
        "seq": 60020,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/602/map/674/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 602,
        "NameZh": "黃2A",
        "DepartureZh": "前鎮高中",
        "DestinationZh": "忠誠路口"
      },
      {
        "ddesc": "小港站－忠誠路口－前鎮高中",
        "seq": 60021,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/6021/map/697/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 6021,
        "NameZh": "黃2B",
        "DepartureZh": "小港站",
        "DestinationZh": "忠誠路口"
      },
      {
        "ddesc": "前鎮高中－忠誠路口－小港站",
        "seq": 60022,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/6022/map/676/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 6022,
        "NameZh": "黃2C",
        "DepartureZh": "前鎮高中",
        "DestinationZh": "忠誠路口"
      },
      {
        "ddesc": "香蕉碼頭－正勤社區",
        "seq": 70001,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/7002/map/966/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 7002,
        "NameZh": "綠1區間",
        "DepartureZh": "香蕉碼頭(棧貳庫)",
        "DestinationZh": "正勤社區"
      },
      {
        "ddesc": "香蕉碼頭－輕軌凱旋中華站",
        "seq": 70001,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/7001/map/965/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 7001,
        "NameZh": "綠1",
        "DepartureZh": "香蕉碼頭(棧貳庫)",
        "DestinationZh": "輕軌凱旋中華站"
      },
      {
        "ddesc": "捷運西子灣2號出口－中山大學行政大樓",
        "seq": 90010,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/901/map/825/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 901,
        "NameZh": "橘1A",
        "DepartureZh": "捷運西子灣2號出口",
        "DestinationZh": "中山大學行政大樓"
      },
      {
        "ddesc": "捷運西子灣2號出口－西子灣",
        "seq": 90011,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9011/map/826/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 9011,
        "NameZh": "橘1B(平日停駛)",
        "DepartureZh": "捷運西子灣2號出口",
        "DestinationZh": "中山大學"
      },
      {
        "ddesc": "捷運西子灣2號出口－中山大學文學院",
        "seq": 90012,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9012/map/827/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 9012,
        "NameZh": "橘1C(延駛中山大學文學院)",
        "DepartureZh": "捷運西子灣2號出口",
        "DestinationZh": "中山大學文學院"
      },
      {
        "ddesc": "捷運衛武營站－舊鐵橋濕地",
        "seq": 90070,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/1601/map/820/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 1601,
        "NameZh": "橘7A(不延駛)",
        "DepartureZh": "捷運衛武營站",
        "DestinationZh": "舊鐵橋濕地"
      },
      {
        "ddesc": "大樹區公所->捷運衛武營站",
        "seq": 90071,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/1602/map/957/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 1602,
        "NameZh": "橘7A(去程放空)",
        "DepartureZh": "",
        "DestinationZh": ""
      },
      {
        "ddesc": "捷運衛武營站－大樹衛生所",
        "seq": 90072,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/160/map/821/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 160,
        "NameZh": "橘7A(延駛大樹衛生所)",
        "DepartureZh": "捷運衛武營站",
        "DestinationZh": "大樹衛生所"
      },
      {
        "ddesc": "捷運衛武營站－大樹衛生所",
        "seq": 90073,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9702/map/822/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 9702,
        "NameZh": "橘7B(不延駛佛陀紀念館)",
        "DepartureZh": "捷運衛武營站",
        "DestinationZh": "大樹衛生所"
      },
      {
        "ddesc": "捷運衛武營站－佛陀紀念館",
        "seq": 90074,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9701/map/823/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 9701,
        "NameZh": "橘7B(假日停駛)",
        "DepartureZh": "捷運衛武營站",
        "DestinationZh": "佛陀紀念館"
      },
      {
        "ddesc": "捷運衛武營站－過埤派出所",
        "seq": 90080,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/908/map/981/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 908,
        "NameZh": "橘8",
        "DepartureZh": "捷運衛武營站",
        "DestinationZh": "過埤派出所"
      },
      {
        "ddesc": "捷運衛武營站－鳳山轉運站",
        "seq": 90100,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/910/map/903/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 910,
        "NameZh": "橘10A(假日停駛)",
        "DepartureZh": "捷運衛武營站",
        "DestinationZh": "臺鐵鳳山站"
      },
      {
        "ddesc": "捷運衛武營站－鳳山轉運站",
        "seq": 90101,
        "routeMapImageUrl": "",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 9103,
        "NameZh": "橘10B公車式小黃",
        "DepartureZh": "捷運衛武營站",
        "DestinationZh": "臺鐵鳳山站"
      },
      {
        "ddesc": "林園站－捷運衛武營站",
        "seq": 90110,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/911/map/864/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 911,
        "NameZh": "橘11A",
        "DepartureZh": "林園站",
        "DestinationZh": "捷運衛武營站"
      },
      {
        "ddesc": "林園站－臺鐵鳳山站",
        "seq": 90110,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9112/map/664/image",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 9112,
        "NameZh": "橘11A(區間車)",
        "DepartureZh": "林園站",
        "DestinationZh": "臺鐵鳳山站"
      },
      {
        "ddesc": "林園站－捷運衛武營站",
        "seq": 90111,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9111/map/865/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 9111,
        "NameZh": "橘11B",
        "DepartureZh": "林園站",
        "DestinationZh": "捷運衛武營站"
      },
      {
        "ddesc": "中崙社區－長庚紀念醫院",
        "seq": 90120,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/912/map/808/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 912,
        "NameZh": "橘12鳳青幹線(白天延駛澄清湖)",
        "DepartureZh": "中崙國中",
        "DestinationZh": "長庚醫院"
      },
      {
        "ddesc": "捷運鳳山西站－鳳新高中",
        "seq": 90121,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9121/map/557/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 9121,
        "NameZh": "橘12鳳青幹線區間車(寒暑假停駛)",
        "DepartureZh": "捷運鳳山西站",
        "DestinationZh": "鳳新高中 "
      },
      {
        "ddesc": "鳳山轉運站－仁武區公所",
        "seq": 90160,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/916/map/752/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 916,
        "NameZh": "橘16",
        "DepartureZh": "臺鐵鳳山站",
        "DestinationZh": "仁武高中"
      },
      {
        "ddesc": "鳳山轉運站－大社區公所",
        "seq": 90161,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9161/map/753/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 9161,
        "NameZh": "橘16(延駛大社)",
        "DepartureZh": "臺鐵鳳山站",
        "DestinationZh": "大社區公所"
      },
      {
        "ddesc": "鳳山轉運站－長庚醫院",
        "seq": 90170,
        "routeMapImageUrl": "",
        "TimescheduleUrl": "",
        "weight": "0",
        "op_type": "0",
        "Id": 9171,
        "NameZh": "橘17(延駛)",
        "DepartureZh": "鳳山轉運站",
        "DestinationZh": "長庚醫院"
      },
      {
        "ddesc": "鳳山轉運站－長庚醫院",
        "seq": 90170,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/917/map/712/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 917,
        "NameZh": "橘17(原 橘7C)",
        "DepartureZh": "鳳山轉運站",
        "DestinationZh": "長庚醫院"
      },
      {
        "ddesc": "捷運大寮站－大寮農會",
        "seq": 90200,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/920/map/561/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 920,
        "NameZh": "橘20A",
        "DepartureZh": "捷運大寮站",
        "DestinationZh": "大寮農會"
      },
      {
        "ddesc": "捷運大寮站－上寮里",
        "seq": 90201,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9201/map/562/image",
        "TimescheduleUrl": "",
        "weight": "2.0",
        "op_type": "0",
        "Id": 9201,
        "NameZh": "橘20B",
        "DepartureZh": "捷運大寮站",
        "DestinationZh": "上寮路口(大寮路)"
      },
      {
        "ddesc": "捷運大寮站－上寮里",
        "seq": 90202,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9205/map/566/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 9205,
        "NameZh": "橘20B(延駛輔英科大)",
        "DepartureZh": "捷運大寮站",
        "DestinationZh": "上寮路口(大寮路)"
      },
      {
        "ddesc": "捷運大寮站－輔英科大",
        "seq": 90203,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9206/map/806/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 9206,
        "NameZh": "橘20C(直達車)",
        "DepartureZh": "捷運大寮站",
        "DestinationZh": "輔英科大"
      },
      {
        "ddesc": "捷運大寮站－輔英科大",
        "seq": 90203,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9202/map/807/image",
        "TimescheduleUrl": "",
        "weight": "1.0",
        "op_type": "0",
        "Id": 9202,
        "NameZh": "橘20C",
        "DepartureZh": "捷運大寮站",
        "DestinationZh": "輔英科大"
      },
      {
        "ddesc": "捷運大寮站－潮寮國中",
        "seq": 90204,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9203/map/564/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 9203,
        "NameZh": "橘20D(假日停駛)",
        "DepartureZh": "捷運大寮站",
        "DestinationZh": "潮寮國中"
      },
      {
        "ddesc": "和春技術學院(大發校區)－捷運大寮站",
        "seq": 90210,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/921/map/567/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 921,
        "NameZh": "橘21A公車式小黃",
        "DepartureZh": "和春技術學院(大發校區)",
        "DestinationZh": "捷運大寮站"
      },
      {
        "ddesc": "月天寺－捷運大寮站",
        "seq": 90211,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/9211/map/568/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 9211,
        "NameZh": "橘21B公車式小黃",
        "DepartureZh": "月天寺",
        "DestinationZh": "捷運大寮站"
      },
      {
        "ddesc": "江山社區活動中心－捷運大寮站",
        "seq": 90220,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/922/map/569/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 922,
        "NameZh": "橘22公車式小黃",
        "DepartureZh": "後庄(鳳屏一路)",
        "DestinationZh": "捷運大寮站"
      },
      {
        "ddesc": "捷運大寮站－中庄",
        "seq": 90230,
        "routeMapImageUrl": "",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 9231,
        "NameZh": "橘23公車式小黃",
        "DepartureZh": "捷運大寮站",
        "DestinationZh": "中庄"
      }
    ]
  },
  {
    "id": "11",
    "name": "其他",
    "description": "其他",
    "seq": 6,
    "routes": [
      {
        "ddesc": "桃源區－旗山南站－義大醫院－長庚醫院",
        "seq": 50110,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/411/map/769/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 411,
        "NameZh": "JOY公車H11",
        "DepartureZh": "桃源區公所",
        "DestinationZh": "長庚醫院"
      },
      {
        "ddesc": "寶來－義大醫院－長庚醫院",
        "seq": 50111,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/1111/map/297/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 1111,
        "NameZh": "JOY公車H11A",
        "DepartureZh": "寶來7-11",
        "DestinationZh": "長庚醫院"
      },
      {
        "ddesc": "六龜－桃源",
        "seq": 50113,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/133/map/797/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 133,
        "NameZh": "JOY公車H11區間(六龜-桃源)",
        "DepartureZh": "六龜新站",
        "DestinationZh": "桃源"
      },
      {
        "ddesc": "六龜－寶來",
        "seq": 50114,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/132/map/798/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 132,
        "NameZh": "JOY公車H11區間(六龜-寶來)",
        "DepartureZh": "六龜新站",
        "DestinationZh": "寶來"
      },
      {
        "ddesc": "旗山轉運站－新開不老溫泉",
        "seq": 50115,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/4134/map/799/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 4134,
        "NameZh": "JOY公車H11區間(旗山轉運站-新開不老溫泉)",
        "DepartureZh": "旗山轉運站",
        "DestinationZh": "美崙山"
      },
      {
        "ddesc": "桃源區－旗山南站－榮民總醫院－高雄醫學院",
        "seq": 50120,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/112/map/800/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 112,
        "NameZh": "JOY公車H12",
        "DepartureZh": "桃源區公所",
        "DestinationZh": "高醫"
      },
      {
        "ddesc": "五里埔(小林國小)－高醫",
        "seq": 50210,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/85/map/947/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 85,
        "NameZh": "JOY公車H21",
        "DepartureZh": "五里埔(小林國小)",
        "DestinationZh": "高醫"
      },
      {
        "ddesc": "旗山轉運站－溪洲",
        "seq": 50211,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/1123/map/301/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 1123,
        "NameZh": "JOY公車H21區間",
        "DepartureZh": "旗山轉運站",
        "DestinationZh": "溪洲"
      },
      {
        "ddesc": "甲仙站 － 旗山轉運站",
        "seq": 50212,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/1122/map/300/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 1122,
        "NameZh": "JOY公車H21區間(甲仙站-旗山)",
        "DepartureZh": "甲仙站",
        "DestinationZh": "旗山轉運站"
      },
      {
        "ddesc": "那瑪夏區公所－甲仙站",
        "seq": 50213,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/1121/map/299/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 1121,
        "NameZh": "JOY公車H21區間(那瑪夏-甲仙站)",
        "DepartureZh": "那瑪夏區公所",
        "DestinationZh": "甲仙站"
      },
      {
        "ddesc": "那瑪夏區－旗山南站－義大醫院－長庚醫院",
        "seq": 50220,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/86/map/527/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 86,
        "NameZh": "JOY公車H22",
        "DepartureZh": "那瑪夏",
        "DestinationZh": "長庚醫院"
      },
      {
        "ddesc": "多納－旗山轉運站",
        "seq": 50310,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/131/map/975/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 131,
        "NameZh": "JOY公車H31",
        "DepartureZh": "多納",
        "DestinationZh": "旗山轉運站"
      },
      {
        "ddesc": "臺鐵鳳山站－佛陀紀念館",
        "seq": 56820,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/682/map/419/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 682,
        "NameZh": "大樹祈福線A",
        "DepartureZh": "臺鐵鳳山站",
        "DestinationZh": "佛陀紀念館"
      },
      {
        "ddesc": "鳳山轉運站－佛陀紀念館",
        "seq": 56821,
        "routeMapImageUrl": "http://ibus.tbkc.gov.tw/cms/api/route/6821/map/420/image",
        "TimescheduleUrl": "",
        "weight": "3.0",
        "op_type": "0",
        "Id": 6821,
        "NameZh": "大樹祈福線B",
        "DepartureZh": "鳳山轉運站",
        "DestinationZh": "佛陀紀念館"
      }
    ]
  }
]

module.exports = { 
  routeMap,
  routeGroup
};