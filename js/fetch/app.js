const fetch = require('node-fetch');
const gql = require('graphql-tag');
const { fromJS, Map, is } = require('immutable');
const { createHttpLink } = require('apollo-link-http');
// const ApolloClient = require('apollo-client');
const { ApolloClient } = require('apollo-client')
const { InMemoryCache } = require('apollo-cache-inmemory');
const { onError } = require('apollo-link-error');


// const url = 'http://10.40.201.200:8082/api/bt_ttp/paths';
// const url = 'http://10.40.201.200:8082/api/bt_ttp/edges';
// const url = 'https://btmgt-p.freeway.gov.tw/api/bttc-paths';
const url = 'http://localhost:8080/api/bt_ttp/nodes';

// const splitNodes = raw => fromJS(raw)
//   .map((item, idx) => ([item, raw[idx + 1]]))
//   .splice(-1)
//   .toJS();

const splitNodes = raw => fromJS(raw)
  .map((item, idx) => ([item, raw[idx + 1]]))
  // .splice(-1)
  .toJS();

// const splitNodes = raw => {
//   return fromJS(raw).map((item, idx) => {
//     console.log(item,raw,idx);
//     // console.log(([item, raw[idx + 1]]).splice(-1));
//     return ([item, raw[idx + 1]]).splice(-1);
//   }).toJS()
// };

const fetchData = async (url) => {
  const res = await fetch(url, {
    method: 'GET',
    timeout: 30000,
  });
  if (res.status === 200) {
    return res.json();
  }
  return null;
};

const destory_BTTC_Path_API = async(id) => {
  const bttcPathAPI = 'http://10.40.51.24:8080/api/bt_ttp/paths';
  const res = await fetch(bttcPathAPI, {
    method: 'DELETE',
    timeout: 30000,
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ path_id: id }),
  });

  // console.log('status', res.status);
  if (res.status === 200) {
    const raw = await res.json();
    return;
  }
}

const destory_BTTC_EDGE_API = async(id) => {
  const bttcEDGEAPI = 'http://10.40.51.24:8080/api/bt_ttp/edges';
  const res = await fetch(bttcEDGEAPI, {
    method: 'DELETE',
    timeout: 30000,
    headers: { 'Content-Type': 'application/json' },
    body: JSON.stringify({ edge_id: id }),
  });

  console.log('status', res.status);
  if (res.status === 200) {
    const raw = await res.json();
    return;
  }
}

const find = async () => {
  const bttcPathAPI = 'http://10.40.51.24:8080/api/bt_ttp/paths';
  const bttcEDGEAPI = 'http://10.40.51.24:8080/api/bt_ttp/edges';
  const path = await fetchData(bttcPathAPI);
  const edge = await fetchData(bttcEDGEAPI);
  const paths = fromJS(path).map(data => data.get('node_ids')).toJS();
  const edges = fromJS(edge);
  // console.log('path', path);
  // console.log('edge', edge);
  // console.log('paths', fromJS(paths));
  return fromJS(paths).map((ids) => {
    // console.log('ids', ids);
    // console.log('ids', ids.toJS());
    const splitIds = splitNodes(ids.toJS());
    // console.log('splitIds', splitIds);
    const dists = fromJS(splitIds).map((nextIds) => {
      const edge = edges.find((item) => {
        // console.log('item1', item);
        // console.log('item2', item.get('node_ids'), nextIds);
        // console.log('nextIds', nextIds);
        return is(item.get('node_ids'), nextIds);
      });
      // console.log('edge', edge);
      // console.log('dist', edge.get('dist'));
      return edge ? edge.get('dist') : 0;
    });
    return dists;
  }).toJS();
}

const index = async (ctx) => {
  const res = await fetch(url, {
    method: 'GET',
    timeout: 30000,
  });
  const raw = await res.json();
  // console.log('raw',raw);
};

const Run = async () => {
  // const data = await find();
  // console.log(data);
  // await destory_BTTC_Path_API(25);
  // await destory_BTTC_Path_API(26);
  for(let i=24;i<=27;i++){
    await destory_BTTC_Path_API(i);
  }
  for(let i=124;i<=177;i++){
    await destory_BTTC_EDGE_API(i);
  }
};
Run();
