const fetch = require('node-fetch');
const gql = require('graphql-tag');
const { fromJS } = require('immutable');
const { createHttpLink } = require('apollo-link-http');
// const ApolloClient = require('apollo-client');
const { ApolloClient } = require('apollo-client')
const { InMemoryCache } = require('apollo-cache-inmemory');
const { onError } = require('apollo-link-error');


const index = async (ctx) => {
  const res = await fetch('http://10.40.201.200:8082/api/bt_ttp/edges', {
    method: 'GET',
    timeout: 30000,
  });
  const raw = await res.json();
  console.log('raw',raw);
};

const query = gql`
  query Blog($pager: Int, $offset: String!) {
    blogs(first: $pager, after: $offset){
      edges{
        node {
          id
          title
          content
          createdAt
        }
        cursor
      }
      pageInfo {
        hasNextPage
        hasPreviousPage
      }
    }
  }
`;

const farequery = gql`
  query RouteSchedule($xno: Int!){
    route(xno: $xno, lang: "zh"){
      id
      name
      fares {
        ticketType
        fareClass
        price
        }
    }
  }
`;


const routequery = gql`
  query RouteSchedule($xno: Int!,$lang: String!){
    route(xno: $xno, lang: $lang){
      id
      name
      providers{
        id
        name
      }
      fares{
        ticketType
        fareClass
        price
      }
      description
      departure
      destination
    }
  }
`;

// const httpLink = createHttpLink({ uri: 'http://localhost:4000/graphql', fetch });
// const httpLink = createHttpLink({ uri: 'http://192.168.30.30:4000/graphql', fetch });

const errorLink = onError(({ graphQLErrors, networkError }) => {
  // const debug = Debug('graphql:error');
  if (graphQLErrors) {
      graphQLErrors.map(({ message, locations, path }) => console.log(`Message: ${message}, Locations: ${locations}, Path: ${path}`));
  }
  if (networkError) console.log(`[Network error]: ${networkError}`);
});

const gqlLink = new ApolloClient({
  link: errorLink.concat(httpLink),
  cache: new InMemoryCache({ addTypename: false }),
});

const fetchData = async (query, variables) => {
  // const debug = Debug('query:error');
  try {
      const result = await gqlLink.query({ query, variables });
      return result;
  } catch (err) {
      // debug(err.message);
      throw err;
  }
};

const ptx = async () => {
  // const { data } = await fetchData(query, { pager: 100, offset: '' });
  // console.log('data',JSON.stringify(data));
  // const { data } = await fetchData(farequery, { xno: parseInt(3370, 10) });
  const { data } = await fetchData(routequery, { xno: parseInt(3370, 10),lang: 'zh' });
  // console.log('data',JSON.stringify(data));


  const map = fromJS(data.route);
  const routeId = map.get('id');
  const routeName = map.get('name');
  const description = map.get('description');
  const departure = map.get('departure');
  const destination = map.get('destination');
  const providers = map.get('providers');

  console.log(routeId,routeName,description,departure,destination);
  console.log(map.get('fares').map(fare => fare.mapKeys((key) => {
    if (key === 'ticketType') return 'TicketType';
    if (key === 'fareClass') return 'FareClass';
    if (key === 'price') return 'Price';
    return key;
})).toJS());

console.log(map.get('providers').map(provider => provider.mapKeys((key) => {
  if (key === 'id') return 'OperatorID';
  if (key === 'name') return 'OperatorCode';
  return key;
})).toJS());

console.log(map.get('providers').map(provider => {
  // console.log(provider.get("id"));
  return {
    id: provider.get("id"),
    name: provider.get("name")
  }
}).toJS());


};


ptx();
