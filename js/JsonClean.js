const json = {
  'BusStationList': {
    'UpdateTime': '2017-04-05T02:13:06+08:00',
    'UpdateInterval': '86400',
    'AuthorityCode': 'TPE',
    'Stations': {
      'Station': {
        'StationID': '732',
        'StationName': {
          'Zh_tw': '松園',
          'En': 'SongYuan'
        },
        'StationPosition': {
          'PositionLat': '25.15812',
          'PositionLon': '121.56511'
        },
        'RoadName': '…',
        'Bearing': '…',
        'StationAddress': '…',
        'StationDescription': '…'
      }
    }
  }
}

console.log(JSON.stringify(json));

