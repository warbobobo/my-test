const Redis = require('ioredis');
// const redisDSN = "redis://192.168.30.110:6379";
// const redisDSN = "192.168.30.110:6379";
const redisDSN = "localhost:6379";
const client = new Redis(redisDSN, { lazyConnect: true });
client.on('ready', () => {
  console.log('connected');
});
client.on('reconnecting', () => {
  console.log('reconnecting');
});
client.on('error', (err) => {
  console.log(err.message);
});

module.exports = client;
