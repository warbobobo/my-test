const sevenBit = parseInt('01111111', 2);
const fourBit = parseInt('00001111', 2);
const oneBit = parseInt('00000001', 2);

module.exports = {
  oneBit,
  fourBit,
  sevenBit
};
