const unpacker = require('./unpacker');
const { stripBtHeader } = require('./util');
const dispatchUnpacker = (btOrder, data) => {
  switch (btOrder) {
    case 1:
      return unpacker.location(data);
    case 2:
      return { key1: data.slice(5).toString('hex') };
    case 3:
      return { key2: data.slice(5).toString('hex') };
    case 4:
      return unpacker.performance(data);
    case 5:
      return unpacker.travelTime(data);
    case 6:
      return unpacker.message(data);
    default:
      return;
  }
};

const unpack = (buffs) => {
  const result = [];
  let msgs = Buffer.from([]);
  for (let buff of buffs) {
    if (typeof buff === 'string') buff = Buffer.from(buff, 'hex');
    try{
      const { btOrder, data } = stripBtHeader(buff);
      if (btOrder < 6) result.push(dispatchUnpacker(btOrder, data));
      else {
        msgs = Buffer.concat([msgs, data]);
      }
    }catch(e){
      result.push({});
    }
  }
  result.push(dispatchUnpacker(6, msgs));

  return result;
};

const test = (data) => {
  const { hexStr } = JSON.parse(data);
  // console.log('hexStr', hexStr);
  const uniqueHexStr = hexStr.slice(0, 10);
  const unpackData = unpack(uniqueHexStr);
  // console.log('id', unpackData);
  // console.log('id', unpackData[0]);
  // console.log('id', unpackData[0].tcId);
  console.log('id', unpackData[4]);
  // if (hexStr && ip) {
  //   const uniqueHexStr = hexStr.slice(0, 10);
  //   const unpackData = unpack(uniqueHexStr);
  //   const activeBTTC = bttcs.find(bttc => bttc._id === unpackData[0].tcId && bttc.ip === ip);
  //   if (activeBTTC) {
  //     const params = Map(convertParams(unpackData, uniqueHexStr));
  //     if (params.has('bttc') && params.has('direction') && params.has('mileage')) {
  //       const newOne = await BTTCData.create(params.toJS());
  //       return newOne._id;
  //     }
  //   }
  // }
}
// const str = "{\"ts\": 1591952287280, \"hexStr\": [\"0c1606ff011085d41c92180010\", \"1e1606ff022c207729f7c19264cb3eb4015717738a4f6ade58925fb31aad19\", \"0a1606ff03dee4323826ba\", \"161606ff04ff8485999999a99a88999999778819112203\", \"1a1606ff05ff65003386076fb3ff0d576a67970020003500315206\", \"1e1606ff0618fe4600570b003553175411982d57ce516553e358c5585e1cfe\", \"1e1606ff074700570b00355317541100330031002d00320032004b58c5585e\", \"1e1606ff0824fea800301073cd60dc751f547d301191525f8c4e0d958b8eca\", \"0e1606ff095b89516867094fdd969c\", \"041606ff0a\", \"0c1606ff011085d41c92180010\", \"1e1606ff022c207729f7c19264cb3eb4015717738a4f6ade58925fb31aad19\", \"0a1606ff03dee4323826ba\", \"161606ff04ff8485999999a99a88999999778819112203\", \"1a1606ff05ff65003386076fb3ff0d576a67970020003500315206\", \"1e1606ff0618fe4600570b003553175411982d57ce516553e358c5585e1cfe\", \"1e1606ff074700570b00355317541100330031002d00320032004b58c5585e\", \"1e1606ff0824fea800301073cd60dc751f547d301191525f8c4e0d958b8eca\", \"0e1606ff095b89516867094fdd969c\", \"041606ff0a\"]}"
const str =  "{\"ts\": 1591951630284, \"hexStr\": [\"0c1606ff011085d41c92180010\", \"1e1606ff0264de6367ae6fba88ad1a95ca91e8db25b032e1836bb2ffccacc6\", \"0a1606ff0321719e44f905\", \"161606ff04ff8485999989a89a88999999778718221203\", \"1a1606ff05ff65003286076fb3ff0d576a67970020003500305206\", \"1e1606ff061cfe4600570b00355317541100330031002d00320032004b58c5\", \"1e1606ff07585e24fea700301073cd60dc751f547d301191525f8c4e0d958b\", \"101606ff088eca5b89516867094fdd969c\", \"041606ff09\", \"041606ff0a\", \"0c1606ff011085d41c92180010\", \"1e1606ff0264de6367ae6fba88ad1a95ca91e8db25b032e1836bb2ffccacc6\", \"0a1606ff0321719e44f905\", \"161606ff04ff8485999989a89a88999999778718221203\", \"1a1606ff05ff65003286076fb3ff0d576a67970020003500305206\", \"1e1606ff061cfe4600570b00355317541100330031002d00320032004b58c5\", \"1e1606ff07585e24fea700301073cd60dc751f547d301191525f8c4e0d958b\", \"101606ff088eca5b89516867094fdd969c\", \"041606ff09\", \"041606ff0a\"]}"
// const str =  "{\"ts\": 1591951698131, \"hexStr\": [\"0c1607ff014085891c921800c3\", \"1e1607ff028a2add3322fd2be1f7e6245c003664178508a66570997e30c119\", \"0a1607ff03d56f8a2f9d97\", \"161607ff04ff8485778811113133434544547876868807\", \"081607ff05ff650000\", \"1e1607ff0618fe4600570b003553175411982d57ce516553e358c5585e1cfe\", \"1e1607ff074700570b00355317541100330031002d00320032004b58c5585e\", \"1e1607ff0824fea800301073cd60dc751f547d301191525f8c4e0d958b8eca\", \"0e1607ff095b89516867094fdd969c\", \"041607ff0a\", \"0c1607ff014085891c921800c3\", \"1e1607ff028a2add3322fd2be1f7e6245c003664178508a66570997e30c119\", \"0a1607ff03d56f8a2f9d97\", \"161607ff04ff8485778811113133434544547876868807\", \"081607ff05ff650000\", \"1e1607ff0618fe4600570b003553175411982d57ce516553e358c5585e1cfe\", \"1e1607ff074700570b00355317541100330031002d00320032004b58c5585e\", \"1e1607ff0824fea800301073cd60dc751f547d301191525f8c4e0d958b8eca\", \"0e1607ff095b89516867094fdd969c\", \"041607ff0a\"]}"
test(str);