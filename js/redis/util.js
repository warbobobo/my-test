exports.stripBtHeader = (buff) => {
  const btOrder = buff.readUInt8(4);
  const data = buff.slice(5);

  return {
    btOrder,
    data
  };
};
