const iconv = require('iconv-lite');

const masks = require('./masks');

const readMsg = (buff) => {
  const msgType = buff.readUInt8(1) & masks.oneBit;
  const meta = buff.readUInt8(2);
  const graph = buff.readUInt8(3);
  const data = iconv.decode(buff.slice(4), 'utf16be');

  const priority = meta >> 5;
  const bleOrder = meta & masks.fourBit;

  return {
    msgType,
    priority,
    bleOrder,
    graph,
    data
  };
};

exports.message = (buff) => {
  const result = [];
  const buffLen = buff.length;
  let msgStart = 0;

  while (msgStart < buffLen) {
    const msgLen = buff.readUInt8(msgStart);
    const msg = buff.slice(msgStart, msgStart + msgLen);
    result.push(readMsg(msg));

    msgStart += msgLen;
  }

  return result;
};

exports.location = (buff) => {
  const tcAttr = buff.readUInt8(0);
  const roadInfo = buff.readUInt8(1);
  const mileage = buff.readUInt16BE(2);
  const battery = buff.readUInt16BE(4);
  const tcId = buff.readUInt16BE(6);

  const unitCode = tcAttr & masks.fourBit;
  const roadType = tcAttr >> 4;
  const roadId = roadInfo & masks.sevenBit;
  const direction = roadInfo >> 7;

  return {
    unitCode,
    roadType,
    roadId,
    direction,
    mileage,
    battery,
    tcId
  };
};

exports.performance = (buff) => {
  const msgType = buff.readUInt8(0) & masks.oneBit;
  const meta = buff.readUInt8(1);
  const roadInto = buff.readUInt8(2);
  const segmentSpeeds = buff.slice(3).toString('hex');

  const bleOrder = meta & masks.fourBit;
  const priority = meta >> 4;
  const roadId = roadInto & masks.sevenBit;
  const direction = roadInto >> 7;
  const speeds = segmentSpeeds.split('')
    .map(speed => parseInt(speed, 16))
    .reverse()
    .reduce((arr, v, idx) => {
      if (idx % 2 === 0){
        arr[idx/2] = [v];
      }else{
        arr[(idx-1)/2].push(v);
      }
      return arr;
    },[])
    .reverse()
    .reduce((arr, v) =>{
      return arr.concat(v);
    },[]);

  return {
    msgType,
    bleOrder,
    priority,
    roadId,
    direction,
    speeds
  };
};

exports.travelTime = (buff) => {
  const msgType = buff.readUInt8(0) & masks.oneBit;
  const meta = buff.readUInt8(1);
  const roadPerformance = buff.readUInt8(2);
  const travelDelay = buff.readUInt8(3);
  const freeText = iconv.decode(buff.slice(4), 'utf16be');

  const bleOrder = meta & masks.fourBit;
  const priority = meta >> 4;
  const averageSpeed = roadPerformance & masks.fourBit;
  const levelOfService = (roadPerformance & masks.sevenBit) >> 4;
  console.log('msgType',  msgType);
  console.log('meta',  meta);
  console.log('roadPerformance',  roadPerformance);
  console.log('travelDelay',  travelDelay);
  console.log('freeText',  freeText);
  console.log('bleOrder',  bleOrder);
  console.log('priority',  priority);
  console.log('averageSpeed',  averageSpeed);
  console.log('levelOfService',  levelOfService);


  return {
    msgType,
    bleOrder,
    priority,
    averageSpeed,
    levelOfService,
    travelDelay,
    freeText
  };
};
