const redis = require('./redisClient');

const redisKeys = {
  TEC: 'tpeg:tec.light.event',
  TCEvents: 'tpeg:tc.eventids',
  TCSpeeds: 'tpeg:roadsegmentspeed',
  TCTraveltime: 'tpeg:tc.etag.traveltime',
  TCHeavyTF: 'HeavyTF',
  BTDATA: 'BT:DATA',
  BTHEALTH: 'BT:HEALTH',
  BTMACS: 'BT:MACS',
  BTDOOR: 'BT:DOOR',
  BTREADY: 'BT:READY',
  BTCFG: 'BT:CFG',
  BTLAPTOP_CONNECT: 'BT:LAPTOP:CONNECT',
  BDSCHEDULE: 'bd:schedule',
};

const test = async () => {
  const data = await redis.hgetall(redisKeys.BTDATA);
  console.log('data',data);
}
test();