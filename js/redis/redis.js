const Redis = require('ioredis');

const redisDSN = "redis://192.168.30.110:6379";
// const redisDSN = "redis://127.0.0.1:6380";
// const redisDSN = "redis://172.19.0.2:6379";
//"redis://10.15.52.201:6379";
//`redis://btf5-redis:6379`;
//"redis://btf5-redis:6379/0";
//
//"redis://10.40.101.251:6379";
//"redis://127.0.0.1:6379";
//"redis://172.19.0.2:6379";

const redisKeys = {
  TEC: 'tpeg:tec.light.event',
  TCEvents: 'tpeg:tc.eventids',
  TCSpeeds: 'tpeg:roadsegmentspeed',
  TCTraveltime: 'tpeg:tc.etag.traveltime',
  TCHeavyTF: 'HeavyTF',
  BTDATA: 'BT:DATA',
  BTHEALTH: 'BT:HEALTH',
  BTMACS: 'BT:MACS',
  BTDOOR: 'BT:DOOR',
  BTREADY: 'BT:READY',
  BTCFG: 'BT:CFG',
  BTLAPTOP_CONNECT: 'BT:LAPTOP:CONNECT',
  BDSCHEDULE: 'bd:schedule',
};

console.log('connecting',redisDSN);
const client = new Redis(redisDSN, { lazyConnect: true });
client.on('ready', () => {
  console.log('redis ready');
});
client.on('connect', function () {
  console.log('redis connected');
  // console.log(`connected ${client.connected}`);
}).on('error', function (error) {
  console.log(error);
});
client.on('reconnecting', () => {
  console.log('reconnecting');
});
client.on('error', (err) => {
  console.log(err.message);
});
const test = ()=>{
  client.set('foo', 'bar');
  client.get('foo1', function (err, result) {
    console.log(result);
  });
}
const messageExcute = async () => {
  // const xno = 23552;
  // result = await client.hgetall(`bd:lr.schedules:${xno}`).then(result => {
  //   console.log('result', result);
  // });
  // console.log('result', result);

  // result = await client.hgetall(`bd:schedules:${xno}`).then(result => {
  //   console.log('result', result);
  // });
  const result = await client.hgetall(redisKeys.TCTraveltime);
  // const result = await client.hgetall(redisKeys.BTDATA);
  // const result = await client.hgetall(redisKeys.BTDATA);
  console.log('result', result);
}
const key = async() =>{
  const prefix = `tpeg:tec.light.event:HeavyTF-`;
  const keys = await client.keys(`${prefix}*`);
  console.log(keys);
}
messageExcute();
// key();

// module.exports = redisClient;
// docker run --name redis-lab -p 6379:6379 -d redis