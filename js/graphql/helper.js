const { List } =  require('immutable');
const fetch = require('node-fetch');
const { createHttpLink } = require('apollo-link-http');
const { onError } = require('apollo-link-error');
const { InMemoryCache } = require('apollo-cache-inmemory');

const { ApolloClient } = require('apollo-client');

// const url = 'http://10.40.201.178:1338/graphql';
// const url = 'http://192.168.30.30:4000/graphql';

const fetchData =async (url, query, index) => {
    const httpLink = createHttpLink({ uri: url, fetch });
    const errorLink = onError(({ graphQLErrors, networkError }) => {
        if (graphQLErrors) {
            graphQLErrors.map(({ message, locations, path }) => console.log(`Message: ${message}, Locations: ${locations}, Path: ${path}`));
        }
    });
    const gqlLink = new ApolloClient({
        link: errorLink.concat(httpLink),
        cache: new InMemoryCache({ addTypename: false }),
    });
    
    const data = takeql(gqlLink, query, index);
    // const data = await gqlLink.query({ query, index })
    // .catch(function (error) {
    //     throw error;
    // });
    return data;
};

const takeql = async (gqlLink, query, variables) => {
//   console.log('query', query);
    try {
        const result = await gqlLink.query({ query,variables });
        // console.log('query', query);
        return result;
    } catch (err) {
        console.log(err.message);
        throw err;
    }
};

module.exports = {
  fetchData
}