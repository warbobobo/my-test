const gql = require('graphql-tag');
const { fetchData } = require('./helper');

// const url = 'http://192.168.30.30:4000/graphql';
// const query = gql`
//   query RouteSchedule($xno: Int!=3301){
//     route(xno: $xno, lang:"zh") {
//         id
//         travelTime {
//           from
//           to
//           time
//           distance
//         }
//     }
//   }
// `;
// const url = 'https://citybus.taichung.gov.tw/ebus/strapi/graphql';
const query = gql`
query Routes {
  routes(lang: "zh") {
    edges {
      node {
        id
        name
        departure
        description
        destination
        seq
      }
    }
  }
}
`;
// const query = gql`
// query Routes {
//   blogs {
//     edges {
//       node {
//         title
//       }
//     }
//   }
// }
// `

const index = async () => {
  // const { data } = await fetchData(url, query, { xno: 3301 });
  // const { data } = await fetchData(url, query, { lang: 'zh' });
  const { data } = await fetchData(url, query, {});
  // const data = await fetchData(url, query, { lang: "zh" });
  // console.log('data:',data);
  console.log('data:',data.routes.edges[0].node.id);
  var list = [];
  for(i in data.routes.edges) {
    // console.log(data.routes.edges[i].node.id);
    list.push(data.routes.edges[i].node.id);
  }
  console.log(list.sort());
};
index();