const axios = require("axios")
const gql = require('graphql-tag');
const moment = require('moment');
const { fetchData } = require('./helper');

const url = 'http://127.0.0.1:1339/graphql';
const query = gql`
  query TestSchedule{
    tests{
      name
      source{
        url
      }
    }
  }
`;

const index = async () => {
  const { data } = await fetchData(url, query, {});
  console.log('data:',data);
};
index();