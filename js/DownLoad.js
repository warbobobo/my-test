const http = require('http');
const fs = require('fs');
const request = require('request');
let fileSize = 0


let baseUrl = 'http://localhost:1337'
let chunkSize = 1 * 1024 * 1024 // 切片的大小

const FileLoad = async (name, path) => {
  const file = fs.createWriteStream(name);
  const request = http.get(`${baseUrl}${path}`, function(response) {
    response.pipe(file);
  });
}
    
const MyGet = () => {
    request({
        uri: `${baseUrl}/files`,
        method: "GET",
        timeout: 10000,
        followRedirect: true,
        maxRedirects: 10
    }, (error, response, body) => {
        json  = JSON.parse(body);
        const url = json[2].source[0].url;
        const name = url.split('/')[2];
        // console.log(json);
        // console.log(name,url);
        FileLoad(name,url);
        // console.log(json);
        // console.log(url);
        // console.log(json[0].source);
        // console.log(JSON.parse(body));
    });
};

const DownLoad =async () => {
  // MyGet();
  const json = await MyGet();
  // console.log(json);
  // console.log(json[0]);
  // const response = await MyGet();
  // console.log(response);
  // console.log(response.uri.href);
  // const fileName = response.uri.href.split('/')[3];
  // console.log(fileName);
}

MyGet();