// const Koa = require('koa');
// const { ApolloServer, gql } = require('apollo-server-koa');

// // Construct a schema, using GraphQL schema language
// const typeDefs = gql`
//   type Query {
//     hello: String
//   }
// `;

// // Provide resolver functions for your schema fields
// const resolvers = {
//   Query: {
//     hello: () => 'Hello world!',
//   },
// };

// const server = new ApolloServer({ 
//   typeDefs, 
//   resolvers
// });

// const app = new Koa();
// server.applyMiddleware({ app });
// // alternatively you can get a composed middleware from the apollo server
// // app.use(server.getMiddleware());

// app.listen({ port: 4000 }, () =>
//   console.log(`🚀 Server ready at http://localhost:4000${server.graphqlPath}`),
// );



const Koa = require("koa");
const { ApolloServer } = require("apollo-server-koa");

const { gql } = require("apollo-server-koa");

// 定義從伺服器獲取資料的graphql方法
const typeDefs = gql`
  type Provider {
    id: Int!
    name: String
  }
  type User {
    id: Int!
    name: String
    age: Int
  }
  type todo {
    _id: ID!
    content: String!
    content2: String!
    completed: Boolean!
  }
  type Query {
    Provider:Provider
    test: User
    todoList: [todo]!
  }
`;

const server = new ApolloServer({
  // 使用gql標籤和字串定義的graphql的DocumentNode
  typeDefs,
  // 開啟mock
  mocks: true
});

const app = new Koa();

// applyMiddleware將graphql服務連線到koa框架
server.applyMiddleware({ app });

app.listen({ port: 5000 }, () =>
  console.log(`? Server ready at http://localhost:4000${server.graphqlPath}`)
);