
const path = require('path')
const concat = require('concat-files')
const fs = require('fs')
const crypto = require('crypto');


// 列出文件夹下所有文件
function listDir(path) {
  return new Promise((resolve, reject) => {
      fs.readdir(path, (err, data) => {
          if (err) {
              reject(err)
              return
          }
          // 把mac系统下的临时文件去掉
          if (data && data.length > 0 && data[0] === '.DS_Store') {
              data.splice(0, 1)
          }
          resolve(data)
      })
  })
}
// 检查文件的完整性
let computedHex = (fileName, md5, size) => {
  let rs = fs.createReadStream(fileName)
  let hash = crypto.createHash('md5')
  let hex

  rs.on('data', hash.update.bind(hash))

  rs.on('end', () => {
      hex = hash.digest('hex') 
      // let complete = hex === md5
      console.log('文件名：' + fileName)
      console.log('文件大小：' + size)
      // console.log('文件完整性：' + complete)
  })
}
async function mergeFiles(srcDir, targetDir, newFileName, size, md5) {
  // console.log(...arguments)
  // let targetStream = fs.createWriteStream(path.join(targetDir, newFileName))
  let fileArr = await listDir(srcDir)
  // 按照分割的数据块顺序组装这个文件
  fileArr.sort((x,y) => {
      return x-y;
  })
  // 把文件名加上文件夹的前缀
  for (let i = 0; i < fileArr.length; i++) {
      fileArr[i] = srcDir + '/' + fileArr[i]
  }
  // console.log(fileArr)
  concat(fileArr, path.join(targetDir, newFileName), () => {
      console.log('Merge Success!')
      computedHex(path.join(targetDir, newFileName), md5, size)
  })
}


mergeFiles('temp', './', 'test.jpg', 0, '')
// mergeFiles(path.join(uploadDir, md5), uploadDir, fileName, size, md5)