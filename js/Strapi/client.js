const axios = require('axios');
const fs = require('fs')
const FormData = require('form-data');
const request = require('request');
const crypto = require('crypto');
const md5File = require('md5-file')
const xml2js = require('xml2js');
const xmlconvert = require('xml-js');


// const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NCwiaWF0IjoxNTg3NjMwNjkxLCJleHAiOjE1OTAyMjI2OTF9.HoJJMzMgOBO3LrHWoT3rSr-MgziLcxW_ulhYCe1Fhms';
let token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NCwiaWF0IjoxNTg3NjMzNjE3LCJleHAiOjE1OTAyMjU2MTd9.KBSK6IwZQPm0Pr3srlSX22YcrCQbt4OlwBNf26nXWhk';
// const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ZDZjZGI3YjRmNzBhZjAyM2ExYTlhZDYiLCJpYXQiOjE1ODc2MzI5ODcsImV4cCI6MTU5MDIyNDk4N30.svos9ozQwria7fI2fjeH9HuBS9mo8AB5eAKRJYz8NKs';


const testGet = async () => {
  // const url = `http://127.0.0.1:1337/tests?_limit=2`;
  // const url = `http://127.0.0.1:1337/tests?_limit=2&_sort=value:DESC`;
  // const url = 'http://127.0.0.1:1339/device-infos/test01/version';
  // const url = 'https://citybus.taichung.gov.tw/cms/api/blog';
  // const url = 'https://citybus.taichung.gov.tw/cms/api/notices'
  // const url = 'http://10.40.201.178:1338/device-infos/test01/version';
  // const url = 'https://addr.tgos.tw/addrws/v30/QueryAddr.asmx/QueryAddr';
  // const url = 'https://citybus.taichung.gov.tw/cms/travel-planner/find-plan'
  // const url = 'http://localhost:1337/travel-planner'
  // const url = `http://localhost:3000/ebus/travel-planner`;
  // const url = 'http://10.40.201.178:3006/ebus/adapter/travel-planner/'
  // const url = 'http://localhost:3000/ebus/adapter/travel-planner/'
  // const url = 'https://citybus.taichung.gov.tw/cms/api/route/300/map';
  // const url = 'http://10.40.201.178:1380/ebus/strapi/route-maps';
  const url = 'http://10.40.201.178:1380/ebus/strapi/upload/files';
  // const json = {
  //   // from: '台中市大里區科技路168號',
  //   from: '[24.137712478637695,120.6865005493164]',
  //   // to: '台中市中區台灣大道一段338號'
  //   to: '[24.17877769470215,120.6441421508789]'
  // }
  await axios({
    url: url,
    method: 'get',
    // params: json
    // query: json
    // data: json
  })
  // let data = [];
  // await axios({
  //   url: url,
  //   method: 'get',
  //   // params: json,
  //   // headers: {
  //   //   Authorization: `Bearer ${token}`,
  //   //   "Content-Type": "application/json"
  //   // }
  // })
  .then((response) => {
    // console.log(response);
    // console.log(response.data);
    data = response.data;
  })
  .catch(function (error) {
    console.log("error",error);
    // console.log("error",error.response.status,error.response.statusText);
  });
  return data;
}

const testPost = async () => {
  // const url = `http://10.40.201.178:1337/devices`;
  //const url = 'http://10.40.201.178:1337/testuploads'
  //  const url = `http://127.0.0.1:1337/testuploads`;
  // const url = 'http://10.40.201.178:1338/device-infos';
  // const url = `http://127.0.0.1:1339/burn-imgs`;
  // const url = `http://127.0.0.1:1339/applications`;
  // const url = 'http://10.40.201.178:1338/device-infos';
  // const url = 'http://127.0.0.1:1339/device-infos'
  const url = 'http://10.40.201.178:1340/route-groups';
  await axios({
    url: url,
    method: 'post',
    // data: {name:"name"},
    data: {
      sn:"test12",
      imsi:"null",
      packageName: 'com.busstopadvtctouchrn',
      model: 'DEV',
      fingerPrint: "Maxwin/rk3288/rk3288:7.1.2/NHG47K/20200522_TEST_usrdebug_0953:userdebug/test-keys",
    },
  })
  .then(function (response) {
    console.log(response.data);
    })
    .catch(function (error) {
    // console.log("error",error);
    console.log("error",error.response.status,error.response.statusText);
    });
}

const testUpload = async () => {
  // const url = `http://127.0.0.1:3000/`;
  // const url = `http://127.0.0.1:1339/tests`;
  // const url = `http://127.0.0.1:1339/applications`;
  // const url = `http://127.0.0.1:1337/testuploads`;
  // const url = 'http://10.40.201.178:1337/testuploads'
  // const url = 'http://10.40.201.178:1338/Applications'
  // const url = 'http://10.40.201.178:1338/Firmwares'
  // const url = 'http://10.40.201.178:1338/testuploads'
  // const url = `http://127.0.0.1:1339/upload`;
  // const url = 'http://127.0.0.1:1339/Firmwares/test/updateFile'
  const json = {
    // PackageName: "test02",
    version: "1.0.0"
    // name: "test01",
    // packageName: "test05",
  }
  var formData = new FormData()
  // formData.append(`file`, fs.readFileSync('../file.jpg'), 'file.jpg');
  // formData.append(`files.versions.source`, fs.readFileSync('../file.jpg'), 'file.jpg');
  // formData.append(`files`, fs.readFileSync('../file.jpg'), 'file.jpg');
  // formData.append(`files.source`, fs.readFileSync('../file.jpg'), 'file.jpg');
  formData.append(`files.versions.source`, fs.readFileSync('../file.jpg'), 'file.jpg');
  formData.append('data', JSON.stringify(json));
  // console.log(formData.getHeaders());
  await axios({
      url: url,
      method: 'put',
      // method: 'post',
      data: formData,
      headers : formData.getHeaders(),
    })
  .then(function (response) {
    console.log(response);
    })
    .catch(function (error) {
    // console.log("error",error);
    console.log("error",error.response.status,error.response.statusText);
  });
}

const testUploadFile = async () => {
  // const url_upload = `http://10.40.201.178:1338/upload`;
  // const url_file = 'http://10.40.201.178:1338/burn-imgs/test/updateFile'
  // const url_upload = `http://127.0.0.1:1339/upload`;
  // const url_file = 'http://127.0.0.1:1339/applications/test/updateFile'
  // const url_file = 'http://127.0.0.1:1339/Firmwares/test/updateFile'
  // const url_file = 'http://127.0.0.1:1339/burn-imgs/test/updateFile'
  
  const url_upload = `http://ota.maxwin.com.tw:1337/upload`;
  const url_file = 'http://ota.maxwin.com.tw:1337/burn-imgs/test/updateFile'
  var formData = new FormData()
  formData.append(`files`, fs.readFileSync('../file.jpg'), 'file.jpg');
  // const md5 =crypto.createHash(`md5`).update(JSON.stringify("busstopadv-tc-touch-rn-1.1.1-signed.apk")).digest(`hex`);
  const hash = md5File.sync('test.apk')
  console.log('hash',hash);
  return;
  await axios({
      url: url_upload,
      method: 'post',
      data: formData,
      headers : formData.getHeaders(),
    })
  .then(async (response) => {
    console.log(response.data[0]._id);
    const json = {
      version: "1.0.3",
      source:[response.data[0]._id]
    }
    console.log('json', json);
    await axios({
      url: url_file,
      method: 'put',
      data: json,
      }).then(function (response) {
        console.log(response);
      }).catch(function (error) {
        console.log("error",error.response.status,error.response.statusText);
      })
    })
    .catch(function (error) {
    console.log("error",error.response.status,error.response.statusText);
  });
}

const testPut = async () => {
  // const url = `http://127.0.0.1:1337/tests/3`;
  // const url = `http://10.40.201.178:1337/devices/5ea2822cf6d2673c7a6757c6 `;
  // const url = 'http://127.0.0.1:1338/Devices/5eba38a2ceba53002c541dab'
  // const url = 'http://127.0.0.1:1339/devices/5ec4897a8e61800010ac6634'
  const url = 'http://127.0.0.1:1339/device-infos/test01'
  // const url = 'http://10.40.201.178:1338/device-infos/test01';
  // const url = 'http://127.0.0.1:1339/device-statuses/test09'
  // const url = 'http://127.0.0.1:1339/applications/test09/updateFile'
  // const url = 'http://127.0.0.1:1339/applications/updateFile'
  await axios({
    url: url,
    method: 'put',
    // data: {name:"name"},
    data: {
      // Sn:"test02",
      imsi:"test02",
      // applications: []
      // systemVersion: '1.0.1',
      // appVersion: '1.0.1'
      // source: '5eccc860e45f6600d0d8aca8',
      // packageName: "test04",
      // version: "1.0.0"
      // version:[{version:'2.0.0'}]
    },
  })

  // axios.put(url, {
  //   Sn: 'test01', 
  //   IMSI: 'test01'
  // })

  .then(function (response) {
    console.log(response.data);
  })
  .catch(function (error) {
    console.log("error",error.response.status,error.response.statusText);
  });
}

const testRegister = () => {
  axios.post('http://10.40.201.178:1338/auth/local/register', {
    username: 'root',
    email: 'root@maxwin.com.tw',
    password: 'maxwin80743314',
  })
  // axios.post('http://10.40.201.178:1338/auth/local/register', {
  //   username: 'root',
  //   email: 'hank.lin@maxwin.com.tw',
  //   password: 'maxwin80743314',
  // })
  // axios.post('http://localhost:1337/auth/local/register', {
  //   username: 'warbobobo',
  //   email: 'warbobobo@gmail.com',
  //   password: 'warbobobo',
  // })
  .then(response => {
    // Handle success.
    console.log('Well done!');
    console.log('User profile', response.data.user);
    console.log('User token', response.data.jwt);
    token = response.data.jwt;
  })
  .catch(error => {
    // Handle error.
    console.log("error",error.response.status,error.response.statusText);
  });
}

const testLogin =async () => {
  console.log('Login');
  // axios.post('http://10.40.201.178:1337/auth/local', {
  //   identifier:'root',
  //   password: 'maxwin80743314',
  // })
  // axios.post('http://10.40.201.178:1338/auth/local', {
  //   identifier:'root',
  //   password: 'maxwin80743314',
  // })
  axios.post('http://localhost:1339/auth/local', {
    identifier:'test',
    password: '1234567',
  })
  // await axios.post('http://localhost:1337/auth/local', {
  //   identifier:'warbobobo',
  //   password: 'warbobobo',
  // })
  .then(response => {
    // Handle success.
    console.log('Well done!');
    console.log('User profile', response.data.user);
    console.log('User token', response.data.jwt);
    token = response.data.jwt;
  })
  .catch(error => {
    // Handle error.
    console.log("error",error.response.status,error.response.statusText);
  });
}

const testDelete = async (url,id) => {
  // const url = 'http://localhost:1337/my-files/'
  // const url = 'http://localhost/bttc-paths/'
  // const id = 114;
  console.log(`${url}${id}`);
  await axios
  // .delete(`${url}${id}`, {
  //   headers: {
  //     Authorization: `Bearer ${token}`,
  //   }
  // })
  .delete(`${url}${id}`)
  .then(response => {
    console.log(response);
  })
  .catch(error => {
    // Handle error.
    console.log("error",error.response.status,error.response.statusText);
  });
}

const autoDelete = async (url, list) => {
  console.log(list);
  for(let i = 0;i<list.length;i++){
    console.log(list[i].id);
    await testDelete(url,list[i].id);
  }
}

// curl -X POST -d '{Name:1,Value:2}' http://localhost:1337/tests
// curl -X POST -d "name=name&value=value" http://localhost:1337/tests
// curl -X POST -D '{\"name\":\"Name\",\"value\":\"Name\"}' 'http://localhost:1337/tests'
// curl -X POST -D '{"name" : "Name", "value" : "Value"}' 'http://localhost:1337/tests'
// curl -X POST -H "Content-Type: application/json" -d '{"Name" : "Name", "Value" : "Value"}' "http://localhost:1337/tests"

// curl -v -X POST -d "name=name1&value=value1" http://localhost:1337/tests
// curl -v -X POST -F 'files.cover=@/Users/hank/Documents/test/MyTest/js/file.txt' http://localhost:1337/Uploadfiles?name=name1
// curl -X POST -F 'files=@/Users/hank/Documents/test/MyTest/js/file.txt' http://localhost:1337/upload
// // curl -X POST -F 'Cover=@/Users/hank/Documents/test/MyTest/js/file.txt' -F '{"name" : "Name"}' http://localhost/Uploadfiles
// curl -F data='{"name": "name5"}' -F files.cover=@/Users/hank/Documents/test/MyTest/js/file.txt http://localhost:1337/Uploadfiles

// curl -F data='{"BuildName": "test01"}' -F files.source=@/Users/hank/Documents/test/MyTest/js/file.txt http://localhost:1337/Uploadfiles
// curl -F data='{"PackageName": "test01"}' -F files.source=@/Users/hank/Documents/test/MyTest/js/file.txt http://10.40.201.178:1337/Applications
// curl -F files.source=@/Users/hank/Documents/test/MyTest/js/file.txt http://10.40.201.178:1337/Applications?PackageName=test01

// curl -X POST http://localhost:1337/tests?name=name&value=value


const testUpload2 = async (url, json, file, files) => {
  var formData = new FormData()
  formData.append(files, file);
  formData.append('data', JSON.stringify(json));
  await axios({
      url: url,
      method: 'post',
      data: formData,
      headers : formData.getHeaders(),
    })
  .then(function (response) {
    console.log(response);
    })
    .catch(function (error) {
    console.log("error",error.response.status,error.response.statusText);
  });
}

const ProductionPost = async (json) => {
  // const url = 'https://citybus.taichung.gov.tw/ebus/strapi/blogs'
  // const url = 'https://citybus.taichung.gov.tw/ebus/strapi/notices'
  // const url = 'https://localhost:1337//notices'
  // const url = 'http://10.40.201.178:1380/ebus/strapi/route-maps'

  // const data = {
  //   title :json.title,
  //   // flag : json.flag,
  //   isPublish : true,
  //   onShelfAt : json.on_shelf_at,
  //   offShelfAt : json.off_shelf_at,
  //   locale : 'zh',
  //   content : json.content,
  // }
  // console.log('json',json.link);
  // const data2 = {
  //   link: json.link,
  //   is_publish : json.status == 'publish',
  //   onShelfAt : json.on_shelf_at,
  //   offShelfAt : json.off_shelf_at,
  //   // locale : json.lang,
  //   locale : 'zh',
  //   content : json.message,
  // }
  // console.log('data2',data2);
  // // console.log('data',data);
  // return;
  const formData = new FormData();
  const data = {
    title: 'test',
    route_Id: 300,
    // '路線代碼': 300
    onShelfAt: '2016-07-20',
    // // source: 'https://citybus.taichung.gov.tw/cms/api/route/300/map/46/image',
    // // locale:'',
  };
  // data['title'] = 'test';
  // data['route_Id'] = 300;
  // data['onShelfAt'] = '2016-07-20';
  // formData.append(`files.source`, request('https://citybus.taichung.gov.tw/cms/api/route/300/map/46/image'), 'file.png');
  // formData.append('data', JSON.stringify(data));
  // formData.append('data', JSON.stringify(json));
  await axios({
    url: url,
    method: 'post',
    data: data,
    // data: formData,
    // headers : formData.getHeaders(),
  })
  .then(function (response) {
    console.log(response.data);
    })
    .catch(function (error) {
    // console.log("error",error);
    console.log("error",error.response.status,error.response.statusText);
    });
}
const xmlToJson = (xml) => {
	
	// Create the return object
	var obj = {};

	if (xml.nodeType == 1) { // element
		// do attributes
		if (xml.attributes.length > 0) {
		obj["@attributes"] = {};
			for (var j = 0; j < xml.attributes.length; j++) {
				var attribute = xml.attributes.item(j);
				obj["@attributes"][attribute.nodeName] = attribute.nodeValue;
			}
		}
	} else if (xml.nodeType == 3) { // text
		obj = xml.nodeValue;
	}

	// do children
	if (xml.hasChildNodes()) {
		for(var i = 0; i < xml.childNodes.length; i++) {
			var item = xml.childNodes.item(i);
			var nodeName = item.nodeName;
			if (typeof(obj[nodeName]) == "undefined") {
				obj[nodeName] = xmlToJson(item);
			} else {
				if (typeof(obj[nodeName].push) == "undefined") {
					var old = obj[nodeName];
					obj[nodeName] = [];
					obj[nodeName].push(old);
				}
				obj[nodeName].push(xmlToJson(item));
			}
		}
	}
	return obj;
};

// const planner = async () => {
//   'https://citybus.taichung.gov.tw/cms/travel-planner/find-plan'
// }

const testOTP = async() => {
  // const url = 'https://citybus.taichung.gov.tw/cms/travel-planner/find-plan'
  const json = {
    // from: '[120.695291,24.083993]',
    from: '臺中市大里區東湖里40鄰科技路168號',
    // from: from,
    fx: 120.695291,
    fy: 24.083993,
    // to: '[120.680004,24.142289]',
    to: '臺中市中區大墩里21鄰臺灣大道一段338號',
    // to: to,
    tx: 120.680004,
    ty: 24.142289,
    // arrive_by: 0,
  }
  await axios({
    url: url,
    method: 'get',
    data: json,
  })
  .then(function (response) {
    // data = response.data;
    console.log(response);
  })
  .catch(function (error) {
    data = error;
  });
}

const fetchTgos = async() => {
  // decodeURIComponent('l1H%2BhEA02n%2FrpkG9zWKCbkx0KDMYvd%2B2v1FodzeJU%2Frw99D%2FKmZBpQ%3D%3D')
  // decodeURIComponent('l1H%2BhEA02n%2FrpkG9zWKCbkx0KDMYvd%2B2v1FodzeJU%2Frw99D%2FKmZBpQ%3D%3D')
  
  const query = {
    oReturnMaxCount: 10,
    // oAddress: '%E5%8F%B0%E4%B8%AD%E5%B8%82%E5%A4%A7%E9%87%8C%E5%8D%80%E7%A7%91%E6%8A%80%E8%B7%AF168%E8%99%9F',
    oSRS: 'EPSG%3A4326',
    oResultDataType: 'json',
    oFuzzyType: 1,
    oFuzzyBuffer: 0,
    oIsOnlyFullMatch: false,
    oIsSupportPast: false,
    oIsLockCounty: false,
    oIsLockTown: false,
    oIsLockVillage: false,
    oIsLockRoadSection: false,
    oIsLockLane: false,
    oIsLockAlley: false,
    oIsLockArea: false,
    oIsSameNumber_SubNumber: false,
    oCanIgnoreVillage: false,
    oCanIgnoreNeighborhood: false,
    oAPPId: 'l1H%2BhEA02n%2FrpkG9zWKCbkx0KDMYvd%2B2v1FodzeJU%2Frw99D%2FKmZBpQ%3D%3D',
    oAPIKey: 'cGEErDNy5yNr14zbsE%2F4GSfiGP5i3PuZNrCE441b75Act1ZT0BRsF6%2FlWTKFZjK9HWycLxa%2BBfi5kA%2B5FwzLKMcdbFYjt6944l8HCM4DSiHYPgSwPjdeOY5SZmlTTlvxPmcncWEeRIH%2F%2B5XLhwT5ayX87%2FUhn%2BWr1kDEkS8YsZlXEt9KT5xWh3hjkJWp1JmXoAZg0WhEsmD6p5sB0%2BugQ3gruZbuROgU6ikeJ8USGSNKSnK6jAVmnzb7xdpToyJAU6J%2BmtBbXvY5WwmszSNWq9ye6bwv3DWabeJmOzqB%2FPRztWKqTwyZAiHEJmmn4aI4ie%2Bdv0xIqRyaeARxY7qRwAugUx3amJnPe04XM%2BR2z%2FM%3D',
    oAddress: '臺中市大里區科技路168號',
    // oResultDataType: 'json',
    // // oSRS: encodeURIComponent('EPSG:4326'),
    // oSRS: 'EPSG%3A4326',
    // oFuzzyType: 1,
    // oFuzzyBuffer: 0,
    // oIsOnlyFullMatch: false,
    // oIsSupportPast: false,
    // oIsLockCounty: false,
    // oIsLockTown: false,
    // oIsLockVillage: false,
    // oIsLockRoadSection: false,
    // oIsLockLane: false,
    // oIsLockAlley: false,
    // oIsLockArea: false,
    // oIsSameNumber_SubNumber: false,
    // oCanIgnoreVillage: false,
    // oCanIgnoreNeighborhood: false,
    // oReturnMaxCount: 10,
    // oAPPId: 'l1H%2BhEA02n%2FrpkG9zWKCbkx0KDMYvd%2B2v1FodzeJU%2Frw99D%2FKmZBpQ%3D%3D',
    // oAPIKey: 'cGEErDNy5yNr14zbsE%2F4GSfiGP5i3PuZNrCE441b75Act1ZT0BRsF6%2FlWTKFZjK9HWycLxa%2BBfi5kA%2B5FwzLKMcdbFYjt6944l8HCM4DSiHYPgSwPjdeOY5SZmlTTlvxPmcncWEeRIH%2F%2B5XLhwT5ayX87%2FUhn%2BWr1kDEkS8YsZlXEt9KT5xWh3hjkJWp1JmXoAZg0WhEsmD6p5sB0%2BugQ3gruZbuROgU6ikeJ8USGSNKSnK6jAVmnzb7xdpToyJAU6J%2BmtBbXvY5WwmszSNWq9ye6bwv3DWabeJmOzqB%2FPRztWKqTwyZAiHEJmmn4aI4ie%2Bdv0xIqRyaeARxY7qRwAugUx3amJnPe04XM%2BR2z%2FM%3D'
  }
  // const url = 'https://addr.tgos.tw/addrws/v30/QueryAddr.asmx/PointQueryAddr?oAPPId=l1H%2BhEA02n%2FrpkG9zWKCbkx0KDMYvd%2B2v1FodzeJU%2Frw99D%2FKmZBpQ%3D%3D&oAPIKey=cGEErDNy5yNr14zbsE%2F4GSfiGP5i3PuZNrCE441b75Act1ZT0BRsF6%2FlWTKFZjK9HWycLxa%2BBfi5kA%2B5FwzLKMcdbFYjt6944l8HCM4DSiHYPgSwPjdeOY5SZmlTTlvxPmcncWEeRIH%2F%2B5XLhwT5ayX87%2FUhn%2BWr1kDEkS8YsZlXEt9KT5xWh3hjkJWp1JmXoAZg0WhEsmD6p5sB0%2BugQ3gruZbuROgU6ikeJ8USGSNKSnK6jAVmnzb7xdpToyJAU6J%2BmtBbXvY5WwmszSNWq9ye6bwv3DWabeJmOzqB%2FPRztWKqTwyZAiHEJmmn4aI4ie%2Bdv0xIqRyaeARxY7qRwAugUx3amJnPe04XM%2BR2z%2FM%3D';
  // const url = 'https://addr.tgos.tw/addrws/v30/QueryAddr.asmx/QueryAddr?oReturnMaxCount=10&oAddress=%E5%8F%B0%E4%B8%AD%E5%B8%82%E5%A4%A7%E9%87%8C%E5%8D%80%E7%A7%91%E6%8A%80%E8%B7%AF168%E8%99%9F&oSRS=EPSG%3A4326&oResultDataType=json&oFuzzyType=1&oFuzzyBuffer=0&oIsOnlyFullMatch=false&oIsSupportPast=false&oIsLockCounty=false&oIsLockTown=false&oIsLockVillage=false&oIsLockRoadSection=false&oIsLockLane=false&oIsLockAlley=false&oIsLockArea=false&oIsSameNumber_SubNumber=false&oCanIgnoreVillage=false&oCanIgnoreNeighborhood=false&oAPPId=l1H%2BhEA02n%2FrpkG9zWKCbkx0KDMYvd%2B2v1FodzeJU%2Frw99D%2FKmZBpQ%3D%3D&oAPIKey=cGEErDNy5yNr14zbsE%2F4GSfiGP5i3PuZNrCE441b75Act1ZT0BRsF6%2FlWTKFZjK9HWycLxa%2BBfi5kA%2B5FwzLKMcdbFYjt6944l8HCM4DSiHYPgSwPjdeOY5SZmlTTlvxPmcncWEeRIH%2F%2B5XLhwT5ayX87%2FUhn%2BWr1kDEkS8YsZlXEt9KT5xWh3hjkJWp1JmXoAZg0WhEsmD6p5sB0%2BugQ3gruZbuROgU6ikeJ8USGSNKSnK6jAVmnzb7xdpToyJAU6J%2BmtBbXvY5WwmszSNWq9ye6bwv3DWabeJmOzqB%2FPRztWKqTwyZAiHEJmmn4aI4ie%2Bdv0xIqRyaeARxY7qRwAugUx3amJnPe04XM%2BR2z%2FM%3D';
  // const url = 'https://addr.tgos.tw/addrws/v30/QueryAddr.asmx/QueryAddr';
  // const url  = 'https://map.tgos.tw/TGOSCloud/Generic/Project/GHTGOSViewer_Map.ashx?pagekey=PXBFCMOgiArogLVTfV5TBLEQWu5WIPJh'
  // const path = '%E5%8F%B0%E4%B8%AD%E5%B8%82%E5%A4%A7%E9%87%8C%E5%8D%80%E7%A7%91%E6%8A%80%E8%B7%AF168%E8%99%9F';
  // console.log(encodeURI('臺中市大里區科技路168號'));
  // console.log('%E5%8F%B0%E4%B8%AD%E5%B8%82%E5%A4%A7%E9%87%8C%E5%8D%80%E7%A7%91%E6%8A%80%E8%B7%AF168%E8%99%9F')
  // return;
  const path = encodeURI('台中市大里區科技路168號');
  // const url = `https://addr.tgos.tw/addrws/v30/QueryAddr.asmx/QueryAddr?oReturnMaxCount=10&oAddress=${path}&oSRS=EPSG%3A4326&oResultDataType=json&oFuzzyType=1&oFuzzyBuffer=0&oIsOnlyFullMatch=false&oIsSupportPast=false&oIsLockCounty=false&oIsLockTown=false&oIsLockVillage=false&oIsLockRoadSection=false&oIsLockLane=false&oIsLockAlley=false&oIsLockArea=false&oIsSameNumber_SubNumber=false&oCanIgnoreVillage=false&oCanIgnoreNeighborhood=false&oAPPId=l1H%2BhEA02n%2FrpkG9zWKCbkx0KDMYvd%2B2v1FodzeJU%2Frw99D%2FKmZBpQ%3D%3D&oAPIKey=cGEErDNy5yNr14zbsE%2F4GSfiGP5i3PuZNrCE441b75Act1ZT0BRsF6%2FlWTKFZjK9HWycLxa%2BBfi5kA%2B5FwzLKMcdbFYjt6944l8HCM4DSiHYPgSwPjdeOY5SZmlTTlvxPmcncWEeRIH%2F%2B5XLhwT5ayX87%2FUhn%2BWr1kDEkS8YsZlXEt9KT5xWh3hjkJWp1JmXoAZg0WhEsmD6p5sB0%2BugQ3gruZbuROgU6ikeJ8USGSNKSnK6jAVmnzb7xdpToyJAU6J%2BmtBbXvY5WwmszSNWq9ye6bwv3DWabeJmOzqB%2FPRztWKqTwyZAiHEJmmn4aI4ie%2Bdv0xIqRyaeARxY7qRwAugUx3amJnPe04XM%2BR2z%2FM%3D`;
  // console.log(url);
  // return;
  // const url = 'https://citybus.taichung.gov.tw/cms/travel-planner/fuzzy-place/by-address'
  await axios({
    url: url,
    method: 'get',
    // params: query,
    // params: {
    //   address: '臺中市中區大墩里21鄰臺灣大道一段338號'
    // }
    // headers: {
    //   "Content-Type": 'application/x-www-form-urlencoded'
    // }
  })
  .then(function (response) {
    // data = response.data;
    // response.split(/\'X'|\'Y'/);
    // '<string xmlns="http://tempuri.org/">'
    // console.log('response1', response.data.toString().replace('<?xml version="1.0" encoding="utf-8"?><string xmlns="http://tempuri.org/">',''));
    console.log('=====',response.data);
    const first = response.data.toString().indexOf('{');
    const end = response.data.toString().lastIndexOf('}');
    // const 
    console.log('response1', response.data.toString().indexOf('{'));
    console.log('response1', response.data.toString().lastIndexOf('}'));
    const aaa = JSON.parse(response.data.toString().substring(first, end+1));
    console.log(aaa.AddressList[0].X);
    console.log(aaa.AddressList[0].Y);
    // console.log(response.data.toString().split(/\\r\n /))
    // console.log(encodeURIComponent('EPSG:4326'));
    // console.log('response2', response.data);
    // console.log('response', response.data.toJson());
    // const test = xmlconvert.xml2json(response.data, {compact: true, spaces: 4});
    // console.log('response', test);
    // console.log('response', test);
  //   xml2js.parseString(response.data, { mergeAttrs: true }, (err, result) => {
  //     if (err) {
  //         throw err;
  //     }
  
  //     // `result` is a JavaScript object
  //     // convert it to a JSON string
  //     console.log(result);
  //     const dd = JSON.stringify(result, null, 4);
  //     // console.log(dd.string);
  //     // console.log(dd["X"]);
  //     // save JSON in a file
  //     // fs.writeFileSync('user.json', json);
  
  // }); 
  
    // console.log('response', response.data.split(/\'X'|\'Y'/));
  })
  .catch(function (error) {
    // data = error;
    console.log("error", error);
    // console.log("error",error.response.status,error.response.statusText,error.response.data);
  });
  // return data;
  return [];
}
const Run = async () => {
  // const baseUrl = `http://127.0.0.1:1337/uploadfiles/`;
  // const url = 'http://10.40.201.178:1380/ebus/strapi/route-maps/';
  // const url = 'http://10.40.201.178:1380/ebus/strapi/upload/files/';
  // await testRegister();
  // await testLogin();
  // const list = await testOTP();
  // const list = await fetchTgos();
  // const list = await testGet();
  // autoDelete(url, list);
  // const list = await testGet(`http://127.0.0.1:1337/tests`,{ name: 'test05' });
  await testPost();
  // await testUpload();
  // await testUploadFile();
  // await testPut();
  // for(let i=101;i<=113;i++){
  //   await testDelete(i);
  // }
  // for(let i=0;i<1;i++){
  // for(let i=0;i<list.length;i++){
  //   // console.log('list', list[i]);
  //   await ProductionPost(list[i]);
  // }
  // await ProductionPost({});
  // console.log('list', list.length);
} 
Run();
