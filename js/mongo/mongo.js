const mongoose  = require('mongoose');
const model = require('./model');
const schema = require('./schema');
const sereach = require('./sereach');
const migration = require('./migration');
const test = require('./test');
// import { mongoDSN } from '../../config';

// const mongoDSN=`mongodb://localhost:mongodb:27017/btf5`;
const mongoDSN=`mongodb://localhost:mongodb:27020/strapi`;
// const mongoDSN=`mongodb://localhost:mongodb:27018/strapi`;

const reconnectInterval = 1;
const reconnectTries = 30;


const connect = () => {
  mongoose.connect(mongoDSN, {
    promiseLibrary: global.Promise,
    reconnectInterval: reconnectInterval * 1000,
    reconnectTries,
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
    // family: 4
  }).catch((err) => {
    console.log(`err: ${err.message}`);

    setTimeout(() => {
      console.log('reconnecting');
      connect();
    }, reconnectTries * reconnectInterval * 1000);
  });
};
// mongoose.connect('mongodb://localhost:27017/test', { family: 4 });
mongoose.connection.on('connected',async () => {
  console.log('connected');
  console.log('Start');
  // await test.findMessage();
  // await test.copy();
  // await test.filter();
  // await test.estimatedDocumentCount();
  // await test.countDocuments();
  // await test.find();
  // await test.Update();
  // await test.Delete();
  // await sereach.getData();
  // await migration.Save();
  // await migration.Remove();
  // await migration.Synchronize();
  // await migration.Synchronize();
  // await migration.Synchronize();
  // await migration.Synchronize();
  // await migration.Synchronize();
  // await migration.Synchronize();
  // for(i=0;i<10;i++) {
  //   await migration.Synchronize();
  // }
  console.log('end');
  mongoose.connection.close();
});
mongoose.connection.on('disconnected', () => {
  console.log('disconnected');
});
mongoose.connection.on('reconnectFailed', () => {
  console.log('reconnectFailed');

  setTimeout(() => {
    console.log('reconnecting');
    connect();
  }, reconnectTries * reconnectInterval * 1000);
});

if (!mongoose.connection.readyState) {
  connect();
}
// export default mongoose;
