const model = require('./model');
const schema = require('./schema');
const { Map } = require('immutable');

async function Synchronize() {
  const now = Date.now();
  console.log('Synchronize');
  const id = await LastOne();
  console.log('id',id);
  console.log('LastOne time',Date.now()-now);
  const data = await FindLastData(id);
  console.log('data', data);
  console.log('FindData time',Date.now()-now);
  await WriteData(data);
  console.log('WriteData time',Date.now()-now);
}

async function Remove() {
  const now = Date.now();
  const id = await FindFirstData();
  console.log('id', id);
  console.log('FindData time',Date.now()-now);
  if(id) {
    await RemoveData(id);
  }
  console.log('Remove time',Date.now()-now);
}

async function FindFirstData() {
  // const last = await model.BTTCData3.find({}).limit(1);
  // const last = await model.BTTCData3.find({}).skip(1000000).limit(1);
  const last = await model.BTTCData3.find({}).sort({_id:-1}).limit(1);
  return last.length === 0 ? null : last[0]._id;
}

async function RemoveData(lastId) {
  const res = await model.BTTCData2.deleteMany({ _id: { $lt: lastId } });
  console.log('res', res);
  return res.ok;
}

async function LastOne() {
  const last = await model.BTTCData3.find({}).sort({_id:-1}).limit(1);
  return last.length === 0 ? null : last[0]._id;
}

async function FindLastData(lastId) {
  if (lastId) {
    // return await model.BTTCData.find({_id: {$gt: lastId}}).limit(50000);
    return await model.BTTCData.find({_id: {$gt: lastId}}).limit(1);
  }
  else{
    // return await model.BTTCData.find({}).limit(50000);
    return await model.BTTCData.find({}).limit(1);
  }
}

async function WriteData(data) {
  let list = [];
  // data.forEach(e => {
  //   // const o = { _id:e._id, bttc:e.bttc,createdAt:e.createdAt };
  //   // list.push(o);
  //   list.push(e);
  // });
  // await model.BTTCData2.create(data);
  const d = await model.BTTCData3.insertMany(data);
  console.log('d', d);

  // for(i=0;i<data.length;i++)
  // {
  //   // const BTTC = new model.BTTCData2();
  //   // BTTC._id = data[i]._id;
  //   // BTTC.bttc = data[i].bttc;
  //   // BTTC.createdAt = data[i].createdAt;
  //   // await BTTC.save();

  //   // model.BTTCData2
  //   // console.log('index',data[i].index);
  //   // const params = convertParams(data[i]._id,data[i].bttc,data[i].createdAt);
  //   // console.log('params', params);
  //   // const newOne = await model.BTTCData2.create(params);
  //   console.log(data[i]);
  //   const newOne = await model.BTTCData2.create(data[i]);
  //   // delete map["a"]; 
  // }
}

// export const convertParams = (unpackData, raw) => Map(unpackData[0])
const convertParams = (_id, bttc,createdAt) => Map()
  .set('_id', _id)
  .set('bttc', bttc)
  .set('createdAt', createdAt)
  .toJS();

async function Save() {
  const now = Date.now();
  console.log('copy');

  // const last = await model.BTTCData3.find({}).sort({_id:-1}).limit(1);

  const data = await model.BTTCData.find({}).limit(100000);
  // const data = await model.BTTCData.find({}).limit(100000);
  let list = [];
  for(j=0;j<data.length;j++)
  {
    // const BTTC = new model.BTTCData3();
    // BTTC._id = data[j]._id;
    // BTTC.bttc = data[j].bttc;
    // BTTC.createdAt = data[j].createdAt;
    // // list.push(JSON.stringify(BTTC));
    // await BTTC.save();
    var o = { _id:data[j]._id, bttc:data[j].bttc,createdAt:data[j].createdAt };
    list.push(o);
    // await model.BTTCData2.create({ _id : ObjectId("5bda4200d3c63b003537d7c8"),bttc : 127, createdAt : ISODate("2018-11-01T00:00:00.692Z")});
    // await model.BTTCData2.insertMany(BTTC);
  }
  // console.log('list',list.length);
  // var arr = [{ name: 'Star Wars' }, { name: 'The Empire Strikes Back' }];
  await model.BTTCData3.insertMany(list);

  // console.log('data',data);
  console.log('end');
  console.log('time',Date.now()-now);
}

module.exports = {
  Synchronize,
  Find: LastOne,
  Save,
  Remove
}