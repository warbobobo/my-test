const mongoose = require('mongoose');
const schema = require('./schema');

// User: mongoose.model('user', User),
// BTTCData: mongoose.model('bttc-data', BTTCData),

const User = mongoose.model('user', schema.User);
const BTTC = mongoose.model('bttc', schema.BTTC);
const BTTCData = mongoose.model('bttc-data', schema.BTTCData);
const BTTCData2 = mongoose.model('bttc-data-middle', schema.BTTCData2);
const BTTCData3 = mongoose.model('bttc-data-middle3', schema.BTTCData3);
const HistoryFilter = mongoose.model('history-filter', schema.HistoryFilter);
const Test = mongoose.model('Test', schema.Test);
const Message = mongoose.model('mssage', schema.Message);
const Consumer = mongoose.model('consumer', schema.Consumer);

module.exports = {
  Test,
  User,
  BTTC,
  BTTCData,
  BTTCData2,
  BTTCData3,
  HistoryFilter,
  Message,
  Consumer
};