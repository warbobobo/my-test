#!/bin/bash
# Description: Backup up btf5 collection prev 2 mount data by spectic data and field, and remove collection by --rm
# Usage:  sh backup.btf5.sh histories
#         sh backup.btf5.sh histories executeAt
#         sh backup.btf5.sh histories executeAt 2019-01-01
#         sh backup.btf5.sh histories executeAt --rm
#         sh backup.btf5.sh histories executeAt 2019-01-01 --rm --test
# Author: Thomas Lin

BASEDIR=$(dirname "$0")
removeAfterBackup=0;
tester=0
targetDate=$(date -u +"%Y-%m-%d");

dockerImage="mongo:latest"
dockerContainer="mongodb"
dockerNetwork="btf5dev_default"

cmdPrefix="docker run -i --rm -v ${BASEDIR}:/mongo-backup "
if [ "${dockerNetwork}" != "" ]; then
  cmdPrefix="${cmdPrefix} --network ${dockerNetwork}";
fi
cmdPrefix="${cmdPrefix} --link ${dockerContainer}:mongodb ${dockerImage}";

if [ -z $1 ] || [ -z $2 ]; then
  echo "Usage: sh backup.btf5.sh <collectionName> <filterName> [specificDate] [--rm] [--test]"
  echo " collectionName: collection name";
  echo " filterName: filter collection by name  specificDate";
  echo " specificDate: %Y-%m-%d, default: today";
  echo " --rm: remove collection after backup";
  echo " --test: test found but not running backup and remove"
  exit 0;
fi

collectionName=$1;
fieldName=$2;

if [ "$3" != "--rm" ] && [ "$3" != "--test" ]; then
  targetDate=$3;
fi

if [ "$3" = "--rm" ] || [ "$4" = "--rm" ]; then
  removeAfterBackup=1
fi

if [ "$3" = "--test" ] || [ "$4" = "--test" ] || [ "$5" = "--test" ]; then
  tester=1;
fi

if [ $(uname) = "Darwin" ]; then
  # for macOS
  startDate=$(date -v -2m -u -j -f %Y-%m-%d ${targetDate} +"%Y-%m-01T00:00:00Z");
  endDate=$(date -v -1m -u -j -f %Y-%m-%d ${targetDate} +"%Y-%m-01T00:00:00Z");
  backupMonth=$(date -v -2m -u -j -f %Y-%m-%d ${targetDate} +"%Y%m");
else
  startDate=$(date --date "${targetDate} -2 month" -u +"%Y-%m-01T00:00:00Z");
  endDate=$(date --date "${targetDate} -1 month" -u +"%Y-%m-01T00:00:00Z");
  backupMonth=$(date --date "${targetDate} -2 month" -u +"%Y%m");
fi

targetFile="btf5-${collectionName}.${backupMonth}.backup.gz";
echo "Backup start...";
echo "backup collection: ${collectionName}, from: ${startDate}, to: ${endDate} ";

filter="{\"${fieldName}\": {\"\$gte\":ISODate(\"${startDate}\"),\"\$lt\": ISODate(\"${endDate}\")}}"

## get Count by filter
countCmd="${cmdPrefix} mongo mongodb/btf5 --quiet --eval 'db.getCollection(\"${collectionName}\").count(${filter})'"
countResult=$(eval $countCmd);
# remove \r
count=$(echo ${countResult} | sed $'s/\r//');
if [ $count -eq 0 ]; then
  echo "Nothing to backup, done."
  exit;
fi

echo "Found row: ${count}, start to backup...";
backupCmd="${cmdPrefix} mongodump -h mongodb -d btf5 --collection='${collectionName}' --query='${filter}' --gzip --archive=/mongo-backup/${targetFile}";

if [ $tester -eq 1 ]; then
  echo " # Tester ==> ${backupCmd}";
else
  $(eval $backupCmd);
fi

# get process exit code
backupStatus=$?
if [ $backupStatus -ne 0 ]; then
  echo "backup failed."
  exit $bakupStatus
fi
echo "backup completed. save as ${targetFile}";

if [ $removeAfterBackup -eq 1 ] ; then
  echo "remove collection, start...";
  # remove collection after backup
  dropCmd="${cmdPrefix} mongo mongodb/btf5 --quiet --eval 'db.getCollection(\"${collectionName}\").remove(${filter})'";
  if [ $tester -eq 1 ]; then
    echo " # Tester ==> ${dropCmd}";
  else
    $(eval $dropCmd);
  fi
fi

echo "Backup done.";