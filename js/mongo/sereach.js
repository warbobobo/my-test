const { Map } = require('immutable');
const model = require('./model');
const schema = require('./schema');

async function getData() {
  const now = Date.now();
  console.log('getData');
  // const ids = await findId([1000000,1000024]);
  // const ids = await findId([580000,580024]);
  const ids = await findId([0,3]);
  const data = await findData(ids);
  // console.log('data', data);
  console.log('end');
  console.log('time',Date.now()-now);
}

async function findId(range) {
  const first = range[0];
  const last = range[1];
  const count = (last - first + 1);
  console.log('count',count);
  const sort = ["createdAt","desc"];
  console.log({ [sort[0]]: sort[1].toLowerCase() });
  const data = await model.BTTCData2.find({}).sort({ [sort[0]]: sort[1].toLowerCase() }).skip(first).limit(count);
  // const data = await model.BTTCData2.find({}).sort({ [sort[0]]: sort[1].toLowerCase() }).skip(1000000).limit(24);
  // const data = await model.BTTCData2.find({}).sort({ [sort[0]]: sort[1].toLowerCase() }).skip(1000000).limit(24);
  console.log('data', data.length);
  // let builder = model.BTTCData2.find({});
  // if (sort) {
  //   builder = builder.sort({ [sort[0]]: sort[1].toLowerCase() });
  // }
  // if (range) {
  //   builder = builder.skip(range[0]).limit((range[1] - range[0]) + 1);
  // }
  // // console.log(Date.now() - now);
  // const data  = await builder.exec();

  return data;
}

async function findData(ids) {
  console.log('findData', ids);
  let datas = [];
  // await ids.forEach(async (id) => {
  //   // const data = await model.BTTCData.find({_id: id});
  //   // datas.push(data);
  //   datas.push({_id: id})
  // });
  // for(i=0;i<ids.length;i++)
  // {
  //   // const id = ids[i]
  //   // const data = await model.BTTCData.find({_id: id});
  //   // datas.push(data);
  //   datas.push({ '_id': ids[i]._id})
  // }
  // datas = ids.map(e => e._id );
  // console.log('data2',data);
  ids.forEach(e => {
    datas.push(e._id);
  });
  console.log('datas', datas);
  const data = await model.BTTCData.find({ _id: { $in: datas } });
  // const data = await model.BTTCData.find({ _id: '5e604a33a4e6910027cb2c21' },{ _id: '5e604a33a4e6910027cb2c20' });
  // console.log('data', data);
  // await list.forEach(async id => {
  //   console.log('id', id);
  //   const data = await model.BTTCData.find({'_id': id});
  //   datas.push(data);
  // });
  // console.log('datas', datas);
  return data;
}


module.exports = {
  getData
}