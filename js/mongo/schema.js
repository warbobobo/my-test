const mongoose = require('mongoose');

const defaultOptions = {
  toObject: { virtuals: true, versionKey: false },
  toJSON: { virtuals: true, versionKey: false },
};
const messageStatus = {
  PEND: 'pend',
  QUEUE: 'queue',
  SENT: 'sent',
  REMOVED: 'removed',
};

// import { Schema, Types } from 'mongoose';
const accountSchema = new mongoose.Schema({
  name: {
    type: String,
    minlength: [2, 'must be at least 2 characters'],
  },
  age: {
    type: Number,
    min: [18, 'Must Be 18 Years of Age'],
  },
  sex: String,
  clicks: {
    type: Number,
    default: 0,
  },
  trace: [String],
  fans: [String],
  Date: { type: Date, default: Date.now },
}, { unique: true });

const User = new mongoose.Schema({
  _id: { type: Number, alias: 'id' },
  name: String,
  account: String,
  password: String,
  permissions: { type: mongoose.Schema.Types.Mixed, default: null },
  active: { type: Boolean, default: false },
  createdAt: { type: Date, default: Date.now },
}, defaultOptions);


const BTTCMessage = new mongoose.Schema({
  msgType: Number,
  priority: Number,
  bleOrder: Number,
  graph: { type: Number, default: 0 },
  data: String,
}, {
  _id: false,
});

const BTTCTraveltime = new mongoose.Schema({
  msgType: Number,
  priority: Number,
  bleOrder: Number,
  averageSpeed: Number,
  levelOfService: Number,
  travelDelay: Number,
  freeText: String,
}, {
  _id: false,
});

const BTTCRoadPerformance = new mongoose.Schema({
  msgType: Number,
  priority: Number,
  bleOrder: Number,
  roadId: Number,
  direction: { type: Number, min: 0, max: 2 },
  speeds: [Number],
}, {
  _id: false,
});


const BTTC = new mongoose.Schema({
  _id: { type: Number, alias: 'id' },
  name: String,
  place: String,
  coordinate: {
    lat: Number,
    lng: Number,
  },
  uuid: { type: String, index: true },
  type: { type: String, enum: ['bt', 'btg'], index: true },
  locationOffsets: [],
  struct: String,
  model: String,
  phoneNumber: String,
  network: String,
  host: String,
  ip: { type: String, index: true },
  macaddr: String,
  count: { type: Number, default: 0 },
  description: { type: String, default: '' },
  note: { type: String, default: '' },
  active: { type: Boolean, index: true },
  judgment: { type: Boolean, index: true },
  heavyMessages: { type: Boolean, default: true },
  created_at: { type: Date, default: Date.now },
  azimuth: { type: Array, default: [] },
}, defaultOptions);

const BTTCData = new mongoose.Schema({
  _id: { type: mongoose.Schema.Types.ObjectId, alias: 'id' },
  bttc: { type: Number, ref: 'bttc' },
  ip: String,
  index:{ type: [Number], index: true },
  mileage: Number,
  direction: { type: Number, min: 0, max: 2 },
  messages: [BTTCMessage],
  traveltimes: [BTTCTraveltime],
  performances: [BTTCRoadPerformance],
  packs: [String],
  createdAt: { type: Date, default: Date.now },
}, defaultOptions);
// BTTCData.index({bttc: 1, createdAt: 1 });

const BTTCData2 = new mongoose.Schema({
  _id: { type: mongoose.Schema.Types.ObjectId, alias: 'id' },
  bttc: { type: Number, index: true, ref: 'bttc' },
  createdAt: { type: Date, index: true, default: Date.now },
}, defaultOptions);
// BTTCData2.index({bttc: 1, createdAt: 1 });
// BTTCData2.index({index: 1});
// BTTCData2.index({bttc: 1, type: -1 });
// BTTCData2.index({ createdAt: 1, type: -1 });

const BTTCData3 = new mongoose.Schema({
  _id: { type: mongoose.Schema.Types.ObjectId, alias: 'id' },
  bttc: { type: Number, index: true, ref: 'bttc' },
  createdAt: { type: Date, index: true, default: Date.now },
}, defaultOptions);


 const HistoryFilter = new mongoose.Schema({
  _id: { type: mongoose.Schema.Types.ObjectId, alias: 'id', default: () => (new Types.ObjectId()) },
  url: { type: String, index: true, default: null },
  ip: String,
  user: { type: String, index: true, ref: 'user' },
  executeAt: { type: Date, index: true, default: Date.now },
  rootRoute: { type: String, default: null },
  filter: { type: Object, default: null },
  sort: { type: Array, default: null },
  range: { type: Array, default: null },
}, defaultOptions);


const Message = new mongoose.Schema({
  _id: { type: Number, alias: 'id' },
  origin_id: String,
  event: { type: Number, ref: 'event' },
  graph: { type: Number, ref: 'graph', default: 0 },
  priority: Number,
  /**
   * target: Object, {direction, fmilepost, tmilepost}
   * target: Array, [bttc.id], ex. [1,2,3,4,5]
   */
  target: mongoose.Schema.Types.Mixed,
  content: String,
  /**
   * pend: message on mongo
   * queue: message on tec.light.event
   * sent: message on tec.light.event and BT:DATA
   * removed: message from sent to remove by expire()
   */
  status: { type: String, enum: Object.keys(messageStatus).map(k => messageStatus[k]), default: messageStatus.PEND },
  /**
   * true: publish to internet
   * false: publish to bt
   */
  extra: { type: Boolean, default: false },
  begin_at: { type: Date, default: Date.now },
  end_at: Date,
  sent_at: Date,
  removed_at: Date,
  created_at: { type: Date, default: Date.now },
}, defaultOptions);


const Consumer = new mongoose.Schema({
  _id: { type: mongoose.Schema.Types.ObjectId, alias: 'id', default: () => (new Types.ObjectId()) },
  name: String,
  active: { type: Boolean, default: false },
  // active: { type: Boolean, index: true, default: false },
  createdAt: { type: Date, default: Date.now },
}, defaultOptions);

const Test = new mongoose.Schema({
  name: String,
  age: Number,
  tags: { type: [String], index: true } // field level
});
Test.set('autoIndex', false);
module.exports = {
  // User: mongoose.model('user', User),
  // BTTCData: mongoose.model('bttc-data', BTTCData),
  Test,
  User,
  BTTC,
  BTTCData,
  BTTCData2,
  BTTCData3,
  HistoryFilter,
  Message,
  Consumer
};
