const fetch  = require('node-fetch');
const { List, Map, Set } = require('immutable');
const xml2js = require('xml2js');

const Test = async() => {
  // const api = new URL("https://btmgt-p.freeway.gov.tw/api/inbound-apis/3");
  // const api = new URL("http://10.40.101.251:801/inbound-apis/2/raw");
  // const api = new URL("http://tpeg-exchange-server-web:80/tpeg_exchange_server/v1.0/event2tcs");
  // const res = await fetch(api.toString());
  // const api = new URL("http://10.40.101.251:801/bttc-status");
  // const res = await fetch(api.toString(), {
  //   headers: { 'x-fetch-mode': 'raw' },
  //   timeout: 30 * 1000,
  // });
  // const rawData = await res.json();
  // const dataSet = List(res.file_attribute.section_traffic_data[0].traffic_data);
  // const xml = await res.text();
  // console.log(res);
  // console.log(dataSet);
  // console.log(rawData);
  const res = await fetch("http://10.40.101.251:801/inbound-apis/2/raw", { timeout: 60000 });
  console.log(`status:${res.status}`);
  if (res.headers.get('content-type').indexOf('xml') >= 0) {
    const xml = await res.text();
    const parser = new xml2js.Parser();
    const raw = await parser.parseStringPromise(xml);
    const dataSet = List(raw.file_attribute.section_traffic_data[0].traffic_data);
    const section = dataSet.map(item => item.$).filter(item => item.freewayId === '5' || item.freewayId === '3').toJS();
    // const data = await Promise.all([section]);
    console.log(`dataSet:${JSON.stringify(section)}`);
  }
  else{
    console.log(`json:${res.json()}`);
  }
}
Test();

