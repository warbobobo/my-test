function XmlToJson() {
}
XmlToJson.prototype.setXml = function(xml) {
    if(xml && typeof xml == 'string') {
        this.xml = document.createElement('div');
        this.xml.innerHTML = xml;
        this.xml = this.xml.getElementsByTagName('*')[0];
    }
    else if(typeof xml == 'object'){
        this.xml = xml;
    }
};
XmlToJson.prototype.getXml = function() {
    return this.xml;
};
XmlToJson.prototype.parse = function(xml) {
    this.setXml(xml);
    return this.convert(this.xml);
};
XmlToJson.prototype.convert = function(xml) {
    if (xml.nodeType != 1) {
        return null;
    }
    var obj = {};
    obj.xtype = xml.nodeName.toLowerCase();
    var nodeValue = (xml.textContent || '').replace(/(\r|\n)/g, '').replace(/^\s |\s $/g, '');
   
    if(nodeValue && xml.childNodes.length == 1) {
        obj.text = nodeValue;
    }
    if (xml.attributes.length > 0) {
        for (var j = 0; j < xml.attributes.length; j ) {
            var attribute = xml.attributes.item(j);
            obj[attribute.nodeName] = attribute.nodeValue;
        }
    }
    if (xml.childNodes.length > 0) {
        var items = [];
        for(var i = 0; i < xml.childNodes.length; i ) {
            var node = xml.childNodes.item(i);
            var item = this.convert(node);
            if(item) {
                items.push(item);
            }
        }
        if(items.length > 0) {
            obj.items = items;
        }
    }
    return obj;
};

const x = 
`<BusStationList>
<UpdateTime>2017-04-05T02:13:06+08:00</UpdateTime>
<UpdateInterval>86400</UpdateInterval>
<AuthorityCode>TPE</AuthorityCode>
<Stations>
<Station>
<StationID>732</StationID>
<StationName>
<Zh_tw>松園</Zh_tw>
<En>SongYuan</En>
</StationName>
<StationPosition>
<PositionLat>25.15812</PositionLat>
<PositionLon>121.56511</PositionLon>
</StationPosition>
<RoadName>…</RoadName>
<Bearing>…</Bearing>
<StationAddress>…</StationAddress>
<StationDescription>…</StationDescription>
</Station>
</Stations>
</BusStationList>`;

// const xx = new XmlToJson();

// console.log(XmlToJson());
// console.log(xx);
// console.log(xx.setXml(x));
// console.log(xx.convert(x));


// function eachObj(obj) {
//   if (typeof obj === 'string') 
//     obj = JSON.parse(obj);            
//   if (typeof obj !== 'object') {              
//       console.error('参数错误: %o不是有效的Object对象.', obj);    
//                   return;            
//  }         
//   for (var key in obj) {   
//                 console.log(key,':', obj[key]);       
//                     }        }  //根据动态key获取值:  obj[key]


const json = {
  "BusRouteNetworkList": {
    "-xmlns": "http://ptx.transportdata.tw/standard/schema/",
    "-xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
    "-xsi:schemaLocation": "http://ptx.transportdata.tw/standard/schema/ ",
    "UpdateTime": "2016-08-16T10:03:12+08:00",
    "UpdateInterval": "86400",
    "AuthorityCode": "TPE",
    "RouteNetworks": {
      "RouteNetwork": {
        "RouteID": "17686",
        "RouteName": "216",
        "Zh_tw": "105",
        "En": "105",
        "Segments": {
          "Segment": [
            {
              "Sequence": "1",
              "FromStopID": "290577",
              "ToStopID": "290453",
              "Distance": "…"
            },
            {
              "Sequence": "2",
              "FromStopID": "290453",
              "ToStopID": "290484",
              "Distance": "…"
            },
            {
              "Sequence": "2",
              "FromStopID": "290484",
              "ToStopID": "290523",
              "Distance": "…"
            }
          ]
        }
      }
    }
  }
};

const myLoop = obj => {
  obj.forEach(key => {
    console.log(key);
    // if (typeof obj !== 'object'){
    //   key='';
    // }
    // else{
    //   myLoop(key)
    // }
  });
  // return obj;
}

console.log(json);
console.log(myLoop(json));
